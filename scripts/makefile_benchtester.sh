#!/bin/sh
if [ "$#" -ne 2 ] || ! [ -d "$1" ]; then
  echo "Usage: $0 DIRECTORY SOLVEROPTS " >&2
  exit 1
fi
#Usage: to generate Makefile for running finding times for DAGS in DAGFLDR as first param
#NUMRUNS=5
#make -j20 all NUMRUNS="7" SEEDS="30127 15663 4896 11972 28853 14429 31164" BENCH="AutoGrader" OS="SO-1-5"

#bash makefile_benchtester.sh /home/ubuntu/sos/sketch-backend/synofsyn/ris_auto/temp_py_sk/AutoGraderBenchmarks/experiment_post_so_dags/1/train_size_5
DAGFLDR=$1
OPTS=$2 #--bndwrand 150
cd $DAGFLDR
LANGTR=/home/ubuntu/smt-optim-sos/languageTranslators
BE=/home/ubuntu/sos/sketch-backend
mkdir -p make_outs

echo -n "all: check-env "

for i in `ls *.dag`; do
    echo -n "t$i "
done

echo "";
echo "	echo \"done all\""

echo "check-env:
ifndef NUMRUNS
	\$(error NUMRUNS is undefined)
endif
ifndef SEEDS
	\$(error SEEDS is undefined)
endif
ifndef BENCH
	\$(error BENCH is undefined)
endif
ifndef OS
	\$(error OS is undefined)
endif"


ctr=0
for i in `ls *.dag`; do
    echo "t$i: check-env
	-@(echo \"$i\")
	-@(python $LANGTR/sketchBenchTester.py --numRuns \${NUMRUNS} --fileName $DAGFLDR/$i --solver $LANGTR/cegisSolver.py --solverOptions \"$OPTS\" --benchmark \${BENCH} --optimSource \${OS}  --seeds \${SEEDS}) > ./make_outs/${i}_\${OS}.out
"
done


#Post Makefile SQL Query
# select a.DagFile,a.DagSize as cp_size,a.SolutionMedianTime as cp_time,b.DagSize as s1_5_size,b.SolutionMedianTime as s1_5_time, c.DagSize as do_size,c.SolutionMedianTime as do_time from run_stats_vmcai a, run_stats_vmcai b, run_stats_vmcai c where a.BenchmarkID = b.BenchmarkID and b.BenchmarkID = c.BenchmarkID and a.OptimSource ='CP' and b.optimSource='SO-1-5' and c.OptimSource='DO-L1' and a.DagFile=b.DagFile and b.DagFile = c.DagFile;

