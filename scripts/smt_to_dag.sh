#!/bin/bash

DOMAINS=$1
#$1 = path to parent folder containing domain folders with .smt2 files

LT=/home/ubuntu/smt-optim-sos/languageTranslators

ulimit -s unlimited

#smt2 to dag
for DOMAIN in `find $DOMAINS -type d`; do

    #TODO: Clean dag_generated_flag if you want to generate dag files again
    if mkdir $DOMAIN/dag_generated_flag; then
        for SMT in `ls $DOMAIN/*.smt2`; do
            echo "Processing $SMT"
            python $LT/smtToTemp.py $SMT > $SMT.temp
	        #Converting temp DAG to full DAG via post processing"
	        python $LT/tempToDAG.py $SMT.temp $SMT.dag
        done
    fi

done
