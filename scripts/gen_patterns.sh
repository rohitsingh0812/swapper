#!/bin/bash

#$1 = path to domain folder with .smt2 files
#number of samples, e.g. MILLION 

#forall sizes 3,4,5

LT=/home/ubuntu/smt-optim-sos/languageTranslators
DT=/home/ubuntu/smt-optim-sos/DAGTools
DOMAIN=$1
NUM_SAMPLES=1000000

ulimit -s unlimited

#smt2 to dag
#TODO: Clean dag_generated_flag if you want to generate dag files again
if mkdir $DOMAIN/dag_generated_flag; then
    for SMT in `ls $DOMAIN/*.smt2`; do
        echo "Processing $SMT"
        python $LT/smtToTemp.py $SMT > $SMT.temp
	    #Converting temp DAG to full DAG via post processing"
	    python $LT/tempToDAG.py $SMT.temp $SMT.dag
    done
fi

echo "Starting pattern finding..."
#find patterns from the DOMAIN
python $DT/PatternFinding.py -f $DOMAIN -o patterns -l 3-5 -n $NUM_SAMPLES -t dag

#TODO:run makefiles for generating rules! catching errors?
