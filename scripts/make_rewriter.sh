#!/bin/bash
if [ "$#" -lt 2 ]; then
    echo "make_rewriter.sh: Illegal number of parameters, 2 needed
        REWRITER_OUTPUT_PATH=1
		SosOptimFolder=2
"
    exit 0;
fi
OUTPUT=$1
BESRC='/home/ubuntu/sos/sketch-backend/src/SketchSolver/'
IPSRC=$BESRC/InputParser
SosOptimFolder=$2
DOTOPRE="$BESRC/.libs/libcegis_la-"
#"$BESRC/SosOptim/"
echo "Compiling..."
g++ -I $SosOptimFolder -I $BESRC/ -I $BESRC/SolverInterfaces/  -I $IPSRC/ -o $OUTPUT ${DOTOPRE}NodeVisitor.o ${DOTOPRE}NodeStore.o ${DOTOPRE}SosOptim.o ${DOTOPRE}BooleanDAG.o ${DOTOPRE}BooleanNodes.o ${DOTOPRE}DagCSE.o ${DOTOPRE}DagUtils.o  $SosOptimFolder/SosOptim_Auto.cpp $IPSRC/Rewriter.cpp $IPSRC/RWCommandLineArgs.cpp -lrt
echo "Done: $OUTPUT"
#New files: Rewriter.cpp, RWCommandLineArgs.cpp and make_rewriter.sh