import argparse
import MySQLdb
 
parser = argparse.ArgumentParser(description='Deleting all rules from DB for this folder!')


parser.add_argument('folder', metavar='fldr', type=str,help='a folder name string')

args = parser.parse_args()
#args.folder
db = MySQLdb.connect(host="localhost",user="root",passwd="",db="rewriterules")
cur = db.cursor()
x=cur.execute("delete from rules where folder like '%" + args.folder + "%' ;");
print "Removed rows: " + str(x)
cur.close()
db.commit()
db.close()

