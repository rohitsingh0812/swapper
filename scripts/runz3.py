
import sys
import re
import os
import argparse
import math
import shutil
from os.path import join
from subprocess import call,  Popen, PIPE, check_output, CalledProcessError, STDOUT
from pathos.multiprocessing import ProcessingPool,cpu_count
#Takes a folder name and searches for all smt2 files in there and then runs them using z3 -st
#maintaining a pool count
def d():
    import pdb
    pdb.set_trace()
    
def runFile((outFolder,filebase,TIMEOUT)):
    X = ""
    print "Starting %s." % filebase
    with open(join(outFolder,filebase + ".out"),"w") as fout:
        try:
            X = check_output(['z3','-st','-T:%d' % TIMEOUT, join(outFolder,filebase + ".smt2")],stderr=STDOUT)
            #skast --fe-tempdir ./ 0.sk
            if "timeout" in X:
                TIME = "timeout"
            else:
                TIME = (filter(lambda t: ":time" in t,filter(None,X.split('\n')))[0]).split()[1]
            fout.write(X)
        except CalledProcessError, e:
            X = e.output  
            fout.write(X)
            print "Error %s." % filebase
            return -1
    print "Done %s : %s" % (filebase,TIME)
    return 0
    

if __name__=="__main__":        
    parser = argparse.ArgumentParser()
    parser.add_argument("-of",help="Folder name",type=str,required=True)
    parser.add_argument("-timeout",help="TIMEOUT",type=int,required=True)
    args = parser.parse_args()
    files = map(lambda x: x[:-5],filter(lambda x: x[-5:] == ".smt2" ,os.listdir(args.of)))
    argsrun = [(args.of,f,args.timeout) for f in files]
    ncpus = cpu_count()
    p =  ProcessingPool(nodes=min(20,ncpus-2))
    p.map(runFile,argsrun)


