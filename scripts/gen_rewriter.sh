#!/bin/bash
#Usage: To generate a SO-Optimized Sketch backend binary with custom rules as specified (ordering defined in the way its specified)

# bash gen_rewriter.sh <rulefolder> <parallelism> $BE 38 19 20 21 23 22 5 3 1 6 7 8 2 9 17 10 15 16 18 4 11 12 13 14 25 26 27 28 29 30 31 32 33 38 24 36 34 35 37
# bash gen_rewriter.sh $SOS/do_rules 20 $BE 38 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38


#Inputs: 
#befolder="/home/rohitsingh/Research/Sketchsos/sketch-backend"
repofldr=$1
parallelism=$2
befolder=$3
numfolders=$4
indices="${*:5}"
UAPSCRIPTS=/home/ubuntu/smt-optim-sos/scripts

indicesnum=`echo "$indices" | wc | awk '{print $2}'`
if [ "$numfolders" != "$indicesnum" ]; then
    echo "Invalid NUMFOLDER: $numfolders for $indices_num numbers"
    exit 1
fi
if [ "$#" -lt 5 ]; then
    echo "gen.sh: Illegal number of parameters, 6+ needed
        repofldr=1
        parallelism=2 (not optional)
        befolder=3
        numfolders=4
        indices=5 onwards \${*:5}
"
    exit 0;
fi




cd "$befolder/synofsyn"

#generate matches.cpp

stry="$numfolders $indices"

export LD_LIBRARY_PATH=/home/rohitsingh/Research/Sketchsos/sketch-backend/src/SketchSolver/.libs
echo "calling DEBUG binary compiled with -O0 -D_DEBUG options: ../src/SketchSolver/cegis --synofsyn $repofldr $stry tt"

$befolder/src/SketchSolver/cegis --synofsyn "$repofldr" $stry tt &> ./temp.out || { echo "command failed"; exit 1; }


#should create $repofldr/matches.cpp


#get the header stuff and remove it

cat "$repofldr/matches.cpp" | awk 'BEGIN {seen=0} {if(match($0,"SOSOPTIM")!= 0){ seen=seen+1;} if(seen==1) print $0; if(seen==2) {print $0; seen=3;} }' > "$repofldr/matches.h"
cat "$repofldr/matches.cpp" | awk 'BEGIN {seen=0} {if(match($0,"SOSOPTIM")!= 0){ seen=seen+1;} if(seen!=1) print $0; if(seen==2) {print $0; seen=3;} }' > "$repofldr/SosOptim_Auto.cpp"

#cp "$repofldr/SosOptim_Auto.cpp" "$befolder/src/SketchSolver/InputParser/SosOptim_Auto.cpp"

cat "$befolder/src/SketchSolver/SosOptim/SosOptim.h" | awk 'BEGIN {seen=0} {if(match($0,"DECLARATIONS") != 0){seen=1;} if(seen==0) print $0;}' > "$repofldr/matchespre.h"

cat "$befolder/src/SketchSolver/SosOptim/SosOptim.h" | awk 'BEGIN {seen=0} {if(match($0,"DECLARATIONS") != 0){seen=seen+1;} if(seen>=2) print $0;}' > "$repofldr/matchespost.h"

cat "$repofldr/matches.h" >> "$repofldr/matchespre.h"

cat "$repofldr/matchespost.h" >> "$repofldr/matchespre.h"

mv "$repofldr/matchespre.h" "$repofldr/SosOptim.h"

#cd $befolder

#MSBuild.exe src/Sketch.sln /p:Configuration=Release /m:4 /nologo || { echo "Build failed"; exit 1; }
#cp ./src/Release/Sketch.exe ./src/SketchSolver/cegis.exe
#make -j$parallelism  || { echo "Make failed"; exit 1; }
echo "bash $UAPSCRIPTS/make_rewriter.sh $repofldr/cegis_r $repofldr"

bash $UAPSCRIPTS/make_rewriter.sh $repofldr/cegis_r $repofldr
