#!/usr/bin/env python
# a bar plot with errorbars

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot
import matplotlib.pyplot as plt
from pylab import *
import sys
import MySQLdb
import signal
import numpy as numpie
import re

#plots.getPlots('AutoGrader','AG1')

db = MySQLdb.connect(host="localhost", user="root", passwd="", db="results")
cur = db.cursor()

def get_stmt_same(bench,optimsource):
    if(optimsource == 'AG1' and bench == 'AutoGrader'):
        so_os_like= 'SO-OT-299%'
    elif(optimsource == 'AG2' and bench == 'AutoGrader'):
        so_os_like= 'SO-OT-318%'
    elif(optimsource == 'SY1' and bench == 'Sygus'):
        so_os_like= 'SO-SY1-203%'
    elif(optimsource == 'SY2' and bench == 'Sygus'):
        so_os_like= 'SO-SY1-163%'
    else:
        print "Invalid input to get_stmt_same"
        sys.exit(1)
    
    return "select cp.DagFile, cp.SolutionMedianTime cp_median, so.SolutionMedianTime so_median, do.SolutionMedianTime do_median, cp.SolutionAvgTime cp_avg, so.SolutionAvgTime so_avg, do.SolutionAvgTime do_avg,  cp.SolutionStDevTime cp_std, so.SolutionStDevTime so_std, do.SolutionStDevTime do_std, cp.SolutionAvgMaxMem cp_mem, so.SolutionAvgMaxMem so_mem, do.SolutionAvgMaxMem do_mem from run_stats_vmcai cp, run_stats_vmcai do, run_stats_vmcai so where cp.dagfile = do.dagfile and cp.dagfile = so.dagfile and cp.optimsource ='CP' and do.optimsource = 'DO-L1' and so.benchmarkId = '" + bench+ "' and so.optimsource like '" +so_os_like + "'"

def get_stmt_cross(bench,optimsource):
    main_so=dict()
    main_so['Sygus'] = 'SO-SY1-203%'
    main_so['AutoGrader'] = 'SO-OT-299%'
    if(optimsource == 'AG1' and bench == 'Sygus'):
        so_os_like= 'SO-AG-F1'
    elif(optimsource == 'AG2' and bench == 'Sygus'):
        so_os_like= 'SO-AG-F2'
    elif(optimsource == 'SY1' and bench == 'AutoGrader'):
        so_os_like= 'SO-SY-F1'
    else:
        print "Invalid input to get_stmt_cross"
        sys.exit(1)
    
    return "select cp.DagFile, cp.SolutionMedianTime cp_median, so.SolutionMedianTime so_median, do.SolutionMedianTime do_median, cp.SolutionAvgTime cp_avg, so.SolutionAvgTime so_avg, do.SolutionAvgTime do_avg,  cp.SolutionStDevTime cp_std, so.SolutionStDevTime so_std, do.SolutionStDevTime do_std, cp.SolutionAvgMaxMem cp_mem, so.SolutionAvgMaxMem so_mem, do.SolutionAvgMaxMem do_mem from run_stats_vmcai cp, run_stats_vmcai do, run_stats_vmcai so where cp.dagfile = do.dagfile and cp.dagfile = so.dagfile and cp.optimsource ='CP' and do.optimsource like '"+main_so[bench]+"' and so.benchmarkId = '" + bench+ "' and so.optimsource like '" +so_os_like + "'"
    
def get_ot_stmt(bench, run):
    if(bench == 'AutoGrader'):
        btab = 'OT_AG_C' + str(run)
    elif(bench == 'Sygus'):
        btab = 'OT_SY_CN' + str(run)
    else:
        print "Invalid input to get_ot_stmt"
        sys.exit(1)
        
    if(bench=='Sygus'):
        return  "select id,(100-metric) perf from " + btab + " where id > 0 order by id asc"
    else:
        return  "select (id-50) as id,(100-metric) perf from " + btab + " where id > 50 order by id asc"
    
def execute_smt(stmt):
    cur.execute(stmt)
    x = cur.fetchall()
    #assume that first element is key and the rest are in a list
    r = dict()
    for a in x:
        r[a[0]] = a[1:]
    return r

rewriters = ['AG1','AG2','SY1','SY2']
benches = ['AutoGrader','Sygus']

def parseSketchData(r):
    #return "select cp.DagFile, cp.SolutionMedianTime cp_median, so.SolutionMedianTime so_median, do.SolutionMedianTime do_median, cp.SolutionAvgTime cp_avg, so.SolutionAvgTime so_avg, do.SolutionAvgTime do_avg,  cp.SolutionStDevTime cp_std, so.SolutionStDevTime so_std, do.SolutionStDevTime do_std, cp.SolutionAvgMaxMem cp_mem, so.SolutionAvgMaxMem so_mem, do.SolutionAvgMaxMem do_mem from run_stats_vmcai cp, run_stats_vmcai do, run_stats_vmcai so where cp.dagfile = do.dagfile and cp.dagfile = so.dagfile and cp.optimsource ='CP' and do.optimsource = 'DO-L1' and so.benchmarkId = '" + bench+ "' and so.optimsource like '" +so_os_like + "'"
    res= dict() 
    res ["cp"] = dict()
    res ["so"] = dict()
    res ["do"] = dict()
    for optim in res:
        res[optim]["mean"] = []
        res[optim]["median"] = []
        res[optim]["stdev"] = []
        res[optim]["mem"] = []
        
    for i in r:
        elt = r[i]
        res['cp']['median'].append(elt[0])
        res['so']['median'].append(elt[1])
        res['do']['median'].append(elt[2])
        res['cp']['mean'].append(elt[3])
        res['so']['mean'].append(elt[4])
        res['do']['mean'].append(elt[5])
        res['cp']['stdev'].append(elt[6])
        res['so']['stdev'].append(elt[7])
        res['do']['stdev'].append(elt[8])
        res['cp']['mem'].append(elt[9])
        res['so']['mem'].append(elt[10])
        res['do']['mem'].append(elt[11])
    return res

    
def getLabel(type):
    if(type=='median'):
        return "Median Time (in seconds)"
    elif type == "mean":
        return "Average Time (in seconds)"
    elif type == "mem":
        return "Average Maximum Memory Used (in MBs)"
    else:
        print "Invalid type of graph!"
        cc()
        sys.exit(0)
def getPlot(bench,optimsource,data,type,N,otherbench):
    print bench,optimsource,len(data['so']),type,N
        
    config_num = int(re.sub("[^0-9]","",optimsource))
    fig = plt.figure(figsize=(8*(N/17.0),6))
    #plt.savefig('foo.png', bbox_inches='tight')
    ax = fig.add_subplot(111)
    #fig, ax = plt.subplots()
    ind = numpie.arange(N)
    #w = 0.35
    ## the bars
    margin = 0.15
    w = (1.-2.*margin)/3.0
    
    rects1 = ax.bar(ind+margin, data['cp'][type], width=w, color='red')
    rects2 = ax.bar(ind+margin+w, data['do'][type], width=w, color='yellow')
    rects3 = ax.bar(ind+margin+2*w, data['so'][type], width=w, color='green')
                   
    # rects2 = ax.bar(ind+w, data['so']['mean'], width,
                    # color='red',
                    # yerr=data['so']['stdev'],
                    # error_kw=dict(elinewidth=2,ecolor='black'))
    #ax.set_xlim(-2*w,len(ind)+w)
    #ax.set_ylim(0,45)
    ax.set_ylabel(getLabel(type))
    if(otherbench != bench):
        title = 'Performance of '+ otherbench +' rewriter on '+bench+' Benchmarks'
    else:
        title = 'Performance on '+bench+' Benchmarks: Configuration ' + str(config_num)
    
    ax.set_title(title)
    xTickMarks = [bench[0] + str(i+1) for i in range(N)]#['Group'+str(i) for i in range(1,6)]
    ax.set_xticks(ind+2*w)
    xtickNames = ax.set_xticklabels(xTickMarks)
    plt.setp(xtickNames, rotation=0, fontsize=10)

    ## add a legend
    if(bench == otherbench):
        ax.legend( (rects1[0], rects2[0], rects3[0]), ('Baseline','Hand-crafted', 'Auto-generated') )
    else:
        ax.legend( (rects1[0], rects2[0], rects3[0]), ('Baseline','Auto-generated for ' + bench, 'Auto-generated for ' + otherbench ) )
    plt.savefig(bench + '_' + optimsource+ '_' + type + '.png', bbox_inches='tight')
    plt.close(fig)
    
def getLinePlots():
    for bench in ['AutoGrader','Sygus']:
        r=dict()
        for run in [1,2]: 
            r[run] = execute_smt(get_ot_stmt(bench,run))
        #print r
        #plot two lines graphs in one figure
        fig = plt.figure(figsize=(12,6))
        #ax = fig.add_subplot(111)
        
        plt.plot(r[1].keys(),r[1].values(),marker='o',linestyle='--',color='r',label="Configuration 1")
        plt.plot(r[2].keys(),r[2].values(),marker='s',linestyle='--',color='b',label="Configuration 2")
        plt.xlabel('Iteration')
        plt.ylabel('Optimization Function')
        plt.title('Auto-tuner: variation in value of Optimization Function: ' + bench)
        plt.legend(loc='lower right')
        plt.savefig(bench + '_tuning.png', bbox_inches='tight')
        plt.close(fig)
def getPlots():
    #get 3 plots of CP,DO and SO-autogenerated versions with error bars on Mean time
    #Mean Time, Median Time, AvgMaxMem
    
    for bench in ['AutoGrader','Sygus']:
        for optimsource in ['AG1','AG2','SY1','SY2']:
            for type in ['median','mean','mem']:
                if(optimsource == 'SY2' and bench == 'AutoGrader'):
                    continue
                if(re.sub("[0-9]","",optimsource) == 'AG'):
                    otherbench = 'AutoGrader'
                elif (re.sub("[0-9]","",optimsource) == 'SY'):
                    otherbench = 'Sygus'
                else:
                    cc()
                    print "Invalid optimsource"
                    sys.exit(1)
                #print bench, optimsource
                if(bench == otherbench):
                    stmt = get_stmt_same(bench,optimsource)
                else:
                    stmt = get_stmt_cross(bench,optimsource)
                r = execute_smt(stmt)                
                #get data
                N = len(r)
                data = parseSketchData(r)
                getPlot(bench,optimsource,data,type,N,otherbench)
    
    #plt.show()
def cc():
    cur.close()
    db.close()
    
def signal_handler(signal, frame):
    print('You pressed Ctrl+C!')
    cc()
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)
matplotlib.rcParams.update({'font.size': 8})

getLinePlots()
getPlots()
cc()
