#!/bin/bash -e
#Usage: to generate Makefile for generating DAGs for all sketch files in a folder
#$2 = extra parameters for sketch run
#example: bash cross_gen_makefile.sh autograder " --be:-nodagoptim -V 10 "  > ./autograder/Makefile
rhssize=$1
echo -n "all: "

for i in `ls *.dag | wc -l`; do
    echo -n "t$i "
done

echo "";
echo "	echo \"done all\""

for i in `ls *.dag | wc -l`; do
    echo "t$i: 
	-cegis --genrules $rhssize dag_$i.dag lol 
"
done