import sys
import re
from os import listdir, makedirs, rmdir
from os.path import join, exists
import argparse
import math
import shutil
from subprocess import call,  Popen, PIPE, check_output, CalledProcessError, STDOUT
#from pyparsing import OneOrMore, nestedExpr
#Input: Signature of a dag with ANV info
from dag import *

class SWAPPERSampler(DAGSampler):
    
    def outputFormat(self,patterns):
        #should output all info with ANVs
        patanvstrs = map(lambda (idr,pattern): self.sig_helper(idr,pattern,{},True,True), patterns)
        return '##'.join(list(set(patanvstrs)))
    
    def sig_helper(self,id,included,named,useName,useAnv):
        [op,typ,anv] = self.nodes[id].label
        if id not in included or op in ['S','SRC','CTRL','CONST']:
            if useName and id not in named:
                named[id] = len(named)+1
            if useName and useAnv:
                return "S:%s:%s:%s" % (typ,"N_%d"%(named[id]),anv)
            elif useName and not useAnv:
                return "S:%s:%s" % (typ,"N_%d"%(named[id]))
            elif not useName and useAnv:
                Assert(False,"This shouldn't be a use case!")
            else:
                return "S:%s" % (typ)
          
        
        usualOrder = True #For commutativity
        if op in ['AND','OR','PLUS','TIMES','XOR','EQ']:
            #use the mode with no name and anv to decide parent order
            parStrs = map(lambda pid: self.sig_helper(pid,included,{},False,False),self.nodes[id].parents)
            if parStrs[0] > parStrs[1]:
                usualOrder = False
            Assert(len(parStrs) == 2,"Parent strings for these can only be size 2")
            
        parids = self.nodes[id].parents
        if not usualOrder:
            parids = parids[::-1] #reversed
        pars=[]
        for parid in parids:
            sigp = self.sig_helper(parid,included,named,useName,useAnv)
            pars.append(sigp)
        sig = "%s(%s)" % (op,",".join(pars))
        return sig
        
    def signature(self,id,included):
        return self.sig_helper(id,included,{},True,False)
    def parseAndAddFile(self,filedag,fileanv):
        with open(filedag,"r") as fdag:
            daglines = filter(lambda x: "dag" not in x and "=" in x,fdag.read().split('\n'))
        #TODO: If anvfile doesn't exist then create it!
        anvs = {}
        if len(self.nodes) == 0:
            offset=0
        else:
            offset=max(self.nodes.keys()) + 1
        with open(fileanv,"r") as fanv:
            anvlines = filter(None,fanv.read().split('\n'))
        for line in anvlines:
            [idstr,anv] = line.split()
            anvs[int(idstr)] = anv
        maxpsize = self.T
        for line in daglines:
            lc = line.split()
            Assert(lc[1] == '=', "Invalid format, line missing = (%s)" %line)
            id = int(lc[0])
            op = lc[2]
            typ = lc[3]
            label = [op,typ,anvs[id]]
            if op in ['S','SRC','CTRL','CONST']:
                name = lc[4] #ignore num_bits for now
                label[0]='S'
                parents = []
            elif op in ['AND','OR','XOR','NOT','NEG','PLUS','TIMES','DIV','MOD','LT','EQ','ARR_R','ARR_W']:
                parents = map(lambda x: int(x), lc[4:])
            elif op == 'ARRACC':
                Assert(lc[5]=='2',"ARRACC format assumption, only binary arracc allowed")
                parents = [lc[4],lc[6],lc[7]]
            elif op in ['ASSERT','D','DST']:
                continue #don't want this in our patterns
            else:
                Assert(False,"op=%s not parsed!"%op)
            Assert(offset+id not in self.nodes,"id=%d already found in self.nodes"%id)
            self.nodes[offset+id] = Node(offset+id,label,map(lambda x: int(x)+offset,parents))
            if len(parents)>maxpsize:
                maxpsize = len(parents)
        if maxpsize != self.T:
            self.T = maxpsize
            print "Max in-degree for any node in the DAG T=%d" %self.T
                
def sampleFromFolder(infldr,outfldr,T,k,M):
    lfiles = filter(lambda x: x[-4:] in ['.dag','.anv'],listdir(infldr))
    bases=set(map(lambda x: x[:-4], lfiles))
    #d()
    SS = SWAPPERSampler(T,100)
    for i in bases:
        fdag = ("%s.dag" %i)
        fanv = ("%s.anv" %i)
        Assert(fdag in lfiles, "DAG file %s.dag not present!"%i)
        Assert(fanv in lfiles, "ANV file %s.anv not present!"%i)
        SS.parseAndAddFile(join(infldr,fdag),join(infldr,fanv))
    print "Parsed %d DAGs: %s" % (len(bases),','.join(lfiles))
    with open(join(outfldr,"output_%d.txt"%k),"w") as outf:
        Samples = SS.sample(k,M,outf)
    with open(join(outfldr,"samples_%d.txt"%k),"w") as outs:
        for (sig,[numPats,patterns]) in sorted(Samples.iteritems(),key=lambda a: -a[1][0]):
            outWANV = SS.outputFormat(patterns)
            outs.write("%s %d %d\n" %(sig,numPats,outWANV.count('##')+1))
            
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-inf",help="Input folder for .dag and .anv files",type=str,required=True)
    parser.add_argument("-of",help="Output folder where to put .dag files",type=str,required=True)
    parser.add_argument("-size",help="size of pattern to be sampled",type=int,required=True)
    parser.add_argument("-N",help="Number of samples to find",type=int,required=True)
    args = parser.parse_args()
    sampleFromFolder(args.inf,args.of,-1,args.size,args.N)