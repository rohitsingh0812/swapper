

Run the Sketch sampler as:

python sketchSampler.py -inf test -of out -size 3 -N 10000

arguments:
  -h, --help  show this help message and exit
  -inf INF    Input folder for .dag and .anv files
  -of OF      Output folder where to put .dag files
  -size SIZE  size of pattern to be sampled
  -N N        Number of samples to search - note that samples != patterns, each pattern can be sampled multiple times


Building your own sampler:

Need to add the following functions to the extension of class DAGSampler:

(1) Parsing: Nodes have ids (integers) and DAGSampler.nodes is a dictionary that maps their ids to Node objects. Node objects have their id, a label object and list of parent ids. PArsing function should be able to parse multiple DAGs and append them to in-memory DAG. Current label object has name of operation (op), type of output (typ) and static analysis values (anv). 
(2) signature function: For a chosen pattern represented by the id of the root node (idr) and list of ids of all nodes included in the pattern (pattern list), what is the signature for this pattern which will be used for aggregation: current Sketch DAG example is a bit complicated and needs a helper function to take care of commutativity.
(3) outputFormat: it takes the list of ALL patterns that were found for a particular signature and outputs summary to a file provided by the file object outf. Note that this can always just be the signature and can ignore the list of all patterns! 
  
  
  