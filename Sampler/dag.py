import random
import sys
def Assert(b,msg):
    if not b:
        print "ERROR: %s" % msg
        d()
        sys.exit(1)
def d():
    import pdb
    pdb.set_trace()
CUTOFFERROR = -1
RESTART=-2
class Node:
    def __init__(self,id,label,parents):
        self.id = id
        self.label = label
        self.parents = parents

class DAGSampler:
    def __init__(self, T, cutoff):
        self.nodes = {}
        self.T = T
        self.cutoff = cutoff
    
    def addNode(self,id,label,parents):
        Assert(id not in nodes,"id=%d was already in nodes!" %d)
        for pid in parents:
            Assert(pid in self.nodes,"Parent id=%d must have been defined by now for id=%d" %(pid,id))
        nodes[id] = Node(id,label,parents)
        
    def signature(self,id, included):
        #Find signature of the dag rooted at node id 
        #and containing only nodes in included list
        if id not in included:
            return "S"
        pars = map(lambda x: self.signature(x,included),self.nodes[id].parents)
        sig = "%s(%s)" % (str(self.nodes[id].label),",".join(pars))
        return sig
    
    def outputFormat(self,patterns):
        #For SWAPPER: merge these based on an ANV-included signature and then output only those instead of all patterns!
        return '##'.join(map(lambda (idr,pattern): self.signature(idr,pattern), patterns))
    
    def sample(self,k,M,outf):
        patternsCtr = 0
        prevCtr = -1
        Samples = {}
        while patternsCtr < M:
            (idr,pattern) = self.getSample(k,0)
            if patternsCtr % 4000 == 0 and patternsCtr > 0 and prevCtr != patternsCtr:
                prevCtr = patternsCtr
                print "Checkpoint: sampled %d patterns." % patternsCtr
            #print patternsCtr
            if idr == RESTART:
                #print "RESTARTING"
                continue
            elif idr == CUTOFFERROR:
                break
            elif idr >= 0:
                #Valid ID
                sig = self.signature(idr,pattern)
                if sig not in Samples:
                    Samples[sig] = [1,[(idr,pattern)]]
                else:
                    Samples[sig][0] +=1
                    Samples[sig][1].append((idr,pattern))
                patternsCtr += 1
        print "Found %d patterns." % len(Samples)
        for (sig,[numPats,patterns]) in sorted(Samples.iteritems(),key=lambda a: -a[1][0]):
            outf.write(self.outputFormat(patterns) + "\n")
        return Samples
    
    def isGoodPattern(self,idr,pattern):
        return True

    def getSample(self,k,ctr): #size=k, T=max indegree for any node
        if ctr > self.cutoff:
            print "Searched too deep with %d restarts for finding one pattern. Exiting." % self.cutoff
            return (CUTOFFERROR,[])
        Assert(k >= 2,"Invalid size of sample needed: %d" % k)
        T=self.T
        r = random.randint(0,len(self.nodes)-1)
        idr = self.nodes.keys()[r]
        pattern = [idr]
        while len(pattern) < k:
            boundaryNodes = []
            boundaryEdges = []
            for pid in pattern:
                for parid in self.nodes[pid].parents:
                    #e=(parid,pid)
                    if parid in boundaryNodes or parid in pattern:
                        boundaryEdges.append(None)
                    else:
                        boundaryNodes.append(parid)
                        boundaryEdges.append(parid)
                        
            while len(boundaryEdges) < len(pattern)*(T-1) + 1:
                boundaryEdges.append(None)
            
            eid = random.randint(0,len(boundaryEdges)-1)
            efrom = boundaryEdges[eid]
            if efrom is None:
                break
            else:
                pattern.append(efrom)
        if not self.isGoodPattern(idr,pattern):
            return (RESTART,[])
        elif len(pattern) == k:
            return (idr,pattern)
        else:
            return (RESTART,[])
                