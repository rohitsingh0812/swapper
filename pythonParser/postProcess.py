#!/usr/bin/env python
import sys
from sets import Set
def main():
	associativeSet = Set(["AND", "PLUS", "TIMES", "OR", "XOR"])
	filename = sys.argv[1]
	outputFileName = sys.argv[2]
	o = open(outputFileName, 'w')
	f = open(filename, 'r')
	lines = f.readlines()
	maxID = 0
	for line in lines:
		splitLine = line.split()
		if int(splitLine[0]) > maxID:
			maxID = int(splitLine[0])
	newID = maxID + 1
	for line in lines:
		splitLine = line.split();
		if splitLine[2] == "ITE":
			splitLine[2] = "ARRAC"
			splitLine.insert(4, "2")
		if splitLine[2] == "GTE":
			newLine = [str(newID), "=", "LT", splitLine[3], splitLine[4], splitLine[5]]
			o.write(" ".join(newLine) + "\n")
			oldLine = [splitLine[0], "=", "NOT BIT", str(newID)]
			o.write(" ".join(oldLine) + "\n")
			newID += 1
			continue
		if splitLine[2] == "LTE":
			#we want to replace GT a b with LT b a
			newLine = [str(newID), "=", "LT", splitLine[3], splitLine[5], splitLine[4]] 
			o.write(" ".join(newLine) + "\n")
			oldLine = [splitLine[0], "=", "NOT BIT", str(newID)]
			o.write(" ".join(oldLine) + "\n")
			newID += 1
			continue
		if splitLine[2] == "GT":
			splitLine[2] = "LT"
			temp = splitLine[5]
			splitLine[5] = splitLine[4]
			splitLine[4] = temp
		if splitLine[2] in associativeSet:
			if len(splitLine) > 6:
				newLine = [str(newID), "=", splitLine[2], splitLine[3], splitLine[len(splitLine) - 1], splitLine[len(splitLine) - 2]]	
				o.write(" ".join(newLine) + "\n")
				newID += 1
				for x in range(len(splitLine) - 7):
					newLine = [str(newID), "=", splitLine[2], splitLine[3], str(newID - 1), splitLine[len(splitLine) - 3 - x]]
					o.write(" ".join(newLine) + "\n")
					newID += 1
				newLine = [splitLine[0], "=", splitLine[2], splitLine[3], str(newID - 1), splitLine[4]]
				o.write(" ".join(newLine) + "\n")
				continue
		line = " ".join(splitLine)
		o.write(line + "\n")
	o.close()
	f.close()
if __name__ == "__main__": main()
