import sys
import re
import math

class SMT_Node_Print():
    dictionary_node_smt = {} # stores map id -> node smt encoding
    srcDict = {} # varName: type
    nodeType = {} # nodeName: type
    assertList = []
    synFunList = []

    arrayTypes = {} # store types of array variables based on preprocess analyis

    def print_init(self):
        pass
        #print "(set-logic LIA)"

    def print_input_vars(self):
        for k in self.srcDict.keys():
            print "(declare-var %s %s)" %(k, self.srcDict[k])
 
    def print_asserts(self):
        for constr in self.assertList:
            print constr

    def print_syn_fun(self):
        for synFun in self.synFunList:
            print synFun

    def print_final(self):
        print "(check-sat)"
        print "(get-model)"


    def isBinOp(self, s):
        if s == "AND" or s == "OR" or s == "XOR" or s == "PLUS" or s == "TIMES" or s == "DIV" or s == "MOD" or s == "LT" or s == "EQ":
            return True

    def isUnaryOp(self, s):
        if s == "NEG" or s == "NOT":
            return True

    def getTypeName(self, s):
        if s == "INT":
            return "Int"
        if s == "FLOAT":
            return "Real"
        elif s == "BOOL":
            return "Bool"
        elif s == "BIT":
            return "Bool"
        elif s == "INT_ARR":
            return "(Array Int Int)"
        elif s == "FLOAT_ARR":
            return "(Array Int Real)"
        elif s == "BOOL_ARR":
            return "(Array Int Bool)"
        else:
            raise Exception("Unsupported Type "+s)

    def getConstTypeName(self, s):
        if s == "INT":
            return "Int"
        if s == "FLOAT":
            return "Real"
        elif s == "BOOL":
            return "Int"
        elif s == "BIT":
            return "Int"
        elif s == "INT_ARR":
            return "(Array Int Int)"
        elif s == "FLOAT_ARR":
            return "(Array Int Real)"
        elif s == "BOOL_ARR":
            return "(Array Int Bool)"
        else:
            raise Exception("Unsupported Type "+s)


    def getOpName(self, s):
        if s == "OR":
            return "or"
        elif s == "AND":
            return "and"
        elif s == "XOR":
            return "xor"
        elif s == "PLUS":
            return "+"
        elif s == "TIMES":
            return "*"
        elif s == "DIV":
            return "div"
        elif s == "MOD":
            return "mod"
        elif s == "LT":
            return "<"
        elif s == "EQ":
            return "="
        elif s == "NOT":
            return "not"
        elif s == "NEG":
            return "-"
        else:
            raise Exception("Error: Operator " + s + " not supported")

    def insert_inputVar(self, varName, varType):
        self.srcDict[varName] = varType

    def insert_nodeType(self, nodeName, nodeType):
        self.nodeType[nodeName] = nodeType

    def insert_dictionary(self, id, str):
        self.dictionary_node_smt[id] = str

    def getNodeConstraintName(self, node):
        return "node"+node

    def getConstValue(self, constVal, constType):
        if constType == "BOOL":
            if constVal == 1:
                return "true"
            else:
                return "false"
        return constVal

    def getArrayTypeFromType(self, nodeType):
        if nodeType.startswith("(Array"):
            return nodeType
        else:
            return "(Array Int %s)" %nodeType

    def getTypeFromArrayType(self, nodeType):
        if nodeType == "(Array Int Int)":
            return "Int"
        elif nodeType == "(Array Int Bool)":
            return "Bool"
        elif nodeType == "(Array Int Real)":
            return "Real"
        else:
            raise Exception("getArrayTypeFromType Error: ArrayType " + nodeType + "not supported")

    def findBinopArgType(self, op, leftType, rightType):
        #if op == "PLUS" or op == "TIMES" or op == "DIV" or op == "MOD" or op == "AND" or op == "OR" or op == "XOR":
         #   return resultType
        argType = leftType
        if argType == "Bool":
            argType = rightType
        return argType

    def typeCast(self, nodeName, expectedType):
        nodeNameType = self.nodeType[nodeName]
        # print "NodeName = " + nodeName +", NodeNameType = " + str(nodeNameType) + ", ExpectedType = " + str(expectedType)
        # print "NodeName = %s, nodeType = %s, expectedType = %s" %(nodeName, nodeNameType, expectedType)
        if nodeNameType == expectedType:
            return nodeName
        elif nodeNameType == "Bool":
            if expectedType == "Int" or expectedType == "Real":
                return "(ite %s 1 0)" % nodeName
            elif expectedType == "(Array Int Int)":
                return "(ite %s ((as const (Array Int Int)) 1) ((as const (Array Int Int)) 0))" % nodeName
            elif expectedType == "(Array Int Bool)":
                return "(ite %s ((as const (Array Int Bool)) %s) ((as const (Array Int Bool)) %s))" % (nodeName, nodeName, nodeName)
            elif expectedType == "(Array Int Real)":
                return "(ite %s ((as const (Array Int Real)) 1) ((as const (Array Int Real)) 0))" % nodeName
            else:
                raise Exception("Error: Type " + expectedType + " not supported")
        elif nodeNameType == "Int":
            if expectedType == "(Array Int Int)":
                return "((as const (Array Int Int)) %s)" % (nodeName)
            elif expectedType == "(Array Int Real)":
                return "((as const (Array Int Real)) %s)" % (nodeName)
            else:
                raise Exception("Error: Type " + expectedType + " not supported")
        # elif nodeNameType == "(Array Int Bool)":
        #     if expectedType == "(Array Int Int)":
        #         return "((as const (Array Int Int)) %s)" % (nodeName)
        #     elif expectedType == "(Array Int Real)":
        #         return "((as const (Array Int Real)) %s)" % (nodeName)
        #     else:
        #         raise Exception("Error: Type " + expectedType + " not supported")
        else:
            print "NodeName = %s, nodeType = %s, expectedType = %s" %(nodeName, nodeNameType, expectedType)
            raise Exception("Error: TypeCast neither bool or int original type")

    def print_node_arr_r(self, vals):
        nodeName = self.getNodeConstraintName(vals[0])
        nodeType =  self.getTypeName(vals[3])
        sexpr = "(declare-const %s %s)" %(nodeName, nodeType)
        self.assertList.append(sexpr)
        sexpr = "(assert (= %s (select " % nodeName
        sexpr += self.typeCast(self.getNodeConstraintName(vals[5]), self.getArrayTypeFromType(nodeType))
        sexpr += " "
        sexpr += self.typeCast(self.getNodeConstraintName(vals[4]), "Int")
        sexpr += ")))"
        self.assertList.append(sexpr)
        self.insert_nodeType(nodeName, nodeType)

    def print_node_arr_w(self, vals):
        nodeName = self.getNodeConstraintName(vals[0])
        nodeType =  self.getTypeName(vals[3])
        sexpr = "(declare-const %s %s)" %(nodeName, nodeType)
        self.assertList.append(sexpr)
        sexpr = "(assert (= %s (store " % nodeName
        sexpr += self.typeCast(self.getNodeConstraintName(vals[5]), self.getArrayTypeFromType(nodeType))
        sexpr += " "
        sexpr += self.typeCast(self.getNodeConstraintName(vals[4]), "Int")
        sexpr += " "
        sexpr += self.typeCast(self.getNodeConstraintName(vals[6]), self.getTypeFromArrayType(nodeType))
        sexpr += ")))"
        self.assertList.append(sexpr)
        self.insert_nodeType(nodeName, nodeType)

    def print_node_arr_create(self, vals):
        #print "vals = ", vals
        nodeName = self.getNodeConstraintName(vals[0])
        nodeType = self.getTypeName(self.arrayTypes[vals[0]]) # read from preprocess
        #nodeType =  self.getTypeName(vals[3])
        sexpr = "(declare-const %s %s)" %(nodeName, nodeType)
        self.assertList.append(sexpr)
        arr_size = int(vals[4])
        for i in range(arr_size):
            valName = self.typeCast(self.getNodeConstraintName(vals[5+i]), self.getTypeFromArrayType(nodeType))
            sexpr = "(assert (= (select %s %s) %s))" % (nodeName, str(i), valName)
            self.assertList.append(sexpr)
        self.insert_nodeType(nodeName, nodeType)

    def print_node_binop(self, vals):
        nodeName = self.getNodeConstraintName(vals[0])
        nodeType =  self.getTypeName(vals[3])
	expectedNodeType = self.findBinopArgType(vals[2], self.nodeType[self.getNodeConstraintName(vals[4])], self.nodeType[self.getNodeConstraintName(vals[5])])
        sexpr = "(declare-const %s %s)" %(nodeName, nodeType)
        self.assertList.append(sexpr)
        op = self.getOpName(vals[2])
        sexpr = "(assert (= %s (%s " % (nodeName, op)
        sexpr += self.typeCast(self.getNodeConstraintName(vals[4]), expectedNodeType)
        sexpr += " "
        sexpr += self.typeCast(self.getNodeConstraintName(vals[5]), expectedNodeType)
        sexpr += ")))"
        self.assertList.append(sexpr)
        self.insert_nodeType(nodeName, nodeType)

    def print_node_uop(self, vals):
        nodeName = self.getNodeConstraintName(vals[0])
        nodeType =  self.getTypeName(vals[3])
        sexpr = "(declare-const %s %s)" %(nodeName, nodeType)
        self.assertList.append(sexpr)
        op = self.getOpName(vals[2])
        sexpr = "(assert (= %s (%s " % (nodeName,op)
        sexpr += self.getNodeConstraintName(vals[4])
        sexpr += ")))"
        self.assertList.append(sexpr)
        self.insert_nodeType(nodeName, nodeType)


    def print_node_src(self, vals):
        nodeName = self.getNodeConstraintName(vals[0])
        nodeType =  self.getTypeName(vals[3])
        sexpr = "(declare-const %s %s)" %(nodeName, nodeType)
        self.assertList.append(sexpr)
        self.insert_inputVar(vals[4],self.getTypeName(vals[3]))
        sexpr = "(assert (= %s %s))" % (nodeName, vals[4])
        self.assertList.append(sexpr)
        self.insert_nodeType(nodeName, nodeType)

    def print_node_ctrl(self, vals):
        nodeName = self.getNodeConstraintName(vals[0])
        nodeType =  self.getTypeName(vals[3])
        sexpr = "(declare-const %s %s)" %(nodeName, nodeType)
        self.assertList.append(sexpr)
        sexpr = "(declare-const "
        sexpr += vals[4]
        sexpr += "  "
        # sexpr += " ()  "
        if vals[3] == "BOOL":
             sexpr += "Bool)"
        elif vals[3] == "INT":
            sexpr += " Int)"
        #     nbits = int(math.pow(2,int(vals[5])))
        #     for i in range(nbits):
        #         sexpr += str(i)
        #         sexpr += " "
        #     sexpr += str(nbits)
        #     sexpr += "))"
        # sexpr += ")"
        # # print sexpr
        # #self.insert_dictionary(vals[0], vals[4])
        sexpr1 = "(assert (= %s %s))" % (nodeName, vals[4])
        self.assertList.append(sexpr1)
        self.synFunList.append(sexpr)
        self.insert_nodeType(nodeName, nodeType)

    def print_node_dst(self, vals):
        self.insert_dictionary(vals[0], vals[2])

    def print_node_ufun(self, vals):
        self.insert_dictionary(vals[0], vals[2])

    def print_node_arracc(self, vals):
        nodeName = self.getNodeConstraintName(vals[0])
        nodeType =  self.getTypeName(vals[3])
        sexpr = "(declare-const %s %s)" %(nodeName, nodeType)
        self.assertList.append(sexpr)
        size = int(vals[5])
        for i in range(size):
            sexpr = "(assert (=> "
            sexpr += "(= " + self.typeCast(self.getNodeConstraintName(vals[4]),"Int") + " " + str(i) + ")"
            sexpr += " "
            sexpr += "(= %s " % nodeName
            sexpr += self.typeCast(self.getNodeConstraintName(vals[6+i]),nodeType)
            sexpr += ")))"
            self.assertList.append(sexpr)
        self.insert_nodeType(nodeName, nodeType)

    def print_node_const(self, vals):
        nodeName = self.getNodeConstraintName(vals[0])
        nodeType =  self.getTypeName(vals[3])
        sexpr = "(declare-const %s %s)" %(nodeName, nodeType)
        self.assertList.append(sexpr)
        sexpr = "(assert (= %s %s))" % (nodeName, self.getConstValue(vals[4], vals[3]))
        self.assertList.append(sexpr)
        self.insert_nodeType(nodeName, nodeType)

    def print_node_arrass(self, vals):
        nodeName = self.getNodeConstraintName(vals[0])
        nodeType =  self.getTypeName(vals[3])
        sexpr = "(declare-const %s %s)" %(nodeName, nodeType)
        self.assertList.append(sexpr)
        sexpr = "(assert (%s (ite " % (self.getNodeConstraintName(vals[0]))
        sexpr += " (= "
        sexpr += self.typeCast(self.getNodeConstraintName(vals[4]),"Int")
        sexpr += " "
        sexpr += self.typeCast(self.getNodeConstraintName(vals[6]), "Int")
        sexpr += ") "
        sexpr += self.typeCast(self.getNodeConstraintName(vals[7]), nodeType)
        sexpr += " "
        sexpr += self.typeCast(self.getNodeConstraintName(vals[8]), nodeType)
        sexpr += ")))"
        self.assertList.append(sexpr)
        self.insert_nodeType(nodeName, nodeType)

    def print_node_actrl(self, vals):
        self.insert_dictionary(vals[0], vals[2])

    def print_node_assert(self, vals):
        sexpr = "(declare-const %s %s)" %(self.getNodeConstraintName(vals[0]), "Bool")
        self.assertList.append(sexpr)
        sexpr = "(assert  "
        sexpr += self.getNodeConstraintName(vals[3])
        sexpr += ")"
        #self.insert_dictionary(vals[0], sexpr)
        self.assertList.append(sexpr)

    def print_node(self, node):
        #print "elements = %s" % node
        elements = re.split(' |\n|\r',node)
        #print "node name = %s" % elements[2]
        if elements[2] == "ARR_R":
            self.print_node_arr_r(elements)
        elif elements[2] == "ARR_W":
            self.print_node_arr_w(elements)
        elif elements[2] == "ARR_CREATE":
            self.print_node_arr_create(elements)
        elif self.isUnaryOp(elements[2]):
            self.print_node_uop(elements)
        elif elements[2] == "S":
            self.print_node_src(elements)
        elif elements[2] == "CTRL":
            self.print_node_ctrl(elements)
        elif elements[2] == "DST":
            self.print_node_dst(elements)
        elif elements[2] == "UFUN":
            self.print_node_ufun(elements)
        elif elements[2] == "ARRACC":
            self.print_node_arracc(elements)
        elif elements[2] == "CONST":
            self.print_node_const(elements)
        elif elements[2] == "ARRASS":
            self.print_node_arrass(elements)
        elif elements[2] == "ACTRL":
            self.print_node_actrl(elements)
        elif elements[2] == "ASSERT":
            self.print_node_assert(elements)
        elif self.isBinOp(elements[2]):
            self.print_node_binop(elements)
        else:
            raise Exception("not a valid node name in the dag: ")
        
    def print_smt(self,dagnodes):
        #count = 1
        for node in dagnodes:
            #print "count =" + str(count)
            #count = count + 1
            self.print_node(node)

    def preprocess(self, dagnodes):
        for line in dagnodes:
            elements = re.split(' |\n|\r',line)
            if elements[2] == "ARR_CREATE":
                self.arrayTypes[elements[0]] = elements[3]
            if elements[2] == "ARR_W":
                self.arrayTypes[elements[5]] = elements[3]
            if elements[2] == "ARR_R":
                self.arrayTypes[elements[5]] = elements[3]


        

def main(argv):
    filename = argv[0]
    f = open(filename, 'r')
    dagnodes = f.readlines()
    smt_node_print = SMT_Node_Print()
    smt_node_print.preprocess(dagnodes[1:])
    smt_node_print.print_smt(dagnodes[1:])
    smt_node_print.print_init()
    print ""
    smt_node_print.print_syn_fun()
    print ""
    smt_node_print.print_input_vars()
    print ""
    smt_node_print.print_asserts()
    print ""
    smt_node_print.print_final()
#    for result in smt_node_print.dictionary_node_smt.keys():
#        print "%s: %s" %(result, smt_node_print.dictionary_node_smt[result])
    f.close()

if (__name__=='__main__'):
    main(sys.argv[1:])
