##DAG(Directed Acyclic Graph) Tools
Author: wrv
CurDate: 4/8/2015
--------------

Contains tools to parse and operate on dag files.

# DAG.py
DAG Library that contains Graph class as well as Node class to represent DAGs. 
Methods:
Graph
	printSketchDAGStyle()
	topSort()
	printSubGraph()
	randomSample()
	getSignature()
	evalGraph() //not implemented
Node
	addChild()
	clone()

# Parser.py
Takes in a sketch-DAG-style file and parses it into memory. 

Usage: Parser.py <file to parse 1> <2> ...

# PatternFinding.py
Takes in a directory with sketch-DAG-style files, parses them, and searches for patterns within the files. Outputs an overview with signature, count, and locations, as well as a directory with pattern files.

Usage: PatternFinding -n <x> -l <y> -f </lol/> -t <dag> -o <outputx>
	-n <number of patterns ex:10000> 
	-l <length of sample to get ex:3 or a range 3-6> 
	-f <folder to explore (full path)> 
	-t <the file type ex: dag, dag.2, dag2>
	-o <output folder for results (relative path, will be written in full path)>
	[-v for verbosity]
	[-d for debugging]


# RuleSplitter.py
Takes in segis generated rule files, parses it, and splits it into it's own directory with left hand side, predicate, and right hand side in seperate files. 

Usage: RuleSplitter.py <rule file to split 1> <2> ...

# Other Files
Any other remaining unexplained files were used for testing.