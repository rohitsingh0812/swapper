#!/usr/bin/env python
import multiprocessing, sys, subprocess,os, MySQLdb,time, re
from subprocess import check_output
from os import listdir, makedirs, rmdir
from os.path import join, exists
import traceback
from PatternFinding import parseDags, getSamples
from RuleSplitter import rulesToFileWriter, writeToFile
from languageTranslators import dagToSketch as ds
import shutil
import dill
from pathos.multiprocessing import ProcessingPool,cpu_count
usageString = '''Usage I: RuleOrganizer -n <x> -l <y> -f </lol/> -t <fileextension> -o <outputx> -m MakefileGen
Usage II: RuleOrganizer -mode RuleSplitting -file <rulefilename> -folder <rulefolder>
    -m <mode = MakefileGen or RuleSplitting>
    -file (RuleSplitting mode)
    -folder (RuleSplitting mode & SketchRuleGen mode) 
    -n <number of samples ex:10000> (MakefileGen/PatternFinding mode) <number of top n patterns to use! SketchRuleGen mode>
    -l <length of sample to get ex:3 or a range 3-6> (MakefileGen/PatternFinding mode) 
    -f <SEARCH folder to explore (full path)> (MakefileGen/PatternFinding mode) / file with all signatures in SketchRuleGen mode
    -t <the file type ex: dag, 2, dag2. Only put extension (checks the end)> (MakefileGen/PatternFinding mode)
    -valr <max size of rhs dag: if we specify k then it looks for dags of sizes 1 to k, including k> (MakefileGen mode)
    -o <output folder for rules (relative path, will be written in full path)> (MakefileGen mode)
    -astinc <relative include position of ast.skh from action folder <-f>/action> (SketchRuleGen mode)
    '''


def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

# If there is malformed input or some user-based error then print this
def printUsageAndExit(missingElem):
    print "Error: " + missingElem
    print usageString
    sys.exit(0)

def get_sig_with_anv(outfolder,filename,lhs,curDagIDs):
    params_to_cegis = ["cegis","--signature-with-anv",join(outfolder,filename),str(lhs)]
    params_to_cegis.extend(curDagIDs)
    params_to_cegis.append("lol")
    signature = check_output(params_to_cegis) 
    #print signature
    return signature
    
def getSig(ruleFolder):
    params_to_cegis = ["cegis","--signature",ruleFolder,"lol"]
    l = (check_output(params_to_cegis)).split('\n') 
    assert(len(l) ==3 and l[2] == "" and "Generating" in l[0])
    signature = l[1] #sig.aux already created by cegis!
    #print signature
    return signature
    
def writeRulesWithLocking(sigsWithANV,actionRulesFolder,rulesFolder,idd):
    if not os.path.exists(actionRulesFolder):
        return
    ruleDirs = [e for e in os.listdir(actionRulesFolder) if is_int(e)]
    done = False
    if not ruleDirs:
        done = True

    while not done:
        try:
            makedirs(join(rulesFolder, "split.lock"))
            db = MySQLdb.connect(host="localhost",user="root",passwd="",db="rewriterules")
            cur = db.cursor()
            for curRule in ruleDirs:
                sig = getSig(join(actionRulesFolder,str(curRule)))
                with open(join(join(actionRulesFolder,str(curRule)),"info.aux"),"w") as finfo:
                    finfo.write("action_" + str(idd) + "##".join(sigsWithANV) )
                #First we want to make sure that the rule doesn't exist
                searchStatement = "SELECT folder, rulesignature FROM rules WHERE folder = '%s' AND rulesignature = '%s';" % (rulesFolder, sig)
                cur.execute(searchStatement)
                result = cur.fetchone()
                if result is None:
                    #If not, we write it into the database and write the file to rules
                    statement = "INSERT INTO rules (folder, rulesignature, outfilename) values ('%s', '%s', '%s');" %(rulesFolder,sig,str(idd))
                    cur.execute(statement)
                    #Now we add the rule to rulesFolder
                    newRule = str(max([int(e) for e in os.listdir(rulesFolder) if is_int(e)] + [0]) + 1)
                    #copy from actionRulesFolder/curRule to rulesFolder/newRule
                    shutil.copytree(join(actionRulesFolder,curRule),join(rulesFolder,newRule))
                else:
                    print "Rule found already in DB: " + sig
            cur.close()
            db.commit()
            db.close()

            rmdir(join(rulesFolder, "split.lock"))
            done = True
        except OSError as e:
            print "Waiting on split.lock :" + str(idd)
            time.sleep(2);
            continue
    return
def skgen((sigsWithANV,actionFolder,relativeInclude,finalRulesFolder,idd)):
    try:
        sys.__stdout__.write("Starting %d."%(idd)+"\n")
        sys.stdout = open(join(actionFolder,"run.out"),"w")
        ds.parseSig(sigsWithANV,actionFolder,relativeInclude,False)
        #add rules with locking to finalRulesFolder
        #generated rules are in actionFolder/"rules"
        writeRulesWithLocking(sigsWithANV,join(actionFolder,"rules"),finalRulesFolder,idd)
        #sys.__stdout__.write("Done %d."%(idd) + "\n") 
        #delete action tmp folder
        tmpFolder = join(actionFolder,"tmp")
        if os.path.exists(tmpFolder):
            shutil.rmtree(tmpFolder)
    except Exception, err:
        sys.stderr.write('Case %d\n' % idd)
        traceback.print_exc()
        raise Exception
DEBUG = False
def SketchRuleGen(patterns,outfolderdags,finalRulesFolder,relativeInclude):
    #Take anv signature and generate rules in a sub-folder and transfer them to ruleFolder
    print "Generating Sketch based Rules!"
    listArgsSkGen = []
    
    #sigs = [pts[6] for pts in patterns]
    
    for sigsWithANV in patterns:
        idd = len(listArgsSkGen)
        actionFolder =join(outfolderdags,"action_" + str(idd)) 
        ds.createDir(actionFolder)
        listArgsSkGen.append((sigsWithANV,actionFolder,relativeInclude,finalRulesFolder,idd))

    if(DEBUG):
        elts =listArgsSkGen[58]
        skgen(elts)
        sys.exit(0)
    else:
        ncpus = cpu_count()
        p =  ProcessingPool(nodes=min(20,ncpus-2))
        p.map(skgen,listArgsSkGen)
    sys.stdout = sys.__stdout__
    #Can verify rules here too
    print "Patterns handled: ",len(patterns)
    



##
#Generates Makefile in outfolder with appropriate commands 
#
def MakefileForRuleGen(patterns,outfolderdags):
    MakeFile = 'all:'
    for i in range(len(patterns)):
        MakeFile += ' t' + str(i+1)
        
    MakeFile += '\n\techo "done all"'
    counter=0
    for pts in patterns:
        counter += 1
        outfolder = pts[0]
        filename = pts[1]
        curDagIDs = pts[2]
        i = pts[3]
        lhs = pts[4]
        ruleOutName = pts[5] 
        curSampleSig = pts[6] 
        curdagLocations = ' '.join(curDagIDs)
        curdagLocationsUnder = '_'.join(curDagIDs)
        
        #To decide if we want to add this one or not - 
        #we can run the signature-with-anv call to cegis 
        #and check if the signature has already been seen
        signature = str(counter) #get_sig_with_anv(outfolder,filename,lhs,curDagIDs)
        
        #print "Starting Rule Generation of " + filename
        #--genrulesdaganvstr "(LT|(AND|(LT|(S:N_1:BOOL:L(|0|))|(S:N_2:INT:T)|)|(S:N_3:BOOL:R(0-1))|)|(S:N_2:INT:T)|)" 2 "D:\tmp.txt" lol
        ruleFileName = join(outfolder,'run') + "_" + str(counter) + ".out"
        rulesFolder = join(outfolder,ruleOutName)
        jnkFile = join(outfolder,'output') +"_" +str(counter) +".jnk"
        command0 = "echo '" +str(counter) + "' #" + curSampleSig
        command1 = "-@(cegis --genrulesdaganvstr " + '"' + curSampleSig + '" ' + str(i) + ' "' + ruleFileName + '" ' + " lol 2>&1) > " + jnkFile
        #second command for pattern splitting
        #delete jnk file if there's no "Error" or "Unusual" 
        command1half = "-@(if [ -z `grep -E \"Unusual|Error\" " + jnkFile + "` ]; then rm " + jnkFile+"; fi)"
        command2 = "-@(if [ -s \"" + ruleFileName +"\" ]; then RuleOrganizer.py -m RuleSplitting -file " + ruleFileName + " -folder " + rulesFolder + "; else rm " + ruleFileName + "; fi)"
        MakeFile += "\nt" + str(counter) + ":\n\t" + command0 + "\n" 
        command3 = "echo 'DONE " +str(counter) + "'" 
        MakeFile += "\t" + command1 + "\n\t" + command1half + "\n\t" + command2 + '\n\t' + command3 + '\n'
    outfile = file(join(outfolderdags, "Makefile"), "w")
    print "Patterns: ",len(patterns)
    outfile.write(MakeFile)
    

def SplitRules(ruleFileName,rulesFolder):
    parsedRules = rulesToFileWriter(rulesFolder,[ruleFileName],False)
    #print ruleFileName,":",rulesFolder,parsedRules
    done = False
    while not done:
        try:
            makedirs(join(rulesFolder, "split.lock"))
            db = MySQLdb.connect(host="localhost",user="root",passwd="",db="rewriterules")
            cur = db.cursor()
            for curDagRules in parsedRules:
                print "Curdag Rules"
                for rule in curDagRules:
                    print "looking at rule"
                    print rule
                    #time.sleep(50)
                    #First we want to make sure that the rule doesn't exist
                    searchStatement = "SELECT folder, rulesignature FROM rules WHERE folder = '%s' AND rulesignature = '%s';" % (rulesFolder, rule[3])
                    cur.execute(searchStatement)
                    result = cur.fetchone()
                    if result is None:
                        #If not, we write it into the database and write the file to rules
                        statement = "INSERT INTO rules (folder, rulesignature, outfilename) values ('%s', '%s', '%s');" %(rulesFolder,rule[3],ruleFileName)
                        cur.execute(statement)
                        #Now we add the rules to file
                        writeToFile(rulesFolder, [[rule]], ruleFileName)
                    else:
                        print "Rule found already in DB: " + rule[3]
            cur.close()
            db.commit()
            db.close()

            rmdir(join(rulesFolder, "split.lock"))
            done = True
        except OSError as e:
            print "Waiting on split.lock :" + ruleFileName
            time.sleep(2);
            
            continue
    return

        
##
# RuleGen takes in a filename as well as the dagIDs and calls cegis to generate the rules
# Rule generation is created within a separate process that will a
# should come in like so: [16,19,29, prime_cone_sat_2.smt2.dag]
def RuleGen(pts):
    outfolder = pts[0]
    filename = pts[1]
    curDagIDs = pts[2]
    i = pts[3]
    lhs = pts[4]
    ruleOutName = pts[5] 
    curdagLocations = ' '.join(curDagIDs)
    curdagLocationsUnder = '_'.join(curDagIDs)
    print "Starting Rule Generation of " + filename
    
    command = "cegis --genrulesbigdag " + join(outfolder,filename) + " " + str(i) + " " + str(lhs) + " " + curdagLocations + " lol"
    print command
    #try:
    #f = file("RuleMakeFile", "a")
    #f.write(command + '\n')
    os.system(command)
    #except OSError:
    #    print "Oh no!!"

    print "Splitting Rules for " + filename
    ruleFileName = join(outfolder,filename) + "_" + curdagLocationsUnder + "_" + str(i) + ".out"
    rulesFolder = join(outfolder,ruleOutName)
    
    #print rulesFolder
    #print ruleFileName
    parsedRules = rulesToFileWriter(rulesFolder,[ruleFileName],False)
    print parsedRules
    for curDagRules in parsedRules:
        print "Curdag Rules"
        for rule in curDagRules:
            #print "looking at rule"
            #print rule
            #time.sleep(50)
            #First we want to make sure that the rule doesn't exist
            searchStatement = "SELECT folder, rulesignature FROM rules WHERE folder = %s AND rulesignature = %s;" % (outfolder, rule[3])
            cur.execute(searchStatement)
            result = cur.fetchone()
            if result is None:
                #If not, we write it into the database and write the file to rules
                statement = "INSERT INTO rules (folder, rulesignature) values (%s, %s);",(outfolder,rule[3])
                cur.execute(statement)
                #print statement 
                #Now we add the rules to file
                writeToFile(rulesFolder, [[rule]])
            else:
                print "Found rule already in DB!: " + rule[3]
            cur.close()
            db.commit()
    db.close()
    return

def d():
    import pdb
    pdb.set_trace()
    


def tanvr(file="/home/ubuntu/Dropbox/EXP/AutoGrader/CFGSK1/STEMP/sigs_with_anvs.txt"):
    #from DAGTools import RuleOrganizer as ro
    #ro.testANVRemoval("/home/ubuntu/Dropbox/EXP/AutoGrader/CFGSK1/STEMP/sigs_with_anvs.txt")
    sys.stdout.write(file+"\n")
    with open(file,"r") as ft:
        L = list(ft)
        CTR = 0
        for sig in L:
            try:
                print sig, ds.removeANVfromSig(sig)
            except Exception, err:
                d()
                raise
            
def getPattern(n, thel, folder, filetype,ruleOutName,maxr,minr):
    MODVAL=100
    seen = {}
    parsed = parseDags(folder=folder,filetype=filetype)
    
    #pool = multiprocessing.Pool(20)
    print  str(len(parsed)) + " DAG(s) parsed from: " + folder
    #print parsed
    patterns =[]
    assert(len(thel) == 1)
    l=thel[0]
    ctr=0
    while ctr < n:
        #What result looks like:
        ## [('LT(S()|PLUS(TIMES(S()|S())|S()))', [1, [[16,19,29, prime_cone_sat_2.smt2.dag]]])]
        curSampleMain = getSamples([l], n=1, dags=parsed, aggregate=False)[0]
        curSampleSig = curSampleMain[0]
        curSample = curSampleMain[1][1][0]
        filename = curSample[-1] + filetype
        curDagIDs = curSample[0:-1]
        
        sig_anvs = curSampleSig.strip()
        sig = ds.removeANVfromSig(sig_anvs)
        if sig not in seen:
            seen[sig] = [1,set([sig_anvs])]
        elif(sig in seen):
            seen[sig][0] = seen[sig][0] + 1 #increase the count
            seen[sig][1].add(sig_anvs) #add sig with ANV
        ctr=ctr+1
    
    outfile_sig = file(join(folder, "sigs_with_anvs.txt"), "w")
    for (cnt,seen_anvs) in sorted(seen.values(),key=lambda x: -x[0]):
        outfile_sig.write("##".join(list(seen_anvs)))
        outfile_sig.write("\n")
        #print cnt, len(seen_anvs)
    print "Found %d patterns!" %(len(seen))
    return seen    
    
def splitArgs(args):
    #we need to check the proper arguments were passed in
    if '-m' not in args:
        printUsageAndExit("-m")
    mode = args[args.index('-m')+1] #MakefileGen or RuleSplitting
    if mode not in ['MakefileGen','SketchRuleGen','RuleSplitting','PatternFinding']:
        print "Invalid mode value: "+ mode
        sys.exit(0)
    if mode == 'MakefileGen':
        print "Need to rewrite/bring back older Pattern Finding for this!"
        sys.exit(1)
        if '-n' not in args:
            printUsageAndExit("-n")
        if '-l' not in args:
            printUsageAndExit("-l")
        if '-f' not in args:
            printUsageAndExit("-f")
        if '-o' not in args:
            printUsageAndExit("-o")
        if '-t' not in args:
            printUsageAndExit("-t")
        if '-valr' not in args:
            printUsageAndExit("-valr")
        
        maxr = int(args[args.index('-valr')+1]) #maximum size of rhs
        minr = maxr #maximum size of rhs
        if(minr <= 0):
            print "invalid size ranges for rhs" 
            sys.exit(0)
        n = int(args[args.index('-n')+1]) #number of patterns to explore

        l = args[args.index('-l')+1] #length of subdag nodes to sample
        if len(l) == 1:
            l = [int(l)]
        else:
            brokenl = l.split('-')
            l = range(int(brokenl[0]),int(brokenl[1])+1)

        filetype = args[args.index('-t')+1] #filetype of dags we want to test
        folder = args[args.index('-f')+1] #Folder we will be exploring
        outname = args[args.index('-o')+1] #Folder we will be outputting our rules
        print "Beginning Pattern Finding"
        patterns = getPattern(n, l, folder,filetype,outname,maxr,minr)
        MakefileForRuleGen(patterns,folder)
    elif mode == 'PatternFinding':
        # ~/smt-optim-sos/DAGTools/RuleOrganizer.py -m PatternFinding -n 100 -l 3 -f "/home/ubuntu/Dropbox/EXP/AutoGrader/CFGSK1/SEARCH" -t dag -valr 2 -o rules
        for opt in ['-n','-l','-f','-o','-t','-valr']:
            if opt not in args:
                printUsageAndExit(opt)
        
        maxr = int(args[args.index('-valr')+1]) #maximum size of rhs
        minr = maxr #maximum size of rhs
        if(minr <= 0):
            print "invalid size ranges for rhs" 
            sys.exit(0)
        n = int(args[args.index('-n')+1]) #number of patterns to explore

        l = args[args.index('-l')+1] #length of subdag nodes to sample
        if len(l) == 1:
            l = [int(l)]
        else:
            brokenl = l.split('-')
            l = range(int(brokenl[0]),int(brokenl[1])+1)

        filetype = args[args.index('-t')+1] #filetype of dags we want to test
        folder = args[args.index('-f')+1] #Folder we will be exploring
        outname = args[args.index('-o')+1] #Folder we will be outputting our rules
        print "Beginning Pattern Finding"
        patterns = getPattern(n, l, folder,filetype,outname,maxr,minr)
        
        with open(join(folder,"patterns.txt"),"w") as fpat:
            for (pat,(cnt,seen_anvs)) in sorted(patterns.iteritems(),key=lambda x: -x[1][0]):
                fpat.write("%s %d %d\n" %(pat,cnt, len(seen_anvs)))
        #SketchRuleGen(patterns,folder,finalRulesFolder,relativeInclude)
    elif mode == 'SketchRuleGen':
        #~/smt-optim-sos/DAGTools/RuleOrganizer.py -m SketchRuleGen -f "/home/ubuntu/Dropbox/EXP/AutoGrader/CFGSK1/SEARCH" -astinc "../../../../../../smt-optim-sos/sketchlib/example/" -folder "/home/ubuntu/Dropbox/EXP/AutoGrader/CFGSK1/ruleRepo/"
        for opt in ['-file','-folder','-astinc','-n']:
            if opt not in args:
                printUsageAndExit(opt)
        filename = args[args.index('-file')+1] 
        finalRulesFolder = args[args.index('-folder')+1]
        relativeInclude = args[args.index('-astinc')+1]
        patterns = []
        N=int(args[args.index('-n')+1])
        with open(filename,"r") as myfile:
            head = [next(myfile) for x in xrange(N)]
        #with open(filename,"r") as fpat:
        patterns= map(lambda x: filter(None,x.replace('\n','').split("##")),head)
        assert(len(patterns) == N)
        searchFolder = os.path.dirname(filename)
        SketchRuleGen(patterns,searchFolder,finalRulesFolder,relativeInclude)
    elif (mode == 'RuleSplitting'):
        if '-file' not in args:
            printUsageAndExit("-file")
        if '-folder' not in args:
            printUsageAndExit("-folder")
        ruleFileName = args[args.index('-file')+1]
        rulesFolder = args[args.index('-folder')+1]
        SplitRules(ruleFileName,rulesFolder)
        
if __name__ == '__main__':
    splitArgs(sys.argv[1:])
