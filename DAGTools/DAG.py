#!/usr/bin/python
import random
from copy import deepcopy

SOURCES = ["S", "SRC", "CONST"]
INTOPS = ["PLUS", "TIMES", "DIV", "MOD", "LT", "LTE", "GTE", "GT", "EQ", "ARR_R", "ARR_CREATE", "ARR_W", "ITE", "NEG"]
BOOLOPS = ["NOT", "AND", "OR", "XOR"]
COMMOPS = ['OR', 'AND','XOR','PLUS','TIMES','EQ'] #Commutative ones
###
# class Graph
# 
# Our container for our DAG which will allow us to perform various operations
# 
# Most importantly we want to be able to evaluate the graph based on a set of inputs and 
# organize the graph using a topological sort. Some other niceties that we can throw in 
# are graph viewing using pygraphviz and maybe later on random sampling and common 
# subexpression elimination.
# 
###
class Graph:

  ###
  # Graph Initializer
  #
  # NodeList : the list of nodes for our graph
  #
  # This takes in a list of nodes and creates a graph element based on the Nodes.
  def __init__(self, NodeList):
    self.nodes = NodeList
    self.sourceNodes = []
    self.nodeDict = {}
    
    #First we add the list of children to each node
    for node in self.nodes:
      self.nodeDict[node.uid] = node
      #We want to iterate through the parents to create the children
      for parent in node.parents:
        #We might as well keep track of the source nodes. They have no parents, but are inputs
        if node.operation in SOURCES:
          self.sourceNodes.append(node)
        else:
          #Now we update said parent by iterating through the list until we find the node with the same id
          for node2 in self.nodes:
            if node2.uid == parent:
              node2.addChild(node)
              break
    

  ###
  # evalGraph(self, inputs)
  #
  # inputs : the list of inputs to our graph to begin evaluation
  # 
  # Given a set of inputs to match the number of source nodes
  # we evaluate the graph on the inputs in order to see if it
  # matches out.
  #
  # Returns true or false based on inputs
  def evalGraph(self, inputs):
    pass


  ##
  # printSketchDAGStyle(self, NodeList)
  # 
  # We print off the graph that is given or the current graph in the given Sketch DAG style:
  # 0 = S BIT x0
  # 1 = S INT x1
  # 3 = S BIT x3
  # 6 = S INT x6
  # 8 = S BIT x8
  # In order to this we need to create the source nodes for the given NodeList if it doesn't have it
  #
  def printSketchDAGStyle(self, NodeList=None, verbose=True, constToSrc=False):

    if NodeList:
      curNodeList = NodeList[:]
    else:
      curNodeList = self.nodes[:]

    curNodeUIDs = []
    for node in curNodeList:
      curNodeUIDs.append(node.uid)

    #If it's not the current DAG then we go through this process
    if NodeList:
      curNodeLength = len(curNodeList)
      j = 0
      i = 0
      while j < curNodeLength:
        node = curNodeList[j]
        for parent in node.parents:
          if parent not in curNodeUIDs:
            if parent.isdigit():
              x=Node(parent, 'S', 'INT', ['SRC_' + str(i)])
              x.anvsig = self.nodeDict[parent].anvsig
              curNodeList.append(x)
              i += 1
              curNodeUIDs.append(parent)
        j += 1

      curNodeList = self.topSort(curNodeList)
    
    outString = ""
    i = int(max(curNodeUIDs,key=int))+1

    for node in curNodeList:
      op = node.operation
      par = node.parents
      if constToSrc:
        if op == 'CONST':
          op = 'S'
          par = ['SRC_'+str(i)]
          i += 1

      if node.optionalbits:
        tempStr = (node.uid + ' = ' + op + ' ' + node.outputtype + ' ' + ' '.join(par) + ' ' + ' '.join(node.optionalbits))
        outString += tempStr + '\n'
        if verbose:
          print tempStr      
      else:
        tempStr = (node.uid + ' = ' + op + ' ' + node.outputtype + ' ' + ' '.join(par))
        outString += tempStr + '\n'
        if verbose:
          print tempStr

    return outString

  ## topSort(self)
  # Takes our current graph and topologically sorts it. IDs should adapt to their position in the list
  #
  # Returns a new graph that has been topologically sorted
  # Source: http://en.wikipedia.org/wiki/Topological_sorting 
  def topSort(self, NodeList=None, verbose=False):
    #Empty list that will contain the sorted elements
    sortedList = []

    #Set of all nodes with no incoming edges
    if NodeList:
      #we have to find all source nodes in NodeList
      tempUID = []
      #we gather all UIDs in NodeList
      for n in NodeList:
        tempUID.append(n.uid)
      if verbose:
        print tempUID
      NodeListSauce = []
      #we keep track of Nodes that don't have incoming edges
      for n in NodeList:
        i = 0
        for p in n.parents:
          if p not in tempUID:
            i += 1
        #if none of the parents are in our list then add it to sauce
        if i == len(n.parents):
          NodeListSauce.append(n)

      sauce = NodeListSauce
    else:
      sauce = self.sourceNodes[:]


    seenUIDs = []
    #while S is non-empty do
    while len(sauce) > 0:
      #remove a node n from S
      curnode=sauce.pop(0)
      #add n to tail of L
      sortedList.append(curnode)
      seenUIDs.append(curnode.uid)
      
      #for each node m with an edge e from n to m do
      for child in curnode.children:
        explore = True
        #if m has no other incoming edges then
        for childparents in child.parents:
          #If the childparents have not been seen then other edges exist
          if childparents not in seenUIDs:
            explore = False
        #insert m into S
        if explore:
          sauce.append(child)

    if verbose:
      for node in sortedList:
        print node
    return Graph(sortedList)


  ###
  # printSubGraph(self, NodeList)
  # 
  # NodeList : list of near nodes that we want to create a subgraph for
  #
  # Prints out the relationship between the nodes that we have given and
  # if a parent node does not exist it creates a new source node to handle it
  def printSubGraph(self, NodeList, verbose=True):
    #Used to keep track of the final nodes
    subGNodes = []
    #Used to keep track of the UIDs of each node we have
    NodeListUIDS = []
    for node in NodeList:
      if verbose:
        print "\n<-------SubGraph Node"
        print node
        print "----------------->\n"
      NodeListUIDS.append(node.uid)
      subGNodes.append(deepcopy(node.clone()))

    srccount = 0
    newSources = {}
    newMax = int(max(NodeListUIDS, key=int)) + 1
    #We need to create new source nodes if the parent node does not exist
    for node in subGNodes:
      #iterate through the parents
      if node.operation not in SOURCES:
        for i in range(len(node.parents)):
          curParent = node.parents[i]
          if curParent not in NodeListUIDS:
            prevCurParent = curParent
            if curParent in newSources.keys():
              #since we're increasing now, we should begin to grow above the max value
              #curParent = str(newMax)
              #node.parents[i] = curParent
              #newMax += 1
              srcName = newSources[curParent].parents[0]
            else:
              srcName = "Src_" + str(srccount)
              newSources[curParent] = Node(curParent, "S", self.nodeDict[prevCurParent].outputtype, [srcName])
              newSources[curParent].anvsig = self.nodeDict[curParent].anvsig
              srccount += 1
    
    #We want our sources to be in numerically sorted order for easier reading
    newSorted = []
    for elem in sorted(newSources.keys()):
      newSorted.append(newSources[elem])

    subGNodes = newSorted + subGNodes
    if verbose:
      for node in subGNodes:
        print node

    return Graph(subGNodes)

  ##
  # randomSample(self)
  # 
  # Chooses a random node and extends the parents up until we have length number of nodes
  def randomSample(self, length, verbose=False):
    #Error checking to make sure we don't get stuck in an infinite loop
    if length > len(self.nodes):
      print "Length is longer than number of nodes"
      return Graph([])

    success = False
    sampled = []

    while not success:
      randnum = random.randint(0, len(self.nodes)-1)
      if verbose:
        print "Random number: " + str(randnum)

      rootNode = self.nodes[randnum]
      sampled.append(rootNode)
      i = 0

      while len(sampled) < length:
        try:
          curNode = sampled[i]
        except:
          #if we didn't reach our goal, we can flip a coin to try the previous node again or start over
          randnum = random.randint(0,1)
          if randnum == 1:
            i -= 1
            curNode = sampled[i]
          else:
            #if we lose the coin flip we start over
            sampled = []
            break

        #print curNode
        if curNode.operation in SOURCES or curNode.operation == 'ASSERT' or curNode.operation == 'ARR_CREATE':
          #we want to check if there are more values in sampled to check
          #if (1+i) < len(sampled):
          #  i += 1
          #else:
            #time to restart since we didn't reach our goal
          #Reset since we don't want any source nodes or asserts
          sampled = []
          break
        else:
          #We have to check to see if both parents are already included, if so we go to the next node

          for j in range(len(curNode.parents)):
            parent = curNode.parents[j]

            randnum = random.randint(0,1)
            if randnum == 1:
              parnode = self.nodeDict[parent]
              #to prevent double inclusion
              if parnode not in sampled and parnode.operation not in SOURCES and parnode.operation != 'ASSERT' and parnode.operation != 'ARR_CREATE':
                sampled.append(parnode)

            #if we've reached the end of parents go to the next node
            if j+1 == len(curNode.parents):
              i += 1
            #this will prevent us from going past 
            if len(sampled) == length:
              break

      if len(sampled) == length:
        success = True
    
    #now that we have sampled our values we have to transform values to sources
    newSourcedGraph = self.printSubGraph(sampled, verbose)

    if verbose:
      for node in newSourcedGraph.nodes:
        print node

    return newSourcedGraph.topSort(verbose=verbose)
  
  def getSRCNamesInOrder(self, curNode,stdName,ctr,debug=False):
    #Normalizes the order of sources based on their appearance on DFS
    #will assume that the graph is top sorted
    #print "ctr,curnode: ",ctr, curNode
    #if(ctr is None):
    #print curNode.operation, ctr, stdName
    if curNode.operation in SOURCES:
      nm = curNode.parents[0]
      if nm not in stdName:
        stdName[nm]='N_' + str(ctr)
        return ctr + 1
      else:
        return ctr
    else:
      ctrnew = ctr
      for parent in curNode.parents:
        ctrtmp = self.getSRCNamesInOrder(self.nodeDict[parent],stdName,ctrnew,debug)
        ctrnew = ctrtmp
      return ctrnew

  def getSignature(self,debug=False):
    #will return a unique signature for this graph based on the operation orderings
    #will assume that the graph is top sorted
    stdName = dict()
    ctr=1
    numSRCs = self.getSRCNamesInOrder(self.nodes[-1],stdName,ctr,debug) - 1
    #print stdName
    #print "Getting Sigs!"
    return self.getSignatureHelper(self.nodes[-1],stdName,debug)

  def getSignatureHelper(self, curNode,stdName,debug=False):
    if curNode.operation in ['S','SRC']:
        nm = curNode.parents[0]
        if(nm in stdName):
            if curNode.outputtype == 'BOOL_ARR':
                return '(S:' + stdName[nm] +':INT_ARR:'+ curNode.anvsig +')'
            else:
                return '(S:' + stdName[nm] +':'+ curNode.outputtype +':'+ curNode.anvsig +')'
        else: 
            return '(S::)'
    elif curNode.operation == 'CONST':
        val = curNode.parents[0]
        return "(CONST" + str(val) +")"
    if debug:
        print "\nWithin our signature printer\n"
        print curNode
    tot = '(' + curNode.operation 
    if(curNode.operation in COMMOPS):
        #Commutative binary op
        if(len(curNode.parents) != 2):
            print "Error: Binary Op " + curNode.operation + "without 2 parents!"
            sys.exit(1)
        p1 = curNode.parents[0]
        s1 = self.getSignatureHelper(self.nodeDict[p1],stdName,debug)
        p2 = curNode.parents[1]
        s2 = self.getSignatureHelper(self.nodeDict[p2],stdName,debug)
        if(s1 < s2):
            tot += '|' + s1 + '|' + s2 + '|'
        else:
            tot += '|' + s2 + '|' + s1 + '|'
    else:
        tot+='|'
        for parent in curNode.parents:
          tot += self.getSignatureHelper(self.nodeDict[parent],stdName,debug)
          tot += '|'

    tot = tot + ')'

    return tot

  ##
  # __str__(self)
  #
  # returns a string of strings of each node in self.nodes
  def __str__(self):  
    return str([str(node) for node in self.nodes])

  ##
  # toString(self)
  #
  # prints off each node in the graph
  def toString(self):
    for node in self.nodes:
        print node



#keep both children and parents
class Node:

  def __init__(self, uid, operation, outputtype, parents, optionalbits=None, children=None, anvsig=""):
    #uid is the number associated with the node
    self.uid = uid
    #Operation is the type of node
    self.operation = operation
    #What the output should be after the operation
    self.outputtype = outputtype
    #The feeder nodes to this graph. With all arrows pointing up then these are parent nodes
    self.parents = parents
    #The optionalbits allow for extra messages to be joined
    self.optionalbits = optionalbits
    
    self.anvsig = anvsig
    
    #We can not immediately add the children so this list is updated at graph creation
    if children:
      self.children = children
    else:
      self.children = []

  def addChild(self, childNode):
    self.children.append(childNode)

  ###
  # clone(self)
  # 
  # returns a new node with the same properties that our current node has without it's children
  ###
  def clone(self):
    x=Node(self.uid, self.operation, self.outputtype, self.parents, self.optionalbits)
    x.anvsig = self.anvsig
    return x

  def __str__(self):
    return "\nNode ID: " + self.uid + \
           "\nOperation Type: " + self.operation + \
           "\nOutput Type: " + self.outputtype + \
           "\nParent: " + str(self.parents) +\
           "\nOptional Bits: " + str(self.optionalbits) + \
           "\nChildren: " + str(self.children)
