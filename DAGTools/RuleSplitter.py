#!/usr/bin/env python
import sys, time
from os import listdir, makedirs, rmdir
from os.path import join, exists

def isInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

def safe_int(s):
    try: 
        int(s)
        return int(s)
    except ValueError:
        return 0

def printUsageAndExit():
    print '''Usage: RuleSplitter.py (-o <foldername>) <rule file to split 1> <2> ...
                  -o  Output Folder (optional)
                  Note that the list of files must go at the end.'''
    sys.exit(0)

def writeToFile(outfolder, rulefiles, ruleFileName =""):
  done = False
  #print "Rsplitter:15 = ",outfolder, rulefiles
  while not done:
    ## Locking scheme
    #   We attempt to make the directory. If the directory already exists 
    #   then an exception occures and  we keep looping until available
    #print "herro"
    try:
      #making this directory is our locking file
      #print "plzzzz"
      makedirs(join(outfolder, "put.lock"))
      #print "lefafa"
      j = 1
      if not exists(outfolder):
        makedirs(outfolder)
      #print "here1.5"
      else:
        #print "flounder"
        contents = listdir(outfolder)
        #print contents
        #print "freddy"
        contents.remove('put.lock') #Remove the lock from contents
        contents.remove('split.lock') #Remove the lock from contents
        #print contents
        #print "fancy"
        if [] != contents:
          j = max([safe_int(x) for x in contents])+1 #this assumes that some rule files are positive integers or else starts with 1
        #print "hero"
      for rulefile in rulefiles:
        for rule in rulefile:
          curfolder = join(outfolder, str(j))
          curd = rule[0]
          curp = rule[1]
          curf = rule[2]
          curs = rule[3]

          if not exists(curfolder):
            makedirs(curfolder)

          curdtitle = "d.aux"
          curptitle = "p.aux"
          curftitle = "f.aux"
          curstitle = "sig.aux"
          curbtitle = "info.aux"

          f = file(join(curfolder, curdtitle), "w")
          f.write(curd)

          f = file(join(curfolder, curptitle), "w")
          f.write(curp)

          f = file(join(curfolder, curftitle), "w")
          f.write(curf)

          f = file(join(curfolder, curstitle), "w")
          f.write(curs)
          
          f = file(join(curfolder, curbtitle), "w")
          f.write(ruleFileName)
          
          j += 1
      #Write Files here

      #print "howdy"
      rmdir(join(outfolder, "put.lock"))
      done = True
    except OSError as e:
      #print e
      print "Waiting on put.lock :" + str(rulefiles)
      time.sleep(2);
      
      continue
  #print "Rules Written"



def rulesToFileWriter(outfolder, filesToSplit,writeFile=True):
  #Usage Error

  RULES = []
  #Load the File into memory
  # we will online continue if we can make it
  for rulefile in filesToSplit:
    toparse = open(rulefile)
    #parse our entire file and keep it in memory
    lines = [line.strip() for line in toparse]
    toparse.close() 

    curRules = []

    i = 0
    #print "here2"
    while i < len(lines):
      #print "here" + str(i)
      curd = ''
      curf = ''
      curp = ''
      curs = ''
      
      if "dag d" in lines[i]:
        #File name will be d.aux 
        i += 1
        curd = ''
        #The rules seem to add the assertion at the end. We continue until that assertion
        while "dag p" not in lines[i]:
          curd += lines[i] + '\n'
          i += 1
        #i += 1

      if "dag p" in lines[i]:
        i += 1
        curp = ''
        while "dag f" not in lines[i]:
          curp += lines[i] + '\n'
          i += 1
        #i += 1


      if "dag f" in lines[i]:
        i += 1
        curf = ''
        while "sig" not in lines[i]:
          curf += lines[i] + '\n'
          i += 1
        #i += 1

      if "sig" in lines[i]:
        i+= 1
        curs = lines[i]
        i += 1
      
      curRules.append((curd, curp, curf, curs))
      i += 1

    RULES.append(curRules)
  if writeFile:
    writeToFile(outfolder, RULES)

  return RULES

def splitArgs(argv):
  if len(argv) < 1:
    printUsageAndExit()

  curtime = str(int(time.time()))
  outfolder = "dagrules_" + curtime
  if '-o' in argv:
    outfolder = argv[argv.index('-o')+1]
    filesToSplit = argv[argv.index('-o')+2:]
  else:
    filesToSplit = argv

  rulesToFileWriter(outfolder, filesToSplit)


if __name__ == "__main__":
  splitArgs(sys.argv[1:])

