#!/usr/bin/python
import sys
from os.path import basename, splitext, isfile
from DAG import Graph, Node


################################ CONSTANTS ################################
TYPE = ["BIT", "BOOL", "INT", "INT_ARR", "BIT_ARR", "BOOL_ARR"]
ASSERT = ["ASSERT"]
UNOP = ["S", "SRC","NOT", "NEG", "CONST"]
BINOP = ["AND", "OR", "XOR", "PLUS", "TIMES", "DIV", "MOD", "LT", "EQ", "LTE", "GTE", "GT", "ARR_R", "ARR_W", "ARR_CREATE", "ITE"]
################################ /CONSTANTS ################################

###
# errorInInput(wrongValue, lineNum)
# 
# wrongValue : the value that caused the error to occur
# lineNum : the number of the line that the error occured
# 
# Prints the error and the line number that it occured and exits the program after printing
###
def errorInInput(wrongValue, lineNum, fileName=None):
  if fileName:
    print 'Error in given DAG file: ' + fileName + '\n Check formatting for ' + wrongValue + ' on line: ' + str(lineNum)
  else:
    print 'Error parsing DAG file. Check formatting for ' + wrongValue + ' on line: ' + str(lineNum)
  import pdb
  pdb.set_trace()
  sys.exit(0)



################################ PARSERS ################################
###
# parseAssert(tokens)
# 
# tokens : the line tokenized by spaces
# 
# Parses the input based on the ASSERT function and returns 
#  a node element to be inserted into the graph. 
#
# id = ASSERT val "msg"
# @return node element related to above
###
def parseAssert(tokens):
  curnodeid = tokens[0]
  operator = tokens[2]
  resulttype = "BOOL"
  parents = [tokens[3]]
  optionalbits = None
  #If optional bits exist we take them
  if len(tokens) > 4:
    optionalbits = tokens[4:]

  return Node(curnodeid, operator, resulttype, parents, optionalbits)

###
# parseUNOP(tokens)
# 
# tokens : the line tokenized by spaces
# 
# Parses the input based on the UNOP set of function and returns 
#  a node element to be inserted into the graph.  It also handles
#  SRC with it's additional optional bits.
#
# id = UNOP TYPE parent // where UNOP can be NOT or NEG
# id = SRC TYPE NAME bits
# id = CONST TYPE val
# @return node element related to above
###
def parseUNOP(tokens, lineNum):
  curnodeid = tokens[0]
  operator = tokens[2]
  resulttype = tokens[3]
  if resulttype not in TYPE:
    errorInInput(' '.join(tokens), lineNum)

  parents = [tokens[4]]
  optionalbits = None

  #if we have optional bits or anything past the fifth value we throw
  # it in as optional
  if len(tokens) > 5:
    optionalbits = tokens[5:]

  return Node(curnodeid, operator, resulttype, parents, optionalbits)
 
###
# parseBINOP(tokens)
# 
# tokens : the line tokenized by spaces
# 
# Parses the input based on the BINOP set of function and returns 
#  a node element to be inserted into the graph. 
#
# id = BINOP TYPE left right
# // where BINOP can be AND, OR, XOR, PLUS, TIMES, DIV, MOD, LT, EQ, "ARR_R", "ARR_W", "ARR_CREATE", ITE
#
# @return node element related to above
###
def parseBINOP(tokens, lineNum):
  curnodeid = tokens[0]
  operator = tokens[2]
  resulttype = tokens[3]

  if resulttype not in TYPE:
    errorInInput(' '.join(tokens), lineNum)

  
  if resulttype == "ARR_CREATE":
      # We do this check here in case of the following errors:
      # 3 = ARR_CREATE INT_ARR  0 1 2
      # instead of
      # 3 = ARR_CREATE INT_ARR  3(<-- length of list) 0 1 2

      if parents[4] == len(tokens[5:]):
          parents = tokens[5:]
      else:
          parents = tokens[4:]
  else:
      parents = tokens[4:]

  return Node(curnodeid, operator, resulttype, parents)



##########################
# maybe more operations if needed, try to parse some benchmarks and see which operations you see.
##########################

################################ /PARSERS ################################

###
# main(argv)
# 
# argv : the arguments passed in through console
#
# Takens in a list of file names that contain DAGs and parses them
# in order to create our DAGs in memory.
#
####
def parse(argv, verbose=False):
  #Usage Error
  if len(argv) < 1:
    print 'Usage: Parser.py <file to parse 1> <2> ...'
    sys.exit(0)

  DAGS = []
  #Load the File into memory
  for DAG in argv:
    curFileName = splitext(basename(DAG))[0]
    toparse = open(DAG)
    lines = [line.strip() for line in toparse]
    toparse.close()
    
    ANVFILE = splitext(DAG)[0] + ".anv"
    map_anv = dict()
    if isfile(ANVFILE):
        toparsea = open(ANVFILE)
        for line in toparsea:
            entry = line.split()
            #print entry
            if len(entry) == 2:
                map_anv[entry[0]] = entry[1]
        toparsea.close()
    
    
    
    #Go through each line and split it up to get different files
    
    
    NodeList = []
    arraccount = 0
    for i in range(len(lines)):
      #to make it easier to work with
      line = lines[i]

      tokens = line.split()
      if tokens[0] == 'dag' or (len(tokens) >=3 and tokens[2] == 'D'):
        continue
      #minimum size input is of len 5 ex: id = ASSERT val
      if len(tokens) > 3:
        curnodeid = tokens[0]
        #tokens[1] is '='
        curnode = None
        operator = tokens[2]

        if operator in ASSERT:
          curnode = parseAssert(tokens)
        elif operator in UNOP:
          curnode = parseUNOP(tokens, i)
        elif operator in BINOP:
          curnode = parseBINOP(tokens, i)
        elif operator == 'CTRL':
          tokens[2] = 'SRC'
          curnode = parseUNOP(tokens, i)
        elif operator == 'ARRACC' or operator == 'ARRASS':
          tokens[2] = 'SRC'
          tokens[4] = 'ARRACCSS_' + str(arraccount)
          arraccount += 1
          curnode = parseUNOP(tokens[:5], i)
        else:
          errorInInput(operator, i, DAG)
        
        if(str(curnode.uid) in map_anv):
            curnode.anvsig = map_anv[str(curnode.uid)]
            #print curnode.anvsig
        
        if verbose:
            print curnode
        NodeList.append(curnode)

      else:
        errorInInput(line, i, DAG)

    ElemGraph = Graph(NodeList)

    DAGS.append((curFileName, ElemGraph))
    if verbose:
        print ElemGraph
    #print "###############################################################"
  return DAGS

if __name__ == "__main__":
  parse(sys.argv[1:])

