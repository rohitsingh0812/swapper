import sys
import re
import math
#Mappings of DAG Operation Name -> (smt operation name, # of elements in that operation)
opToName = {"ARRACC": ("ite", 3), "AND": ("and", 2), "EQ": ("=", 2), "OR": ("or", 2), "XOR": ("xor", 2), "PLUS": ("+", 2), "TIMES": ("*", 2), "DIV": ("div", 2), "MOD":("mod", 2), "LTE": ("<=", 2), "LT":("<", 2), "GT":(">", 2), "GTE": (">=", 2), "NEG":("-", 1), "NOT": ("not", 1), "ITE": ("ite", 3), "ARR_R": ("select", 2), "ARR_W": ("store", 3), "S": ("declare-fun", 2)}
class SMT_Node_Print():
	dictionary_node_smt = {} # stores map id -> node smt encoding
	maxEle = 0

	def print_smt(self,dagnodes):
		for node in dagnodes:
			self.print_node(node)
	def print_s(self, node_arr):
		print "(declare-fun " + node_arr[4] + " () " + self.print_type(node_arr[3]) + ")"
		self.dictionary_node_smt[node_arr[0]] = node_arr[4]
	def print_type(self, node_type):
		if node_type == "INT":
			return "Int"
		if node_type == "BOOL" or node_type == "BIT":
			return "Bool"
		if node_type == "INT_ARR":
			return "(Array Int Int)"
		if node_type == "BOOL_ARR":
			return "(Array Int Bool)"
		else:
			raise Exception("Unsupported Type " + s)
	def print_ary_op(self, elements, topLevel):
		print elements
		if elements[0] in self.dictionary_node_smt:
			return self.dictionary_node_smt[elements[0]]
		self.dictionary_node_smt[elements[0]] = "(" + opToName[elements[2]][0]
		for x in range(opToName[elements[2]][1]):
			self.dictionary_node_smt[elements[0]] += " " + self.print_ary_op([elements[4 + x]], False)
		self.dictionary_node_smt[elements[0]] += ")"
		#if topLevel:
			#print self.dictionary_node_smt[elements[0]]
	def print_node(self, node):
		self.maxEle += 1
		elements = re.split(' |\n|\r',node)
		if elements[2] == "ARR_R" or elements[2] == "ARR_W":
			elements[4], elements[5] = elements[5], elements[4]
		elif elements[2] == "S" or elements[2] == "CTRL":
			self.print_s(elements)
		elif elements[2] == "ARRACC":
			#TODO: Check if elements[5] == 2 and 
			if elements[5] != "2":
				print elements[5]
				print "ARRACC IS POORLY FORMED"
			else:
				del elements[5]
				self.print_ary_op(elements, True)	
		elif elements[2] in opToName:
			self.print_ary_op(elements, True)
		elif elements[2] == "CONST":
			self.dictionary_node_smt[elements[0]] = elements[4]
		elif elements[2] == "ASSERT":
			self.dictionary_node_smt[elements[0]] = "(assert " + self.dictionary_node_smt[elements[3]] + ")"
			print self.dictionary_node_smt[str(self.maxEle-1)]
		elif elements[2] not in opToName:
			print "WTF MAN " + elements[2] + "isn't handled"
	def print_end(self):
		print "(check-sat)"
		print "(exit)"
def main(argv):
	filename = argv[0]
	f = open(filename, 'r')
	dagnodes = f.readlines()
	smt_node_print = SMT_Node_Print()
	smt_node_print.print_smt(dagnodes[1:])
	smt_node_print.print_end()
if (__name__=='__main__'):
	main(sys.argv[1:])	
