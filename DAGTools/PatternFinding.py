#!/usr/bin/env python
import sys,time,operator,signal

from DAG import Graph, Node
from Parser import parse
from os import listdir, makedirs, system
from os.path import isfile, join, exists
from random import randint

sigResults = {}
sigDagResults = {}
filetype = ""   #The filetype of the DAG files we're searching for
folder=""	   #The folder we are finding dag files to test
outfolder = ""  #The folder we are outputting our patterns
outfolderdags = ""  #The folder we are outputting the dags and the Makefile
outname = ""	#The name of the output file

n = 0		   #The number of tests we're running
curtime = ''	#The current timestamp
curL = 0		#The current length we're testing for

usageString = '''Usage: PatternFinding -n <x> -l <y> -f </lol/> -o <outputx>
	-n <number of samples ex:10000> 
	-l <length of sample to get ex:3 or a range 3-6> 
	-f <folder to explore (full path)> 
	-t <the file type ex: dag, 2, dag2. Only put extension (checks the end)>
	-o <output folder for results (relative path, will be written in full path)>
	[-v for verbosity]
	[-s for parallel rule generation]
	[-d for debugging]'''

##
# If there is malformed input or some user-based error then print this
def printUsageAndExit(missingElem):
	print "Error: " + missingElem
	print usageString
	sys.exit(0)

##
# In case of Interrupt being pressed, we save our intermediate responses
def signal_handler(signal,frame):
	print "Ctrl+C Pressed! Saving current work"
	writeResultsToFile()
	sys.exit(0)


##
# Generates the MakeFile for in order to generate the rules for the dags
# Takes the top 20 most common patterns and create the commands
def makeFileGen(resultos, rhs):
	global outfolderdags
	#Maximum amount of values we want to get
	maxVal = 20 
	MakeFile = 'all:'
	if len(resultos) < maxVal:
		maxVal = len(resultos)

	maxVal *= rhs-1
	# Ex Command: cegis  --genrulesbigdag 0_2332.py.dag <num-nodes-rhs> 5 514 519 725 3280 3281 [randomness]
	commandPt1 = "-cegis  --genrulesbigdag "
	counter = 1

	for i in range(maxVal):
		MakeFile += ' t' + str(i+1)

	MakeFile += '\n\techo "done all"'
	#What result looks like:
	## ('LT(S()|PLUS(TIMES(S()|S())|S()))', [5912, [[9,12,28, prime_cone_sat_2.smt2.dag], [16,19,29, prime_cone_sat_2.smt2.dag]]])
	for result in resultos:
		locs = result[1]
		randag = locs[1][randint(0, len(locs[1])-1)] #randomly pick a result for this pattern
		curFilename = join(folder, randag[-1])

		curdagLocations = ' '.join(randag[:-1])
		curdagLocationsUnder = '_'.join(randag[:-1])

		for i in range(1,min(5,rhs)):
			command = commandPt1 + curFilename + " " + str(i) + " " + str(rhs) + " " + curdagLocations + " lol"
			ruleSplitCommand = "-RuleSplitter.py -o " + join(folder, "rules") + " " + curFilename + "_" + curdagLocationsUnder + "_" + str(i) + ".out"

			MakeFile += "\n#" + result[0] + "\nt" + str(counter) + ":\n\t" + command + "\n"
			MakeFile += "\t" + ruleSplitCommand + "\n"
			counter += 1

	try:
		outfile = file(join(outfolderdags, "Makefile"), "w")

		outfile.write(MakeFile)
		print "Write of Makefile success!"
	except:
		print "Error printing Makefile"
		print "Error: \n" + sys.exc_info()[0]
		#printUsageAndExit()
		sys.exit(0)

##
# Get the filetype, debug boolean, and folder and parse the dags
def parseDags(folder=folder,filetype=filetype):
	#try opening the folder and loading up all paths to the files
	tempFileType = '.' + filetype
	try:
		dagfiles = [join(folder,f) for f in listdir(folder) if (isfile(join(folder,f)) and tempFileType == f[len(f)-len(tempFileType):len(f)]) ] 
		#print tempFileType, dagfiles[0]
		if tempFileType == '.dag':
			for f in dagfiles:
				anvfname = f[0:len(f)-len(tempFileType)] + ".anv"
				if not isfile(anvfname):
					#create the .anv file
					#print anvfname + "|" +" calling cegis --dag-anvs \"" + f + "\" lol"
					system("cegis --dag-anvs \"" + f + "\" lol")
					if not isfile(anvfname):
						print "Error! ANV file should have been created by now: " + anvfname
						sys.exit(1)
					else:
						print "Created ANV file: " + anvfname
	except OSError, e:
		print e
		printUsageAndExit("folder")
	
	
	return parse(dagfiles)


def getSamples(thelen, verbose=False, debug=False, aggregate=True,n=n,dags=None):
	global sigResults
	global sigDagResults
	global curtime
	global curL
	
	for l in thelen:
		curL = l #This will be used in case of Ctrl+C exit
		curtime = str(int(time.time())) #timestamp for this operation

		#print "Doing Pattern Finding of length: " + str(l)

		#to prevent having to parse all files n times, if the len(dagfiles) < n then we'll parse them all already
		# can do above later, for now will just parse all dags already
		if dags:
			parsedDags = dags
		else:
			parsedDags = parseDags(folder=folder, filetype=filetype)
		if debug:
			print "Parsed files successfully"
		sigResults = {}
		sigDagResults = {}

		#begin to iterate through all n tests
		for i in range(n):
			#randomly pick a dag in our folder
			randag = randint(0, len(parsedDags)-1)
			curdag = parsedDags[randag][1]

			#store that dag's filename
			curdagFileName = parsedDags[randag][0] + '.' + filetype

			rslt = curdag.randomSample(l, verbose)
			#Want to keep track of the nodes within the file
			curdagIDs = []
			for node in rslt.nodes:
				if node not in rslt.sourceNodes:
					curdagIDs.append(node.uid)

			rsltSig = rslt.getSignature()

			sigDagResults[rsltSig] = rslt.printSketchDAGStyle(verbose=False)

			curdagIDs.append(curdagFileName)
			if rsltSig in sigResults.keys():
				sigResults[rsltSig][0] += 1
				#this ensures we get all files that contain the sig
				if curdagIDs not in sigResults[rsltSig][1]:
					sigResults[rsltSig][1].append(curdagIDs) 
			else:
				sigResults[rsltSig] = [1, [curdagIDs]]


		if debug:
			print "Randomly sampled files successfully"

		if aggregate:
			writeResultsToFile()
		else:
			return sigResults.items()


def writeResultsToFile():
	global outfolderdags
	global outfolder 
	#check to see if the output folder exists or else create it
	outfolder = join(folder, outname)

	if not exists(outfolder):
		makedirs(outfolder)
	
	outfolderdags = join(outfolder, "dagfiles_n_" + str(n) + "_l_" + str(curL) + "_" + curtime)
	if not exists(outfolderdags):
		makedirs(outfolderdags)
	filename = "output_nval_" + str(n) + "_lval_" + str(curL) + "_" + curtime

	#we'll now try to write to the file
	sorted_sigResults = []
	try:
		outfile = file(join(outfolder, filename), "w")
		sorted_sigResults = sorted(sigResults.items(), key=operator.itemgetter(1))

		#generate the makefile for this domain, based on most popular
		makeFileGen(sorted_sigResults[::-1], curL)

		outfile.write(str(sorted_sigResults))
		print "Write to file Success! Check results"
	except Exception as e:
		print "Error printing results to file"
		print "Exception: " + str(e)
		printUsageAndExit("output")

	#try:
	for i in range(len(sorted_sigResults)):
		r = sigDagResults[sorted_sigResults[i][0]]
		f = file(join(outfolderdags, "dag_"+str(i)+".dag"), "w")
		f.write(r)
	print "Writing DAG files Success! Check results"


def splitArgs(args):
	global n
	global outname
	global folder
	global filetype

	verbose = False #printing of the process
	debug = False #debugging of Pattern Finding
	aggregate = True #aggregate results in order to run rulespliter

	#we need to check the proper arguments were passed in
	if '-n' not in args:
		printUsageAndExit("-n")
	if '-l' not in args:
		printUsageAndExit("-l")
	if '-f' not in args:
		printUsageAndExit("-f")
	if '-o' not in args:
		printUsageAndExit("-o")
	if '-v' in args:
		verbose = True
	if '-d' in args:
		debug = True
	if '-t' not in args:
		printUsageAndExit("-t")
	if '-s' in args:
		aggregate = False

	n = int(args[args.index('-n')+1]) #number of patterns to explore
	l = args[args.index('-l')+1] #length of subdag nodes to sample

	if len(l) == 1:
		l = [int(l)]
	else:
		brokenl = l.split('-')
		l = range(int(brokenl[0]),int(brokenl[1])+1)
	filetype = args[args.index('-t')+1] #filetype of dags we want to test
	folder = args[args.index('-f')+1] #Folder we will be exploring
	outname = args[args.index('-o')+1] #Folder we will be outputting our results

	getSamples(l,verbose=verbose,debug=debug,aggregate=aggregate, n=n)


signal.signal(signal.SIGINT, signal_handler)

if __name__ == "__main__":
	splitArgs(sys.argv[1:])
