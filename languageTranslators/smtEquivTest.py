#!/usr/bin/env python
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("f1", help="First SMT file")
parser.add_argument("f2", help="Second SMT File")

args = parser.parse_args()

def Assert(cond,msg):
    if not cond:
        print "ERROR: " + msg
        raise Exception(msg)
        sys.exit(1)

from z3 import *
# Wrapper for allowing Z3 ASTs to be stored into Python Hashtables. 
class AstRefKey:
    def __init__(self, n):
        self.n = n
    def __hash__(self):
        return self.n.hash()
    def __eq__(self, other):
        return self.n.eq(other.n)
    def __repr__(self):
        return str(self.n)

def askey(n):
    assert isinstance(n, AstRef)
    return AstRefKey(n)

def get_vars(f):
    r = set()
    def collect(f):
      if is_const(f): 
          if f.decl().kind() == Z3_OP_UNINTERPRETED and not askey(f) in r:
              r.add(askey(f))
      else:
          for c in f.children():
              collect(c)
    collect(f)
    return r

def getVarExp(salt,v):
    Assert(salt != "","salt hash can't be empty")
    name = str(v)
    nn = salt+name
    sort = str(v.sort())
    if sort == "Int":
        return Int(nn)
    elif sort == "Bool":
        return Bool(nn)
    elif sort == "Array(Int, Int)":
        return Array(nn, IntSort(), IntSort())
    elif sort == "Array(Int, Bool)":
        return Array(nn, IntSort(), BoolSort())
    else:
        Assert(False, "Invalid Sort: " + sort)





def check_diff_c():
    #f = parse_smt2_file('./830162/qlock.induction.6.smt2')
    f1 = parse_smt2_file(args.f1)
    f2 = parse_smt2_file(args.f2)
    sv1 = get_vars(f1)
    sv2 = get_vars(f2)

    Assert(len(sv1) == len(sv2),"Sets of Vars don't match!")

    for i in sv1:
        Assert(i in sv2, "Sets of Vars don't match at- " + str(i))
        
    #change all names in f1 : x -> "RR__"+x
    mapvars = {} #string varname -> newvar ASTREF
    for i in sv1:
        mapvars[str(i)] = askey(getVarExp("RR__",i.n))
        f1 = substitute(f1,(i.n,mapvars[str(i)].n))

    #add inequality constraints to Solver
    s = Solver()
    s.add(f1)
    s.add(Not(f2))
    notsame = False
    for i in sv2:
        notsame = Or(notsame,(i.n != mapvars[str(i)].n))

    #print notsame
    s.add(notsame)

    z3_out = s.check()
    if z3_out == sat:
        print "BLASPAHAMY: THE SMT Files aren't equivalent!"
        M = s.model()
        for i in sv2:
            if str(M[i.n]) != str(M[mapvars[str(i)].n]):
                print str(i),str(M[i.n]),str(mapvars[str(i)]),str(M[mapvars[str(i)].n])
        
    elif z3_out == unsat:
        print "unsat"
    else:
        Assert(False, "Invalid z3_out: " + str(z3_out))


def check_diff_out():
    #f = parse_smt2_file('./830162/qlock.induction.6.smt2')
    ctx = Context()
    s = Solver(ctx=ctx)
    f1 = parse_smt2_file(args.f1,ctx=ctx)
    f2 = parse_smt2_file(args.f2,ctx=ctx)
    
    s.add(f1 != f2)
    #print s.assertions()
    z3_out = s.check()
    if z3_out == sat:
        print "BLASPAHAMY: THE SMT Files aren't equivalent!"
        M = s.model()
        print M        
    elif z3_out == unsat:
        print "unsat"
    else:
        Assert(False, "Invalid z3_out: " + str(z3_out))


if __name__ == "__main__": check_diff_out()






