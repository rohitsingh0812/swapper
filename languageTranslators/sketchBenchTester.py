#!/usr/bin/env python

#ID < 58 used folder 2 for testing, ID >= 58 used 2+3 (half_rest_1)
import MySQLdb
import math
import os
import time
import argparse

def mean(data):
    """Return the sample arithmetic mean of data."""
    n = len(data)
    if n < 1:
        raise ValueError('mean requires at least one data point')
    return sum(data)/n # in Python 2 use sum(data)/float(n)

def _ss(data):
    """Return sum of square deviations of sequence data."""
    c = mean(data)
    ss = sum((x-c)**2 for x in data)
    return ss

def pstdev(data):
    """Calculates the population standard deviation."""
    n = len(data)
    if n < 2:
        return 0.0
		#raise ValueError('variance requires at least two data points')
    ss = _ss(data)
    pvar = ss/n # the population variance
    return pvar**0.5
#Takes a fileName, numRuns, solver and rewriter 
#Finds rewritten size and times for 5 runs (median of 5 of em)
#python sketchBenchTester.py --numRuns 1 --fileName /home/ubuntu/sos/sketch-backend/synofsyn/ris_auto/temp_py_sk/dags/0_2066.py.dag --solver /home/ubuntu/smt-optim-sos/languageTranslators/cegisSolver.py --solverOptions "--bndwrand 125" --benchmark AG_TEST --optimSource CP

#python sketchBenchTester.py --numRuns 5 --fileName /home/ubuntu/sos/sketch-backend/synofsyn/ris_auto/temp_py_sk/dags/0_2066.py.dag --rewriter "/home/ubuntu/sos/sketch-backend/src/SketchSolver/cegis -optimtype DO " --solver /home/ubuntu/smt-optim-sos/languageTranslators/cegisSolver.py --solverOptions "--bndwrand 125" --benchmark AG_TEST --optimSource DO-L1

parser = argparse.ArgumentParser()
parser.add_argument("--numRuns", type=int, help="number of Runs to be used for finding median Time",required=True)
parser.add_argument("--fileName", help="path/name of .dag file",required=True)
parser.add_argument("--rewriter", help="path/command for cegis rewriting, optional - omitting this means no rewriting")
parser.add_argument("--solver", help="path/command for py file needed for dag solving",required=True)
parser.add_argument("--solverOptions", help="path/command for dag solving like --bndwrand 125 or --optimType DO", default="")
parser.add_argument("--benchmark", help="Benchmark ID for DB for dag/smt solving",required=True)
parser.add_argument("--optimSource", help="OptimSource entry for DB for dag/smt solving",required=True)
parser.add_argument("--seeds",nargs="+",required=True)

db = MySQLdb.connect(host="localhost", user="root", passwd="", db="results")
cur = db.cursor()
table = "run_stats_cav16"

def doRewrite(filePre,rewriter):
	print "Rewriting the DAG: " + filePre + " || Using: " + rewriter
	os.system(rewriter + " --rewrite " + filePre + ".dag " + filePre + ".rewritten.dag lol")

def main(args):
	solverName = os.path.splitext(args.solver)[0].split('/')[-1]
	solver = __import__(solverName)
	if(args.fileName.endswith(".dag")):
		dagname = args.fileName
		if (not (args.rewriter is None)): #doing rewrites 
			doRewrite(args.fileName[:-4],args.rewriter)
			dagname = args.fileName[:-4] + ".rewritten_sk.dag"
		
		print "Starting time check of dag: " + dagname
		(timeList, memList, soln) = timeCheck(dagname, args.numRuns, solver, args.solverOptions.split(),args.seeds)
		#print timeList, memList,soln
		addToDB(dagname, soln, timeList, memList, args, solverName)
	else:
		print "Need to be a .dag file. Exiting..."
		return 0
		#TODO: Rewrite code for SMT cases
	

def calcStats(outString):
	outList = outString.split('\n')
	for line in outList:
		if(line.startswith("SOLVER RAND SEED =")):
			seedEntry = int(line[18:]) #Seconds
		if(line.startswith("Solution time (ms):")):
			timeEntry = float(line[19:])/1000.0 #Seconds
		if(line.startswith("Max private mem (bytes):")):
			memEntry = float(line[24:])/(1024.0*1024.0) #MBs
	return (timeEntry,memEntry,seedEntry)
		
def timeCheck(fileName, numRuns, solver, options, seeds):
	times = []
	mems = []
	timeOut=600
	checker = solver.Solver(fileName, options, timeOut)
	ansCommon =""
	for x in range(numRuns):
		start = time.time()
		ans = checker.check(seeds[x])
		if(ansCommon == ""):
			ansCommon = ans
		elif (ansCommon != ans):
			print "Inconsistent outputs across multiple runs!"
			return ([0 for i in range(numRuns)],[0 for i in range(numRuns)], "INCONSISTENT:" + ansCommon + "|" + ans)
		end = time.time()
		#get time from checker.outputString
		#print "OUTPUT: \n" + checker.outputString
		(timeEntry,memEntry,seedEntry) = calcStats(checker.outputString)
		oldTimeEntry = end-start
		#if(seedEntry != int(seeds[x])):
		#	raise IOError("Seed was not provided to cegis correctly")
		times.append(timeEntry)
		mems.append(memEntry)
		#if end-start > timeOut/2:
		#	print "Checking the time took longer than 5 minutes, terminating program"
		#	return (-1, "TIMEOUT")
		print "Finishing run " + str(x+1) + "/" + str(numRuns)
		print "TIMES COMPARE: ",timeEntry, oldTimeEntry
		#print "Time: " + str(end-start) + "s"
	return (times, mems, ans)#(sorted(times)[int(math.floor(numRuns/2))], ans)

		
		
def aq(x):
	return "'"+x+"'"
#mysql>  create table run_stats_cav16(BenchmarkID varchar(100), OptimSource varchar(100), DagFile varchar(200), DagSize int, SolverName varchar(500), SolverOptions varchar(1000), SolutionValue varchar(500), SolutionMedianTime float, numRuns int, timeString varchar(1000), SolutionAvgTime float, SolutionStDevTime float, SolutionAvgMaxMem float, MemString varchar(1000), PRIMARY KEY (BenchmarkID, OptimSource, DagFile));
def addToDB(dagFileName, solutionValue, timeList, memList, args, solverName):
	#Need to be in DB
	#BenchmarkID, OptimSource, DagFile, DagSize, SolverName, SolverOptions, SolutionValue, SolutionMedianTime, numRuns
	solutionMedianTime=sorted(timeList)[int(math.floor(len(timeList)/2))];
	solutionAvgTime = mean(timeList)
	solutionStDevTime = pstdev(timeList)
	solutionAvgMaxMem = mean(memList)
	timeString = '/' + (''.join(str(e)+'/' for e in timeList))
	memString = '/' + (''.join(str(e)+'/' for e in memList))
	dagFile = args.fileName.split('/')[-1][:-4]
	dagSize = sum(1 for line in open(dagFileName))
	statement = "INSERT into " + table + " (BenchmarkID, OptimSource, DagFile, DagSize, SolverName, SolverOptions, SolutionValue, SolutionMedianTime, numRuns, timeString, SolutionAvgTime, SolutionStDevTime, SolutionAvgMaxMem,MemString) VALUES (%s, %s, %s, %d, %s, %s, %s, %f, %d, %s, %f, %f, %f, %s) ON DUPLICATE KEY UPDATE DagSize=VALUES(DagSize), SolverOptions=VALUES(SolverOptions), SolutionMedianTime=VALUES(SolutionMedianTime), numRuns=VALUES(numRuns), timeString=VALUES(timeString), SolutionValue=VALUES(SolutionValue), SolutionAvgTime=VALUES(SolutionAvgTime), SolutionStDevTime=VALUES(SolutionStDevTime), SolutionAvgMaxMem=VALUES(SolutionAvgMaxMem), MemString=VALUES(MemString);" % (aq(args.benchmark), aq(args.optimSource), aq(dagFile), dagSize, aq(solverName), aq(args.solverOptions), aq(solutionValue), solutionMedianTime, args.numRuns, aq(timeString),solutionAvgTime,solutionStDevTime,solutionAvgMaxMem,aq(memString))
	print statement
	cur.execute(statement)
	cur.close()
	db.commit()
	db.close()
	#deleteFiles(fileName)

	


if (__name__=='__main__'):
	args = parser.parse_args()
	if(len(args.seeds) != args.numRuns):
		raise IOError("We need as many seeds as numRuns")
	main(args)
