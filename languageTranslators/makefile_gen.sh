#!/bin/bash -e
#Usage: to generate Makefile for generating DAGs for all sketch files in a folder
#$2 = extra parameters for sketch run
#example: bash cross_gen_makefile.sh autograder " --be:-nodagoptim -V 10 "  > ./autograder/Makefile
echo -n "all: "

for i in $(seq 1 `ls allSMTs/*.smt2 | wc -l`); do
    echo -n "t$i "
done

echo "";
echo "	echo \"done all\""
ctr=0
for i in `ls allSMTs/*.smt2`; do
    ctr=$((ctr+1))
    echo "t$ctr:
	-python tester.py $i 5
"
done
