#!/usr/bin/env python
import sys
from pyparsing import OneOrMore, nestedExpr
from sets import Set
sys.setrecursionlimit(10000000)
globalvalue = 0
variableToID = {}
idToType = {}

opToName = {"and": ("AND", 0, "BIT"), "=": ("EQ", 2, "BIT"), "or": ("OR", 0, "BIT"), "xor": ("XOR", 0, "BIT"), "+": ("PLUS", 0, "INT"), "*": ("TIMES", 0, "INT"), "div": ("DIV", 2, "INT"), "mod":("MOD", 2, "INT"), "<=": ("LTE", 2, "BIT"), "<":("LT", 2, "BIT"), ">":("GT", 2, "BIT"), ">=": ("GTE", 2, "BIT"), "-":("NEG", 1, "INT"), "not": ("NOT", 1, "BIT"), "ite": ("ARRACC", 3, "?"), "select": ("ARR_R", 2, "?"), "store": ("ARR_W", 3, "?")}

def Assert(cond,msg):
    if not cond:
        print "ERROR: " + msg
        raise Exception(msg)
        sys.exit(1)

def removeNonFormula(data):
    legalOps = Set(["declare-fun", "assert", "Array", "par", "select", "store", "forall", "true", "false", "not", "=>", "and", "or", "xor", "=", "distinct", "ite", "-", "+", "*", "div", "mod", "abs", "<=", "<", ">=", ">"])
    eleToRemove = []
    seenEnd = False
    for x in range(len(data)):
        if data[x][0] not in legalOps:
            Assert(data[x][0] in ['set-logic','set-info','check-sat','exit','get-model'],"Not a legal Op: " + data[x][0])
            if data[x][0] == 'check-sat':
                seenEnd = True
            eleToRemove.append(x)
        elif seenEnd:
            Assert(False,"A legal Op found after check-sat: " + data[x][0])
    eleToRemove.sort()
    eleToRemove.reverse()
    for y in eleToRemove:
        data.pop(y)

def getParseData(filename):
    f = open(filename, 'r')
    string = f.read()
    data = OneOrMore(nestedExpr()).parseString(string)
    data = data.asList()
    return data


def makeBinary(arr): #possible multiple operands
    Assert(len(arr) >= 3,"Invalid length of arr for makeBinary: " + str(arr))
    if len(arr) == 3:
        return arr
    else:
        maxind = len(arr)-1
        op = arr[0]
        operand1 = arr[maxind]
        operand2 = makeBinary(arr[:-1])
        return [op,operand1,operand2]
def modifyArrForDAG(arr):
    #print "|" + arr[0] + "|"
    if arr[0] == "select" or arr[0] == "store": #index, array [,new-value]
        arr[1], arr[2] = arr[2], arr[1]
    elif arr[0] == "ite": # cond, 0-val, 1-val 
        arr[2], arr[3] = arr[3], arr[2]
    elif arr[0] == ">": # GT x y as LT y x
        arr[0] = "<"
        arr[1],arr[2] = arr[2],arr[1]
    elif arr[0] == ">=": # GTE x y as NOT (LT x y)
        arr[0] = "<"
        arr = ["not",arr]
    elif arr[0] == "<=": #LTE x y as NOT (LT y x) 
        arr[0] = "<"
        arr[1],arr[2] = arr[2],arr[1]
        arr = ["not",arr]
    elif arr[0] in ["and", "+", "*", "or", "xor"]:     #possible multiple children 
        #print str(arr)
        arr=makeBinary(arr)
        #print str(arr)
        Assert(len(arr) == 3,"Length of arr is not 3: " + str(arr))
    elif arr[0] == "-" and len(arr) == 3:
        #binary minus!
        arr = ["+",arr[1],["-",arr[2]]]
    return arr

def getTypeStr(arr):
    if arr[3] == "Int":
        typeStr = "INT"
    elif arr[3] == "Bool":
        typeStr = "BIT"
    elif type(arr[3]) is list:
        if arr[3][0] == "Array" and arr[3][1] == "Int" and arr[3][2] == "Int":
            typeStr = "INT_ARR"
        elif arr[3][0] == "Array" and arr[3][1] == "Int" and arr[3][2] == "Bool":
            typeStr = "BIT_ARR"
        else:
            Assert(False,"Unknown SRC variable type: " + str(arr[3]))
    else:
        Assert(False,"Unknown SRC variable type: " + str(arr[3]))
    return typeStr
    
def prettyPrinter(arr):
    global globalvalue
    global opToName
    if arr[0] in opToName:
        arr = modifyArrForDAG(arr)
        opArr = opToName[arr[0]]
        opHelper(arr, opArr[1], opArr[2], opArr[0])
    elif arr[0] == "declare-fun":
        string = str(globalvalue) + " = CTRL "
        typeStr = getTypeStr(arr)
        string +=typeStr + " "
        string += arr[1]
        print string
        variableToID[arr[1]] = globalvalue
        idToType[globalvalue] = typeStr
    elif arr[0] == "assert":
        if type(arr[1]) is str and str(arr[1]) in variableToID:
            variable1 = variableToID[arr[1]]
        else:
            prettyPrinter(arr[1])
            variable1 = globalvalue - 1
        print str(globalvalue) + " = ASSERT " + str(variable1)
    elif arr[0] == "let":
        for val in arr[1]:            
            if type(val[1]) is str:
                if val[1].isdigit() or (val[1] == "true") or (val[1] == "false"):
                    numberHelper(val[1])
                    variableToID[val[0]] = globalvalue - 1
                else:
                    variableToID[val[0]] = variableToID[val[1]]
            else:
                prettyPrinter(val[1])
                variableToID[val[0]] = globalvalue - 1
        prettyPrinter(arr[2])
        globalvalue -= 1
    else:
        Assert(False,str(globalvalue) + " SOMETHING HAPPENED, NOT IMPLEMENTED " + ', '+str(arr))
    globalvalue += 1

def numberHelper(num): #Assuming SMT is strongly typed!
    global globalvalue
    if num not in variableToID.keys():
        variableToID[num] = globalvalue
        if num.isdigit():
            print str(globalvalue) + " = CONST INT " + num
            idToType[globalvalue] = "INT"
        elif num == "true":
            print str(globalvalue) + " = CONST BIT " + "1"
            idToType[globalvalue] = "BIT"
        elif num == "false":
            print str(globalvalue) + " = CONST BIT " + "0"
            idToType[globalvalue] = "BIT"
        globalvalue += 1
    return variableToID[num]

def joinTypes(a,b):
    if(a==b and a != "?"):
        return a
    validJoins = {}
    validJoins[("BIT","INT")] = "INT"
    validJoins[("BOOL","INT")] = "INT"
    validJoins[("BIT_ARR","INT_ARR")] = "INT_ARR"
    validJoins[("BOOL_ARR","INT_ARR")] = "INT_ARR"
    if (a,b) in validJoins:
        return validJoins[(a,b)]
    elif (b,a) in validJoins:
        return validJoins[(b,a)]
    else:
        Assert(False,"Couldn't find a join of Types: " + a + " & " +b)
    
    
def opHelper(arr, num, retType, op): #num == 0 are the cases with possible multiple associative vals
    global globalvalue
    global opToName
    printStr = " = " + op
    unkType = (retType == "?")
    ids = {}
    operands = ""
    for i in range(len(arr)-1):
        i += 1
        if(arr[0] == "ite" and i==2):
            operands += " 2 "
        if type(arr[i]) is str:
            if arr[i].isdigit() or (arr[i] == "true") or (arr[i] == "false"):
                variable1 = numberHelper(arr[i])
            else:
                variable1 = variableToID[arr[i]]
        else:
            prettyPrinter(arr[i])
            variable1 = globalvalue - 1
        ids[i] = variable1
        operands += " " + str(variable1)
    Assert(len(ids.keys()) == len(arr) -1, "Invalid computation of ids: " + str(ids) + " | " +str(arr))
    #print globalvalue,arr[0], ids
    if unkType and arr[0] == "ite": # cond, 0-val, 1-val
        retType = joinTypes(idToType[ids[3]],idToType[ids[2]])
    elif unkType and (arr[0] == "select"): #index, array
        if idToType[ids[2]] == "INT_ARR":
            retType = "INT"
        elif idToType[ids[2]] == "BIT_ARR":
            retType = "BIT"
        else:
            Assert(False,"ARR_R unknown type not resolved")
    elif unkType and (arr[0] == "store"): #index, array ,new-value
        retType = idToType[ids[2]]
    elif unkType:
        Assert(False, "Invalid unknown Type: " + op)
    Assert(retType != "?", "retType was never updated! " + str(globalvalue)) 
    idToType[globalvalue] = retType
    printStr = printStr + " " + retType + " " + operands
    Assert(printStr.find("?") == -1, "Somehow ? type sent to DAG!")
    print str(globalvalue) + printStr        

#Issues with temporary output:
#    [DONE]1. Possible multiple children for associative nodes: AND, OR, XOR, PLUS, TIMES
#     [DONE]2. Dealing with GTE and LTE as NOT LT, GT x y  as LT y x  
#    [DONE]3. ITE vs ARRACC order 
#   [DONE]4. Add "2" in ARRACCs!

def main():
    
    if len(sys.argv) != 2:
        print "Exactly one argument needed and allowed: SMT filename" 
    data = getParseData(sys.argv[1])
    
    removeNonFormula(data)
    
    for z in data:
        prettyPrinter(z) #recursive DAG printer
        
if __name__ == "__main__": main()
