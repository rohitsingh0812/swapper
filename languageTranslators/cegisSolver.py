import subprocess
class Solver:
	progName = "cegis"
	fileName = ""
	options = ["--bounded-solver-sketch"]
	timeOutDuration = ""
	outputString =""
	def __init__(self, fileName, options, timeOut):
		self.fileName = fileName
		self.options = options + self.options
		self.timeOutDuration = str(timeOut)
	def check(self, seed):
		customOption =[]
		#Ignore random  seed!
        #if(seed is not None):
		#	customOption = ['--seed', str(seed)]
		args = ["timeout", self.timeOutDuration, self.progName] + customOption + self.options + [self.fileName, "lol"]
		proc = subprocess.Popen(args, stdout=subprocess.PIPE)
		(out, err) = proc.communicate()
		self.outputString = out
		if "FAILED" in out:
			return "unsat"
		elif "CORRECT" in out:
			return "sat"
		else:
			return "error"		
