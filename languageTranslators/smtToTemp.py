#!/usr/bin/env python
import sys
from pyparsing import OneOrMore, nestedExpr
from sets import Set
sys.setrecursionlimit(10000000)
globalvalue = 0
variableToID = {}
opToName = {"and": ("AND", 0, "BIT"), "=": ("EQ", 2, "BIT"), "or": ("OR", 0, "BIT"), "xor": ("XOR", 0, "BIT"), "+": ("PLUS", 0, "INT"), "*": ("TIMES", 0, "INT"), "div": ("DIV", 2, "INT"), "mod":("MOD", 2, "INT"), "<=": ("LTE", 2, "BIT"), "<":("LT", 2, "BIT"), ">":("GT", 2, "BIT"), ">=": ("GTE", 2, "BIT"), "-":("NEG", 1, "INT"), "not": ("NOT", 1, "BIT"), "ite": ("ITE", 3, "INT"), "select": ("ARR_R", 2, "INT"), "store": ("ARR_W", 3, "INT_ARR")}
def main():
	legalOps = Set(["declare-fun", "assert", "Array", "par", "select", "store", "forall", "true", "false", "not", "=>", "and", "or", "xor", "=", "distinct", "ite", "-", "+", "*", "div", "mod", "abs", "<=", "<", ">=", ">"])
	filename = sys.argv[1]
	f = open(filename, 'r')
	string = f.read()
	data = OneOrMore(nestedExpr()).parseString(string)
	data = data.asList()
	eleToRemove = []
	for x in range(len(data)):
		if data[x][0] not in legalOps:
			eleToRemove.append(x)
	eleToRemove.sort()
	eleToRemove.reverse()
	for y in eleToRemove:
		data.pop(y)
	for z in data:
		prettyPrinter(z)
def prettyPrinter(arr):
	global globalvalue
	global opToName
	if arr[0] in opToName:
		if arr[0] == "select" or arr[0] == "store":
			arr[1], arr[2] = arr[2], arr[1]
		opArr = opToName[arr[0]]
		opHelper(arr, opArr[1], opArr[2], opArr[0])
	elif arr[0] == "declare-fun":
		string = str(globalvalue) + " = S "
		if type(arr[3]) is str:
			string += arr[3].upper()
			string += " "
		else:
			if arr[3][0] == "Array":
				string += arr[3][1].upper()
				string += "_ARR"
				string += " "
		string += arr[1]
		print string
		variableToID[arr[1]] = globalvalue
	elif arr[0] == "assert":
		prettyPrinter(arr[1])
		variable1 = globalvalue - 1
		print str(globalvalue) + " = ASSERT " + str(variable1)
	elif arr[0] == "let":
		for val in arr[1]:			
			if type(val[1]) is str:
				if val[1].isdigit() or (val[1] == "true") or (val[1] == "false"):
					numberHelper(val[1])
					variableToID[val[0]] = globalvalue - 1
				else:
					variableToID[val[0]] = variableToID[val[1]]
			else:
				prettyPrinter(val[1])
				variableToID[val[0]] = globalvalue - 1
		prettyPrinter(arr[2])
		globalvalue -= 1
	else:
		print str(globalvalue) + " SOMETHING HAPPENED, NOT IMPLEMENTED " + ', '.join(arr)
	globalvalue += 1
def numberHelper(num):
	global globalvalue
	if num not in variableToID.keys():
		variableToID[num] = globalvalue
		if num.isdigit():
			print str(globalvalue) + " = CONST INT " + num
		elif num == "true":
			print str(globalvalue) + " = CONST BIT " + "1"
		elif num == "false":
			print str(globalvalue) + " = CONST BIT " + "0"
		globalvalue += 1
	return variableToID[num]
def opHelper(arr, num, retType, op):
	global globalvalue
	global opToName
	printStr = " = " + op + " " + retType
	for i in range(len(arr)-1):
		i += 1
		if type(arr[i]) is str:
			if arr[i].isdigit() or (arr[i] == "true") or (arr[i] == "false"):
				variable1 = numberHelper(arr[i])
			else:
				variable1 = variableToID[arr[i]]
		else:
			prettyPrinter(arr[i])
			variable1 = globalvalue - 1
		printStr += " " + str(variable1)
	print str(globalvalue) + printStr		
if __name__ == "__main__": main()
