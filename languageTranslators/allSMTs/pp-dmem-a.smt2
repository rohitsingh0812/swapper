(set-logic QF_ALIA)
(set-info :source |
Translated from old SVC processor verification benchmarks.  Contact Clark
Barrett at barrett@cs.nyu.edu for more information.

This benchmark was automatically translated into SMT-LIB format from
CVC format using CVC Lite
|)
(set-info :smt-lib-version 2.0)
(set-info :category "industrial")
(set-info :status unsat)
(declare-fun CONFSTATE_S2_INIT () Int)
(declare-fun CONFSTALL_S2M_INIT () Bool)
(declare-fun VALID1_SEL1_S2_INIT () Bool)
(declare-fun VALID1_RW1_S2_INIT () Bool)
(declare-fun VALID1_CACHEDOUT_S2_INIT () Bool)
(declare-fun DTAG1_ADDR_S2_INIT () Int)
(declare-fun DTAG1_CACHEDOUT_S2_INIT () Int)
(declare-fun DTAGREAD_S2M_INIT () Bool)
(declare-fun INSTRISLOAD_S2M_INIT () Bool)
(declare-fun EXTSTATE_S2_INIT () Int)
(declare-fun INSTRISSTORE_S2W_INIT () Bool)
(declare-fun FLUSHSTATE_S2_INIT () Int)
(declare-fun MCBUSY_S2_INIT () Bool)
(declare-fun DEXTREQ_S2_INIT () Bool)
(declare-fun XFERCNT_S2_INIT () Int)
(declare-fun DCACHEDATAVALID_S2_INIT () Bool)
(declare-fun RESTARTED_S2_INIT () Bool)
(declare-fun DOLOAD_INIT () Bool)
(declare-fun DOSOMETHING_INIT () Bool)
(declare-fun INSTRISSTORE_S2M_INIT () Bool)
(declare-fun DCACHEHIT1_S1W_INIT () Bool)
(declare-fun INSTRISSTORE_S1W_INIT () Bool)
(declare-fun DIRTY1_SEL1_S2_INIT () Bool)
(declare-fun DIRTY1_RW1_S2_INIT () Bool)
(declare-fun DIRTY1_CACHEDOUT_S2_INIT () Bool)
(declare-fun SBWRITE_S2_INIT () Bool)
(declare-fun MCREQ_S2_INIT () Bool)
(declare-fun MCREAD_S2_INIT () Bool)
(declare-fun DELAYEDMCWRITE_S2_INIT () Bool)
(declare-fun DMEM_INIT () (Array Int Int))
(declare-fun DELAYEDMCADDR_S2_INIT () Int)
(declare-fun DRVMCDBDATA_S2_INIT () Bool)
(declare-fun SBOUT_S2_INIT () Int)
(declare-fun NO_VALUE () Int)
(declare-fun EXTSTATE_S1_INIT () Int)
(declare-fun FLUSHSTATE_S1_INIT () Int)
(declare-fun DTAGDATA1_S1_INIT () Int)
(declare-fun MISSADDR_S1W_INIT () Int)
(declare-fun SB_SEL_S2_INIT () Bool)
(declare-fun SB_RW_S2_INIT () Bool)
(declare-fun SB_OUT_S2_INIT () Int)
(declare-fun DTAG1_RW1_S2_INIT () Bool)
(declare-fun DTAG1_DATA_INIT () Int)
(declare-fun DCACHESEL_S2M_INIT () Bool)
(declare-fun DCACHERW_S2M_INIT () Bool)
(declare-fun DCACHE_OUT_S2_INIT () Int)
(declare-fun DTAG1_SEL1_S2_INIT () Bool)
(declare-fun SB_IN_S2_INIT () Int)
(declare-fun SB_DATA_INIT () Int)
(declare-fun STOREADDR_S2M_INIT () Int)
(declare-fun STOREADDR_S1W_INIT () Int)
(declare-fun STOREDATA_S2M_INIT () Int)
(declare-fun STOREDATA_S1W_INIT () Int)
(declare-fun MCDBDATA_OUT_S2_INIT () Int)
(declare-fun MCDATA_S1_INIT () Int)
(declare-fun DIRTY1_IN_S2_INIT () Bool)
(declare-fun DIRTY1_DATA_INIT () Bool)
(declare-fun VALID1_IN_S2_INIT () Bool)
(declare-fun VALID1_DATA_INIT () Bool)
(declare-fun DCACHE_IN_S2_INIT () Int)
(declare-fun DCACHE_DATA_INIT () Int)
(declare-fun MEMADDR_S1M_INIT () Int)
(declare-fun STOREDATA_S1M_INIT () Int)
(declare-fun ARB_ADDR () Int)
(declare-fun DEXTREAD_S2_INIT () Bool)
(declare-fun DCACHEDATAVALID_S1_INIT () Bool)
(declare-fun DCACHEMISS_S1W_INIT () Bool)
(declare-fun REQWAITING_S2_INIT () Bool)
(declare-fun CCMEMREQ_S1_INIT () Bool)
(declare-fun MCREAD_S1_INIT () Bool)
(declare-fun DELAYEDMCWRITE_S1_INIT () Bool)
(declare-fun XFERCNT_S1_INIT () Int)
(declare-fun DRVMCDBDATA_S1_INIT () Bool)
(declare-fun CONFSTATE_S1_INIT () Int)
(declare-fun MCDBDATAVALID_S1_INIT () Bool)
(declare-fun MCDBDATAVALID_S2_INIT () Bool)
(declare-fun RESTARTED_S1_INIT () Bool)
(declare-fun SBWRITE_S1_INIT () Bool)
(declare-fun BWASLOAD_S1W_INIT () Bool)
(declare-fun VALID1_ADDR_S2_INIT () Int)
(declare-fun DCACHE_ADDR_S2_INIT () Int)
(declare-fun DIRTY1_ADDR_S2_INIT () Int)
(declare-fun DELAYEDMCADDR_S1_INIT () Int)
(declare-fun SB_IN_S1_INIT () Int)
(declare-fun SBOUT_S1_INIT () Int)
(declare-fun SBDELAYADDR_S2_INIT () Int)
(declare-fun REQWAITING_S1B_INIT () Bool)
(declare-fun SBDELAYADDR_S1_INIT () Int)
(declare-fun SB_ADDR_S2_INIT () Int)
(declare-fun MCADDR_S2_INIT () Int)
(assert (let ((?v_50 (ite DOLOAD_INIT false true)) (?v_66 (= CONFSTATE_S2_INIT 10)) (?v_7 (ite (ite VALID1_SEL1_S2_INIT VALID1_RW1_S2_INIT false) VALID1_CACHEDOUT_S2_INIT false)) (?v_8 (= DTAG1_ADDR_S2_INIT DTAG1_CACHEDOUT_S2_INIT))) (let ((?v_1 (ite (ite (ite ?v_7 false true) true (ite ?v_8 false true)) DTAGREAD_S2M_INIT false)) (?v_0 (= EXTSTATE_S2_INIT 11)) (?v_9 (= CONFSTATE_S2_INIT 12)) (?v_2 (= EXTSTATE_S2_INIT 13))) (let ((?v_13 (ite ?v_66 (ite (ite CONFSTALL_S2M_INIT (ite (ite ?v_1 false true) (ite INSTRISLOAD_S2M_INIT ?v_0 false) false) false) 12 10) (ite ?v_9 (ite ?v_2 10 14) 10)))) (let ((?v_12 (= ?v_13 10)) (?v_68 (ite INSTRISSTORE_S2W_INIT false true)) (?v_20 (= EXTSTATE_S2_INIT 15)) (?v_19 (= FLUSHSTATE_S2_INIT 16)) (?v_33 (= EXTSTATE_S2_INIT 17)) (?v_70 (ite MCBUSY_S2_INIT false true))) (let ((?v_24 (ite ?v_70 DEXTREQ_S2_INIT false)) (?v_72 (= EXTSTATE_S2_INIT 18)) (?v_3 (= EXTSTATE_S2_INIT 19)) (?v_22 (= XFERCNT_S2_INIT 20)) (?v_21 (= FLUSHSTATE_S2_INIT 21))) (let ((?v_78 (ite ?v_21 false true)) (?v_73 (= EXTSTATE_S2_INIT 22))) (let ((?v_6 (ite ?v_0 (ite (ite ?v_1 INSTRISSTORE_S2W_INIT false) 13 (ite (ite ?v_1 ?v_68 false) 15 11)) (ite ?v_2 15 (ite ?v_20 (ite (ite ?v_19 false true) 15 17) (ite ?v_33 (ite ?v_24 18 17) (ite ?v_72 19 (ite ?v_3 (ite (ite ?v_22 ?v_78 false) 22 19) (ite ?v_73 23 11))))))))) (let ((?v_11 (= ?v_6 11))) (let ((?v_4 (ite ?v_11 false true))) (let ((?v_51 (ite (ite ?v_12 false true) true (ite ?v_4 true ?v_1))) (?v_52 (ite (ite (ite INSTRISLOAD_S2M_INIT (ite ?v_3 DCACHEDATAVALID_S2_INIT false) false) true (ite RESTARTED_S2_INIT ?v_4 false)) false true)) (?v_10 (ite DOLOAD_INIT DOSOMETHING_INIT false)) (?v_5 (ite ?v_50 DOSOMETHING_INIT false))) (let ((?v_48 (ite (ite ?v_51 (ite ?v_52 true (ite ?v_10 true ?v_5)) false) false true)) (?v_62 (ite (ite ?v_51 ?v_52 false) false true)) (?v_14 (= ?v_6 19))) (let ((?v_17 (ite ?v_14 false true)) (?v_15 (= ?v_6 22))) (let ((?v_53 (ite ?v_17 (ite ?v_15 false true) false)) (?v_54 (ite (ite INSTRISSTORE_S2M_INIT DTAGREAD_S2M_INIT false) (ite DTAGREAD_S2M_INIT (ite ?v_7 ?v_8 false) false) DCACHEHIT1_S1W_INIT)) (?v_65 (ite INSTRISLOAD_S2M_INIT false true)) (?v_115 (ite ?v_9 true ?v_2))) (let ((?v_39 (ite ?v_65 true ?v_115)) (?v_23 (= EXTSTATE_S2_INIT 23))) (let ((?v_16 (ite ?v_39 (ite INSTRISSTORE_S2M_INIT (ite ?v_23 false true) false) INSTRISSTORE_S1W_INIT)) (?v_55 (ite ?v_11 ?v_12 false)) (?v_42 (= ?v_13 12)) (?v_43 (= ?v_6 13))) (let ((?v_56 (ite ?v_42 true ?v_43)) (?v_18 (ite ?v_14 true ?v_15)) (?v_44 (ite (= ?v_6 23) ?v_16 false))) (let ((?v_57 (ite (ite ?v_18 DCACHEDATAVALID_S2_INIT false) true ?v_44)) (?v_74 (= FLUSHSTATE_S2_INIT 24)) (?v_75 (= FLUSHSTATE_S2_INIT 25)) (?v_76 (= FLUSHSTATE_S2_INIT 26)) (?v_25 (= FLUSHSTATE_S2_INIT 27))) (let ((?v_26 (ite ?v_19 (ite (ite ?v_20 (ite (ite (ite DIRTY1_SEL1_S2_INIT DIRTY1_RW1_S2_INIT false) DIRTY1_CACHEDOUT_S2_INIT false) ?v_7 false) false) 21 16) (ite ?v_21 (ite ?v_22 24 21) (ite ?v_74 (ite ?v_23 25 24) (ite ?v_75 (ite ?v_24 26 25) (ite ?v_76 (ite DCACHEDATAVALID_S2_INIT 27 26) (ite ?v_25 (ite ?v_22 16 27) 16)))))))) (let ((?v_29 (= ?v_26 24)) (?v_69 (ite SBWRITE_S2_INIT false true))) (let ((?v_30 (ite ?v_69 false true)) (?v_31 (= ?v_6 17)) (?v_35 (= ?v_26 27)) (?v_67 (ite MCREAD_S2_INIT false true)) (?v_28 (ite DELAYEDMCWRITE_S2_INIT (store DMEM_INIT DELAYEDMCADDR_S2_INIT (ite DRVMCDBDATA_S2_INIT SBOUT_S2_INIT NO_VALUE)) DMEM_INIT)) (?v_110 (= EXTSTATE_S1_INIT 18)) (?v_27 (= FLUSHSTATE_S1_INIT 26))) (let ((?v_32 (ite (ite (ite ?v_25 true ?v_35) (ite MCREQ_S2_INIT ?v_67 false) false) (store ?v_28 (ite (ite ?v_110 true ?v_27) (ite ?v_27 DTAGDATA1_S1_INIT MISSADDR_S1W_INIT) NO_VALUE) (ite (ite SB_SEL_S2_INIT SB_RW_S2_INIT false) SB_OUT_S2_INIT NO_VALUE)) ?v_28)) (?v_83 (ite DTAG1_RW1_S2_INIT false true))) (let ((?v_36 (ite ?v_83 DTAG1_ADDR_S2_INIT DTAG1_DATA_INIT)) (?v_34 (ite ?v_21 (ite (ite DCACHESEL_S2M_INIT DCACHERW_S2M_INIT false) DCACHE_OUT_S2_INIT NO_VALUE) NO_VALUE)) (?v_37 (ite ?v_33 (ite (ite DTAG1_SEL1_S2_INIT DTAG1_RW1_S2_INIT false) DTAG1_CACHEDOUT_S2_INIT NO_VALUE) DTAGDATA1_S1_INIT)) (?v_159 (ite SB_RW_S2_INIT false true))) (let ((?v_38 (ite ?v_159 SB_IN_S2_INIT SB_DATA_INIT))) (let ((?v_40 (ite (ite ?v_29 (ite ?v_30 ?v_31 false) false) (store ?v_32 ?v_36 ?v_34) (ite (ite ?v_29 (ite ?v_30 (ite ?v_31 false true) false) false) (store ?v_32 ?v_37 ?v_34) (ite (ite (ite (= ?v_26 16) false true) (ite (ite (= ?v_26 21) false true) (ite ?v_35 false true) false) false) (ite ?v_31 (store ?v_32 ?v_36 ?v_38) (store ?v_32 ?v_37 ?v_38)) ?v_32)))) (?v_46 (ite ?v_39 STOREADDR_S2M_INIT STOREADDR_S1W_INIT)) (?v_45 (ite ?v_39 STOREDATA_S2M_INIT STOREDATA_S1W_INIT))) (let ((?v_58 (ite ?v_16 (store ?v_40 ?v_46 ?v_45) ?v_40)) (?v_59 (ite ?v_42 true (ite ?v_43 true ?v_44))) (?v_60 (ite DCACHEDATAVALID_S2_INIT MCDBDATA_OUT_S2_INIT MCDATA_S1_INIT)) (?v_137 (ite DCACHERW_S2M_INIT false true))) (let ((?v_47 (ite (ite (ite (ite DIRTY1_RW1_S2_INIT false true) DIRTY1_IN_S2_INIT DIRTY1_DATA_INIT) (ite (ite (ite VALID1_RW1_S2_INIT false true) VALID1_IN_S2_INIT VALID1_DATA_INIT) (ite ?v_29 false true) false) false) (store ?v_40 ?v_36 (ite ?v_137 DCACHE_IN_S2_INIT DCACHE_DATA_INIT)) ?v_40))) (let ((?v_61 (ite ?v_16 (store ?v_47 ?v_46 ?v_45) ?v_47))) (let ((?v_63 (ite (ite (ite (ite (ite (ite ?v_54 (ite (ite ?v_16 ?v_55 false) true ?v_56) false) true ?v_57) false true) false true) ?v_17 false) (ite ?v_18 ?v_58 (store ?v_40 ?v_36 (ite (ite (ite ?v_16 ?v_11 false) true ?v_59) ?v_45 ?v_60))) ?v_61))) (let ((?v_64 (ite (ite (ite ?v_62 false INSTRISSTORE_S2M_INIT) ?v_53 false) (store ?v_63 (ite ?v_62 MEMADDR_S1M_INIT STOREADDR_S2M_INIT) (ite ?v_62 STOREDATA_S1M_INIT STOREDATA_S2M_INIT)) ?v_63)) (?v_41 (ite ?v_10 false true))) (let ((?v_49 (ite (ite (ite (ite (ite (ite ?v_54 (ite (ite ?v_16 (ite ?v_41 ?v_55 false) false) true ?v_56) false) true ?v_57) false true) false true) ?v_17 false) (ite ?v_18 ?v_58 (store ?v_40 ?v_36 (ite (ite (ite ?v_16 (ite ?v_41 ?v_11 false) false) true ?v_59) ?v_45 ?v_60))) ?v_61)) (?v_71 (ite INSTRISSTORE_S2M_INIT false true)) (?v_161 (= XFERCNT_S2_INIT 28)) (?v_97 (ite DTAGREAD_S2M_INIT false true)) (?v_85 (ite DIRTY1_SEL1_S2_INIT false true)) (?v_82 (ite DIRTY1_DATA_INIT false true)) (?v_81 (ite VALID1_DATA_INIT false true)) (?v_86 (ite VALID1_SEL1_S2_INIT false true)) (?v_120 (= EXTSTATE_S1_INIT 11)) (?v_143 (ite CONFSTALL_S2M_INIT false true)) (?v_142 (ite INSTRISSTORE_S1W_INIT false true)) (?v_77 (ite RESTARTED_S2_INIT false true)) (?v_107 (ite MCREQ_S2_INIT false true)) (?v_103 (ite DELAYEDMCWRITE_S2_INIT false true)) (?v_139 (ite DCACHEMISS_S1W_INIT false true)) (?v_102 (ite MCREAD_S1_INIT false true)) (?v_99 (= CONFSTATE_S1_INIT 10)) (?v_151 (ite RESTARTED_S1_INIT false true)) (?v_104 (ite SBWRITE_S1_INIT false true)) (?v_92 (ite ?v_76 true ?v_25))) (let ((?v_79 (ite ?v_75 true ?v_92)) (?v_114 (= CONFSTATE_S2_INIT 14)) (?v_160 (ite ?v_21 true ?v_74)) (?v_80 (ite ?v_73 true ?v_23))) (let ((?v_174 (ite ?v_3 true ?v_80)) (?v_84 (ite ?v_75 true DEXTREAD_S2_INIT)) (?v_87 (= EXTSTATE_S1_INIT 17)) (?v_88 (ite ?v_3 false true)) (?v_89 (= EXTSTATE_S1_INIT 19)) (?v_144 (= EXTSTATE_S1_INIT 22))) (let ((?v_90 (ite ?v_144 true DCACHERW_S2M_INIT)) (?v_166 (ite ?v_25 true ?v_76))) (let ((?v_91 (ite (ite ?v_78 VALID1_IN_S2_INIT false) true ?v_166)) (?v_93 (ite ?v_92 true SBWRITE_S1_INIT)) (?v_95 (ite VALID1_IN_S2_INIT DCACHEDATAVALID_S1_INIT false))) (let ((?v_94 (ite DTAGREAD_S2M_INIT true ?v_95)) (?v_96 (ite ?v_95 false true)) (?v_119 (ite ?v_66 false true)) (?v_98 (ite ?v_20 true DEXTREAD_S2_INIT))) (let ((?v_100 (ite VALID1_RW1_S2_INIT true ?v_98)) (?v_101 (ite ?v_97 false true)) (?v_105 (ite DRVMCDBDATA_S1_INIT true ?v_25)) (?v_106 (ite REQWAITING_S2_INIT true CCMEMREQ_S1_INIT)) (?v_108 (ite MCREQ_S2_INIT MCREAD_S1_INIT MCDBDATAVALID_S1_INIT)) (?v_109 (ite (ite (ite REQWAITING_S1B_INIT false true) MCREQ_S2_INIT false) true REQWAITING_S1B_INIT)) (?v_124 (ite INSTRISSTORE_S1W_INIT ?v_0 false)) (?v_116 (ite ?v_23 INSTRISSTORE_S1W_INIT false))) (let ((?v_111 (ite ?v_9 true (ite ?v_2 true ?v_116))) (?v_112 (= DCACHE_IN_S2_INIT STOREDATA_S1W_INIT)) (?v_113 (= DCACHE_IN_S2_INIT MCDATA_S1_INIT)) (?v_117 (ite ?v_114 true (ite ?v_20 true SBWRITE_S2_INIT))) (?v_125 (ite ?v_0 ?v_66 false)) (?v_118 (ite (ite (ite VALID1_IN_S2_INIT true ?v_73) DCACHEDATAVALID_S1_INIT false) true ?v_116))) (let ((?v_136 (ite (ite DCACHEHIT1_S1W_INIT (ite (ite INSTRISSTORE_S1W_INIT ?v_125 false) true ?v_115) false) true ?v_118)) (?v_145 (ite ?v_9 false true)) (?v_138 (ite (ite DCACHEHIT1_S1W_INIT ?v_115 false) true ?v_118)) (?v_163 (ite ?v_0 false true))) (let ((?v_121 (ite ?v_119 true (ite ?v_163 true DCACHEMISS_S1W_INIT))) (?v_156 (ite ?v_120 false true))) (let ((?v_150 (ite ?v_119 true (ite ?v_156 true DCACHEMISS_S1W_INIT)))) (let ((?v_153 (ite ?v_150 false true))) (let ((?v_152 (ite ?v_153 false BWASLOAD_S1W_INIT)) (?v_122 (ite MCREAD_S2_INIT MCREQ_S2_INIT false)) (?v_123 (= (select DMEM_INIT MCADDR_S2_INIT) MCDBDATA_OUT_S2_INIT)) (?v_127 (ite ?v_119 true ?v_2))) (let ((?v_131 (ite ?v_124 true ?v_127)) (?v_128 (= DTAG1_ADDR_S2_INIT STOREADDR_S1W_INIT)) (?v_129 (= DTAG1_ADDR_S2_INIT MISSADDR_S1W_INIT))) (let ((?v_126 (ite ?v_131 ?v_128 ?v_129)) (?v_130 (ite ?v_127 ?v_128 ?v_129)) (?v_132 (= DIRTY1_ADDR_S2_INIT STOREADDR_S1W_INIT)) (?v_133 (= DIRTY1_ADDR_S2_INIT MISSADDR_S1W_INIT)) (?v_134 (ite ?v_66 ?v_0 false))) (let ((?v_135 (ite ?v_127 ?v_132 ?v_133)) (?v_140 (ite DCACHEDATAVALID_S1_INIT ?v_25 ?v_76)) (?v_141 (ite MCREQ_S2_INIT ?v_76 ?v_75)) (?v_178 (ite ?v_142 ?v_143 true)) (?v_146 (ite ?v_114 true (ite (= EXTSTATE_S1_INIT 15) true SBWRITE_S2_INIT))) (?v_147 (ite ?v_9 true (= EXTSTATE_S1_INIT 13))) (?v_148 (ite (ite (ite VALID1_IN_S2_INIT true ?v_144) DCACHEDATAVALID_S1_INIT false) true (ite (= EXTSTATE_S1_INIT 23) INSTRISSTORE_S1W_INIT false)))) (let ((?v_154 (ite (ite DCACHEHIT1_S1W_INIT (ite (ite INSTRISSTORE_S1W_INIT (ite ?v_120 ?v_66 false) false) true ?v_147) false) true ?v_148)) (?v_149 (ite ?v_85 false true)) (?v_155 (ite (ite DCACHEHIT1_S1W_INIT ?v_147 false) true ?v_148)) (?v_157 (ite BWASLOAD_S1W_INIT DCACHEDATAVALID_S1_INIT false))) (let ((?v_158 (ite ?v_157 true ?v_156)) (?v_175 (ite INSTRISLOAD_S2M_INIT true INSTRISSTORE_S2M_INIT)) (?v_169 (= DTAG1_ADDR_S2_INIT STOREADDR_S2M_INIT)) (?v_165 (ite ?v_161 false true))) (let ((?v_162 (ite BWASLOAD_S1W_INIT (ite (ite (ite ?v_3 (ite ?v_165 true ?v_160) false) true ?v_73) DCACHEDATAVALID_S1_INIT false) false))) (let ((?v_164 (ite ?v_162 true ?v_163)) (?v_177 (ite INSTRISLOAD_S2M_INIT INSTRISSTORE_S1W_INIT false)) (?v_167 (= STOREADDR_S1W_INIT STOREADDR_S2M_INIT)) (?v_176 (ite ?v_163 (ite ?v_2 false true) false)) (?v_168 (ite INSTRISSTORE_S2M_INIT ?v_163 false)) (?v_171 (ite ?v_73 true (ite ?v_23 true (ite ?v_3 (ite ?v_78 ?v_22 false) false))))) (let ((?v_170 (ite INSTRISSTORE_S2M_INIT ?v_171 false)) (?v_172 (= DTAG1_ADDR_S2_INIT DTAG1_DATA_INIT)) (?v_173 (= DTAG1_DATA_INIT STOREADDR_S2M_INIT))) (not (ite (ite (= (select (ite (ite (ite ?v_48 ?v_5 INSTRISSTORE_S2M_INIT) ?v_53 false) (store ?v_49 (ite ?v_48 MEMADDR_S1M_INIT STOREADDR_S2M_INIT) (ite ?v_48 STOREDATA_S1M_INIT STOREDATA_S2M_INIT)) ?v_49) ARB_ADDR) (select (ite (ite (ite DOSOMETHING_INIT ?v_50 false) ?v_48 false) (store ?v_64 MEMADDR_S1M_INIT STOREDATA_S1M_INIT) ?v_64) ARB_ADDR)) false true) (ite (ite (ite ?v_65 (ite ?v_71 (ite ?v_0 (ite ?v_19 (ite ?v_66 (ite (ite DEXTREAD_S2_INIT false true) (ite ?v_161 (ite DCACHERW_S2M_INIT (ite (ite DCACHESEL_S2M_INIT false true) (ite DTAG1_RW1_S2_INIT (ite (ite DTAG1_SEL1_S2_INIT false true) (ite ?v_97 (ite DIRTY1_RW1_S2_INIT (ite ?v_85 (ite ?v_82 (ite (ite DIRTY1_CACHEDOUT_S2_INIT false true) (ite ?v_81 (ite (ite VALID1_CACHEDOUT_S2_INIT false true) (ite VALID1_RW1_S2_INIT (ite ?v_86 (ite (ite VALID1_IN_S2_INIT false true) (ite (ite DCACHEDATAVALID_S2_INIT false true) (ite ?v_67 (ite ?v_120 (ite (= FLUSHSTATE_S1_INIT 16) (ite (ite DEXTREQ_S2_INIT false true) (ite ?v_143 (ite ?v_142 (ite ?v_68 (ite ?v_77 (ite ?v_107 (ite SB_RW_S2_INIT (ite ?v_69 (ite ?v_103 (ite (ite DCACHEHIT1_S1W_INIT false true) (ite DIRTY1_IN_S2_INIT (ite (ite DCACHEDATAVALID_S1_INIT false true) (ite ?v_139 (ite ?v_70 (ite (ite REQWAITING_S2_INIT false true) (ite (ite CCMEMREQ_S1_INIT false true) (ite ?v_102 (ite (ite DELAYEDMCWRITE_S1_INIT false true) (ite (= XFERCNT_S1_INIT 28) (ite (ite DRVMCDBDATA_S1_INIT false true) (ite ?v_99 (ite (ite MCDBDATAVALID_S1_INIT false true) (ite (ite MCDBDATAVALID_S2_INIT false true) (ite ?v_151 (ite ?v_104 (ite BWASLOAD_S1W_INIT false true) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) true (ite (ite ?v_65 true ?v_71) (ite (ite ?v_0 true (ite ?v_2 true (ite ?v_20 true (ite ?v_33 true (ite ?v_72 true (ite ?v_73 true (ite ?v_3 true ?v_23))))))) (ite (ite ?v_19 true (ite ?v_21 true (ite ?v_74 true ?v_79))) (ite (ite ?v_66 true (ite ?v_9 true ?v_114)) (ite (ite (ite ?v_72 true ?v_3) (ite ?v_19 true ?v_160) true) (ite (ite ?v_80 (ite ?v_19 true ?v_74) true) (ite (ite ?v_0 (ite ?v_77 (ite ?v_19 true (ite ?v_78 true ?v_79)) false) true) (ite (ite ?v_79 (ite ?v_0 true (ite ?v_2 true ?v_20)) true) (ite (ite ?v_74 (ite ?v_33 true (ite ?v_72 true ?v_174)) true) (ite (ite ?v_21 (ite ?v_33 true (ite ?v_72 true (ite ?v_3 true ?v_73))) true) (ite (= DTAG1_ADDR_S2_INIT VALID1_ADDR_S2_INIT) (ite (ite VALID1_CACHEDOUT_S2_INIT VALID1_DATA_INIT ?v_81) (ite (= FLUSHSTATE_S1_INIT FLUSHSTATE_S2_INIT) (ite (ite DIRTY1_CACHEDOUT_S2_INIT DIRTY1_DATA_INIT ?v_82) (ite (= DTAG1_CACHEDOUT_S2_INIT DTAG1_DATA_INIT) (ite (ite VALID1_RW1_S2_INIT DTAG1_RW1_S2_INIT ?v_83) (ite (= SB_DATA_INIT SB_OUT_S2_INIT) (ite (= DCACHE_ADDR_S2_INIT DIRTY1_ADDR_S2_INIT) (ite (ite DEXTREQ_S2_INIT ?v_84 (ite ?v_84 false true)) (ite (ite DCACHESEL_S2M_INIT DIRTY1_SEL1_S2_INIT ?v_85) (ite (ite DTAG1_SEL1_S2_INIT VALID1_SEL1_S2_INIT ?v_86) (ite (= EXTSTATE_S1_INIT EXTSTATE_S2_INIT) (ite (ite DEXTREAD_S2_INIT ?v_87 (ite ?v_87 false true)) (ite (= DCACHE_DATA_INIT DCACHE_OUT_S2_INIT) (ite (ite DIRTY1_IN_S2_INIT ?v_88 (ite ?v_88 false true)) (ite (ite SBWRITE_S2_INIT ?v_21 ?v_78) (ite (ite VALID1_IN_S2_INIT ?v_89 (ite ?v_89 false true)) (ite (ite DIRTY1_RW1_S2_INIT ?v_90 (ite ?v_90 false true)) (ite (ite DCACHEDATAVALID_S2_INIT ?v_91 (ite ?v_91 false true)) (ite (ite SB_SEL_S2_INIT ?v_93 (ite ?v_93 false true)) (ite (ite VALID1_SEL1_S2_INIT ?v_94 (ite ?v_94 false true)) (ite (ite DTAG1_RW1_S2_INIT ?v_96 (ite ?v_96 false true)) (ite (ite (ite (ite (ite ?v_119 VALID1_RW1_S2_INIT false) true ?v_98) DTAGREAD_S2M_INIT ?v_97) true (ite ?v_100 DTAGREAD_S2M_INIT ?v_97)) (ite (ite (ite (ite (ite (ite (ite ?v_99 false true) VALID1_RW1_S2_INIT false) true ?v_98) false true) ?v_97 ?v_101) true (ite (ite ?v_100 false true) ?v_97 ?v_101)) (ite (ite MCREAD_S2_INIT MCREAD_S1_INIT ?v_102) (ite (ite DELAYEDMCWRITE_S1_INIT DELAYEDMCWRITE_S2_INIT ?v_103) (ite (= XFERCNT_S1_INIT XFERCNT_S2_INIT) (ite (= DELAYEDMCADDR_S1_INIT DELAYEDMCADDR_S2_INIT) (ite (= SB_IN_S1_INIT SB_IN_S2_INIT) (ite (= SBOUT_S1_INIT SBOUT_S2_INIT) (ite (ite RESTARTED_S1_INIT RESTARTED_S2_INIT ?v_77) (ite (= CONFSTATE_S1_INIT CONFSTATE_S2_INIT) (ite (ite SB_RW_S2_INIT ?v_104 (ite ?v_104 false true)) (ite (ite DRVMCDBDATA_S2_INIT ?v_105 (ite ?v_105 false true)) (ite (ite MCBUSY_S2_INIT ?v_106 (ite ?v_106 false true)) (ite (ite CCMEMREQ_S1_INIT MCREQ_S2_INIT ?v_107) (ite (= MISSADDR_S1W_INIT SBDELAYADDR_S2_INIT) (ite (ite MCDBDATAVALID_S2_INIT ?v_108 (ite ?v_108 false true)) (ite (ite REQWAITING_S2_INIT ?v_109 (ite ?v_109 false true)) (ite (= (ite SBWRITE_S1_INIT SBDELAYADDR_S1_INIT NO_VALUE) SB_ADDR_S2_INIT) (ite (= (ite (ite ?v_110 true ?v_76) (ite ?v_76 DTAGDATA1_S1_INIT MISSADDR_S1W_INIT) NO_VALUE) MCADDR_S2_INIT) (ite (ite (ite (ite ?v_124 true ?v_111) ?v_112 ?v_113) true (ite ?v_111 ?v_112 ?v_113)) (ite (ite (ite (ite ?v_117 true ?v_136) DIRTY1_SEL1_S2_INIT ?v_85) true (ite (ite (ite (ite ?v_145 ?v_0 false) true ?v_117) true ?v_138) DIRTY1_SEL1_S2_INIT ?v_85)) (ite (ite (ite (ite (ite (ite ?v_121 ?v_77 false) false true) false BWASLOAD_S1W_INIT) INSTRISLOAD_S2M_INIT ?v_65) true (ite (ite ?v_152 INSTRISLOAD_S2M_INIT ?v_65) true (ite (ite (ite ?v_121 false true) true BWASLOAD_S1W_INIT) INSTRISLOAD_S2M_INIT ?v_65))) (ite (ite (ite ?v_122 ?v_123 false) true (ite ?v_122 ?v_123 true)) (ite (ite ?v_126 true (ite (ite ?v_125 false ?v_126) true (ite (ite ?v_125 true ?v_126) true (ite (ite ?v_125 false ?v_130) true (ite ?v_125 true ?v_130))))) (ite (ite (ite ?v_131 ?v_132 ?v_133) true (ite (ite ?v_134 false ?v_135) true (ite ?v_134 true ?v_135))) (ite (ite ?v_0 true (ite ?v_23 true (ite ?v_3 true (ite ?v_73 true (ite (ite MCREQ_S2_INIT ?v_72 ?v_33) true (ite ?v_33 true (ite ?v_20 true (ite (ite DCACHEMISS_S1W_INIT ?v_2 ?v_0) true (ite DCACHEMISS_S1W_INIT ?v_20 ?v_0))))))))) (ite (ite (ite (ite ?v_136 false true) DCACHERW_S2M_INIT ?v_137) true (ite (ite ?v_138 false true) DCACHERW_S2M_INIT ?v_137)) (ite (ite ?v_66 true (ite ?v_114 true (ite (ite ?v_139 BWASLOAD_S1W_INIT false) ?v_9 ?v_66))) (ite (ite DRVMCDBDATA_S1_INIT DCACHEDATAVALID_S1_INIT true) (ite (ite (ite SBWRITE_S1_INIT ?v_21 (ite DRVMCDBDATA_S1_INIT ?v_25 ?v_19)) true (ite (ite SBWRITE_S1_INIT ?v_74 ?v_19) true (ite (ite SBWRITE_S1_INIT ?v_21 ?v_140) true (ite (ite SBWRITE_S1_INIT ?v_74 ?v_140) true (ite (ite SBWRITE_S1_INIT ?v_21 ?v_141) true (ite (ite SBWRITE_S1_INIT ?v_74 ?v_141) true (ite (ite SBWRITE_S1_INIT ?v_21 ?v_74) true (ite ?v_74 true (ite (ite SBWRITE_S1_INIT ?v_21 ?v_75) true (ite (ite SBWRITE_S1_INIT ?v_74 ?v_75) true (ite ?v_19 true ?v_21))))))))))) (ite ?v_178 (ite (ite (ite (ite (ite ?v_146 true ?v_154) false true) ?v_85 ?v_149) true (ite (ite (ite (ite (ite ?v_145 ?v_120 false) true ?v_146) true ?v_155) false true) ?v_85 ?v_149)) (ite (ite (ite (ite (ite (ite (ite ?v_150 ?v_151 false) false true) false BWASLOAD_S1W_INIT) false true) (ite (ite ?v_152 false true) (ite (ite ?v_153 true BWASLOAD_S1W_INIT) false true) false) false) ?v_65 true) (ite (ite (ite (ite MCREQ_S2_INIT MCREQ_S2_INIT false) false true) ?v_67 true) (ite (ite (ite (ite (ite ?v_153 INSTRISSTORE_S1W_INIT false) false true) (ite (ite (ite ?v_150 ?v_77 false) false true) (ite ?v_153 (ite (ite ?v_153 INSTRISSTORE_S1W_INIT true) false true) false) false) false) ?v_68 true) (ite (ite (ite ?v_154 ?v_155 false) ?v_137 true) (ite (ite ?v_151 true (ite (ite RESTARTED_S1_INIT ?v_156 (ite ?v_156 false true)) true (ite (ite RESTARTED_S1_INIT ?v_157 (ite ?v_157 false true)) true (ite RESTARTED_S1_INIT ?v_158 (ite ?v_158 false true))))) (ite (ite MCREQ_S2_INIT (ite ?v_76 true ?v_72) true) (ite (ite MCREAD_S2_INIT ?v_72 (ite ?v_72 false true)) (ite (ite ?v_159 ?v_160 true) (ite (ite (ite ?v_175 (ite ?v_0 (ite ?v_66 ?v_139 false) false) false) ?v_169 true) (ite (ite (ite RESTARTED_S1_INIT ?v_162 (ite ?v_162 false true)) true (ite RESTARTED_S1_INIT ?v_164 (ite ?v_164 false true))) (ite (ite ?v_177 INSTRISSTORE_S2W_INIT true) (ite (ite (ite INSTRISLOAD_S2M_INIT (ite INSTRISSTORE_S1W_INIT (ite ?v_0 ?v_167 false) false) false) (ite CONFSTALL_S2M_INIT true ?v_9) true) (ite (ite (ite INSTRISLOAD_S2M_INIT ?v_176 false) ?v_142 true) (ite (ite ?v_165 (ite ?v_3 true (ite ?v_21 true ?v_166)) true) (ite (ite (ite ?v_165 ?v_78 false) DCACHEDATAVALID_S1_INIT true) (ite (ite ?v_168 (ite INSTRISSTORE_S1W_INIT ?v_167 false) true) (ite (ite ?v_170 ?v_172 true) (ite (ite (ite INSTRISSTORE_S2M_INIT ?v_80 false) ?v_173 true) (ite (ite ?v_168 ?v_77 true) (ite (ite (ite ?v_3 (ite ?v_78 INSTRISSTORE_S2M_INIT false) false) ?v_169 true) (ite (ite ?v_170 VALID1_DATA_INIT true) (ite (ite (ite ?v_0 INSTRISSTORE_S2M_INIT false) ?v_139 true) (ite (ite (ite INSTRISLOAD_S2M_INIT ?v_171 false) ?v_172 true) (ite (ite (ite INSTRISLOAD_S2M_INIT ?v_80 false) ?v_173 true) (ite (ite (ite ?v_20 true (ite ?v_33 true (ite ?v_72 true (ite ?v_2 true ?v_21)))) ?v_77 true) (ite (ite (ite ?v_3 (ite ?v_78 INSTRISLOAD_S2M_INIT false) false) ?v_169 true) (ite (ite (ite INSTRISLOAD_S2M_INIT ?v_174 false) (= MISSADDR_S1W_INIT STOREADDR_S2M_INIT) true) (ite (ite ?v_80 VALID1_DATA_INIT true) (ite (ite (ite ?v_175 ?v_0 false) DTAGREAD_S2M_INIT true) (ite (ite ?v_176 ?v_66 true) (ite (ite (ite ?v_9 ?v_0 false) (ite ?v_172 VALID1_DATA_INIT false) true) (ite (ite (ite ?v_0 (ite CONFSTALL_S2M_INIT ?v_177 false) false) (ite ?v_169 (ite VALID1_DATA_INIT ?v_167 false) false) true) (ite ?v_178 (ite (ite (ite ?v_0 ?v_177 false) ?v_169 true) (ite (ite ?v_119 ?v_167 true) (ite (ite ?v_124 (= DTAG1_DATA_INIT STOREADDR_S1W_INIT) true) (ite ?v_161 true (ite (= XFERCNT_S2_INIT 29) true ?v_22)) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false) false)) false true) true)))))))))))))))))))))))))))))))))))))))))))))))))))))
(check-sat)
(exit)
