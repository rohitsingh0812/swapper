(set-logic QF_ALIA)
(set-info :source |
Benchmarks used in the followin paper:
Big proof engines as little proof engines: new results on rewrite-based satisfiability procedure
Alessandro Armando, Maria Paola Bonacina, Silvio Ranise, Stephan Schulz. 
PDPAR'05
http://www.ai.dist.unige.it/pdpar05/


|)
(set-info :smt-lib-version 2.0)
(set-info :category "crafted")
(set-info :status unsat)
(declare-fun earray_12 () (Array Int Int))
(declare-fun earray_15 () (Array Int Int))
(declare-fun earray_18 () (Array Int Int))
(declare-fun earray_21 () (Array Int Int))
(declare-fun earray_24 () (Array Int Int))
(declare-fun earray_27 () (Array Int Int))
(declare-fun earray_3 () (Array Int Int))
(declare-fun earray_30 () (Array Int Int))
(declare-fun earray_33 () (Array Int Int))
(declare-fun earray_36 () (Array Int Int))
(declare-fun earray_39 () (Array Int Int))
(declare-fun earray_41 () (Array Int Int))
(declare-fun earray_43 () (Array Int Int))
(declare-fun earray_45 () (Array Int Int))
(declare-fun earray_47 () (Array Int Int))
(declare-fun earray_49 () (Array Int Int))
(declare-fun earray_51 () (Array Int Int))
(declare-fun earray_53 () (Array Int Int))
(declare-fun earray_55 () (Array Int Int))
(declare-fun earray_57 () (Array Int Int))
(declare-fun earray_59 () (Array Int Int))
(declare-fun earray_6 () (Array Int Int))
(declare-fun earray_61 () (Array Int Int))
(declare-fun earray_9 () (Array Int Int))
(declare-fun elem_0 () Int)
(declare-fun elem_1 () Int)
(declare-fun elem_10 () Int)
(declare-fun elem_11 () Int)
(declare-fun elem_13 () Int)
(declare-fun elem_14 () Int)
(declare-fun elem_16 () Int)
(declare-fun elem_17 () Int)
(declare-fun elem_19 () Int)
(declare-fun elem_2 () Int)
(declare-fun elem_20 () Int)
(declare-fun elem_22 () Int)
(declare-fun elem_23 () Int)
(declare-fun elem_25 () Int)
(declare-fun elem_26 () Int)
(declare-fun elem_28 () Int)
(declare-fun elem_29 () Int)
(declare-fun elem_31 () Int)
(declare-fun elem_32 () Int)
(declare-fun elem_34 () Int)
(declare-fun elem_35 () Int)
(declare-fun elem_37 () Int)
(declare-fun elem_38 () Int)
(declare-fun elem_4 () Int)
(declare-fun elem_40 () Int)
(declare-fun elem_42 () Int)
(declare-fun elem_44 () Int)
(declare-fun elem_46 () Int)
(declare-fun elem_48 () Int)
(declare-fun elem_5 () Int)
(declare-fun elem_50 () Int)
(declare-fun elem_52 () Int)
(declare-fun elem_54 () Int)
(declare-fun elem_56 () Int)
(declare-fun elem_58 () Int)
(declare-fun elem_60 () Int)
(declare-fun elem_7 () Int)
(declare-fun elem_8 () Int)
(declare-fun a () (Array Int Int))
(declare-fun i () Int)
(assert (= earray_12 (store earray_9 elem_10 elem_11)))
(assert (= earray_15 (store earray_12 elem_13 elem_14)))
(assert (= earray_18 (store earray_15 elem_16 elem_17)))
(assert (= earray_21 (store earray_18 elem_19 elem_20)))
(assert (= earray_24 (store earray_21 elem_22 elem_23)))
(assert (= earray_27 (store earray_24 elem_25 elem_26)))
(assert (= earray_3 (store a elem_0 elem_2)))
(assert (= earray_30 (store earray_27 elem_28 elem_29)))
(assert (= earray_33 (store earray_30 elem_31 elem_32)))
(assert (= earray_36 (store earray_33 elem_34 elem_35)))
(assert (= earray_39 (store a elem_31 elem_38)))
(assert (= earray_41 (store earray_39 elem_28 elem_40)))
(assert (= earray_43 (store earray_41 elem_25 elem_42)))
(assert (= earray_45 (store earray_43 elem_22 elem_44)))
(assert (= earray_47 (store earray_45 elem_19 elem_46)))
(assert (= earray_49 (store earray_47 elem_16 elem_48)))
(assert (= earray_51 (store earray_49 elem_13 elem_50)))
(assert (= earray_53 (store earray_51 elem_10 elem_52)))
(assert (= earray_55 (store earray_53 elem_7 elem_54)))
(assert (= earray_57 (store earray_55 elem_4 elem_56)))
(assert (= earray_59 (store earray_57 elem_0 elem_58)))
(assert (= earray_6 (store earray_3 elem_4 elem_5)))
(assert (= earray_61 (store earray_59 i elem_60)))
(assert (= earray_9 (store earray_6 elem_7 elem_8)))
(assert (= elem_0 (+ i 1)))
(assert (= elem_1 (select a i)))
(assert (= elem_10 (+ elem_7 1)))
(assert (= elem_11 (+ elem_8 1)))
(assert (= elem_13 (+ elem_10 1)))
(assert (= elem_14 (+ elem_11 1)))
(assert (= elem_16 (+ elem_13 1)))
(assert (= elem_17 (+ elem_14 1)))
(assert (= elem_19 (+ elem_16 1)))
(assert (= elem_2 (+ elem_1 1)))
(assert (= elem_20 (+ elem_17 1)))
(assert (= elem_22 (+ elem_19 1)))
(assert (= elem_23 (+ elem_20 1)))
(assert (= elem_25 (+ elem_22 1)))
(assert (= elem_26 (+ elem_23 1)))
(assert (= elem_28 (+ elem_25 1)))
(assert (= elem_29 (+ elem_26 1)))
(assert (= elem_31 (+ elem_28 1)))
(assert (= elem_32 (+ elem_29 1)))
(assert (= elem_34 (+ elem_31 1)))
(assert (= elem_35 (+ elem_32 1)))
(assert (= elem_37 (select a elem_34)))
(assert (= elem_38 (- elem_37 1)))
(assert (= elem_4 (+ elem_0 1)))
(assert (= elem_40 (- elem_38 1)))
(assert (= elem_42 (- elem_40 1)))
(assert (= elem_44 (- elem_42 1)))
(assert (= elem_46 (- elem_44 1)))
(assert (= elem_48 (- elem_46 1)))
(assert (= elem_5 (+ elem_2 1)))
(assert (= elem_50 (- elem_48 1)))
(assert (= elem_52 (- elem_50 1)))
(assert (= elem_54 (- elem_52 1)))
(assert (= elem_56 (- elem_54 1)))
(assert (= elem_58 (- elem_56 1)))
(assert (= elem_60 (- elem_58 1)))
(assert (= elem_7 (+ elem_4 1)))
(assert (= elem_8 (+ elem_5 1)))
(assert (= earray_36 earray_61))
(assert (not (= elem_35 elem_37)))
(check-sat)
(exit)
