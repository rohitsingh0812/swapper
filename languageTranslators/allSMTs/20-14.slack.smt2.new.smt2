(declare-fun x0_minus () Int)
(declare-fun x0_plus () Int)
(declare-fun x10_minus () Int)
(declare-fun x10_plus () Int)
(declare-fun x11_minus () Int)
(declare-fun x11_plus () Int)
(declare-fun x12_minus () Int)
(declare-fun x12_plus () Int)
(declare-fun x13_minus () Int)
(declare-fun x13_plus () Int)
(declare-fun x14_minus () Int)
(declare-fun x14_plus () Int)
(declare-fun x15_minus () Int)
(declare-fun x15_plus () Int)
(declare-fun x16_minus () Int)
(declare-fun x16_plus () Int)
(declare-fun x17_minus () Int)
(declare-fun x17_plus () Int)
(declare-fun x18_minus () Int)
(declare-fun x18_plus () Int)
(declare-fun x19_minus () Int)
(declare-fun x19_plus () Int)
(declare-fun x1_minus () Int)
(declare-fun x1_plus () Int)
(declare-fun x2_minus () Int)
(declare-fun x2_plus () Int)
(declare-fun x3_minus () Int)
(declare-fun x3_plus () Int)
(declare-fun x4_minus () Int)
(declare-fun x4_plus () Int)
(declare-fun x5_minus () Int)
(declare-fun x5_plus () Int)
(declare-fun x6_minus () Int)
(declare-fun x6_plus () Int)
(declare-fun x7_minus () Int)
(declare-fun x7_plus () Int)
(declare-fun x8_minus () Int)
(declare-fun x8_plus () Int)
(declare-fun x9_minus () Int)
(declare-fun x9_plus () Int)
(assert (not (< x0_minus 0)))
(assert (not (< x0_plus 0)))
(assert (not (< x10_minus 0)))
(assert (not (< x10_plus 0)))
(assert (not (< x11_minus 0)))
(assert (not (< x11_plus 0)))
(assert (not (< x12_minus 0)))
(assert (not (< x12_plus 0)))
(assert (not (< x13_minus 0)))
(assert (not (< x13_plus 0)))
(assert (not (< x14_minus 0)))
(assert (not (< x14_plus 0)))
(assert (not (< x15_minus 0)))
(assert (not (< x15_plus 0)))
(assert (not (< x16_minus 0)))
(assert (not (< x16_plus 0)))
(assert (not (< x17_minus 0)))
(assert (not (< x17_plus 0)))
(assert (not (< x18_minus 0)))
(assert (not (< x18_plus 0)))
(assert (not (< x19_minus 0)))
(assert (not (< x19_plus 0)))
(assert (not (< x1_minus 0)))
(assert (not (< x1_plus 0)))
(assert (not (< x2_minus 0)))
(assert (not (< x2_plus 0)))
(assert (not (< x3_minus 0)))
(assert (not (< x3_plus 0)))
(assert (not (< x4_minus 0)))
(assert (not (< x4_plus 0)))
(assert (not (< x5_minus 0)))
(assert (not (< x5_plus 0)))
(assert (not (< x6_minus 0)))
(assert (not (< x6_plus 0)))
(assert (not (< x7_minus 0)))
(assert (not (< x7_plus 0)))
(assert (not (< x8_minus 0)))
(assert (not (< x8_plus 0)))
(assert (not (< x9_minus 0)))
(assert (not (< x9_plus 0)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (- x7_minus) x5_minus) x18_minus) x15_minus) (* (- 3) x11_minus)) x1_minus) (- x0_minus)) x7_plus) (- x5_plus)) (- x18_plus)) (- x15_plus)) (* 3 x11_plus)) (- x1_plus)) x0_plus) 0)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (- x5_minus) x19_minus) x17_minus) x12_minus) (- x11_minus)) x5_plus) (- x19_plus)) (- x17_plus)) (- x12_plus)) x11_plus) 0)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (- x8_minus) (- x7_minus)) (* (- 2) x4_minus)) (- x13_minus)) (- x12_minus)) (- x11_minus)) x10_minus) (- x1_minus)) (* (- 2) x0_minus)) x8_plus) x7_plus) (* 2 x4_plus)) x13_plus) x12_plus) x11_plus) (- x10_plus)) x1_plus) (* 2 x0_plus)) 0)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ x5_minus (* (- 2) x2_minus)) (* 2 x15_minus)) x12_minus) (- x11_minus)) (* 2 x0_minus)) (- x5_plus)) (* 2 x2_plus)) (* (- 2) x15_plus)) (- x12_plus)) x11_plus) (* (- 2) x0_plus)) 0)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ x6_minus x4_minus) x19_minus) (- x18_minus)) (- x16_minus)) x13_minus) (- x11_minus)) (- x1_minus)) (- x6_plus)) (- x4_plus)) (- x19_plus)) x18_plus) x16_plus) (- x13_plus)) x11_plus) x1_plus) 0)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (* 2 x9_minus) x6_minus) (* (- 2) x5_minus)) x2_minus) x19_minus) (- x13_minus)) (* (- 2) x0_minus)) (* (- 2) x9_plus)) (- x6_plus)) (* 2 x5_plus)) (- x2_plus)) (- x19_plus)) x13_plus) (* 2 x0_plus)) 0)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ x7_minus x2_minus) (- x19_minus)) (- x17_minus)) (- x15_minus)) (- x14_minus)) (- x13_minus)) x12_minus) (- x10_minus)) (- x0_minus)) (- x7_plus)) (- x2_plus)) x19_plus) x17_plus) x15_plus) x14_plus) x13_plus) (- x12_plus)) x10_plus) x0_plus) 0)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (- x9_minus) (- x8_minus)) x4_minus) x18_minus) (- x15_minus)) x12_minus) (- x10_minus)) (- x1_minus)) x0_minus) x9_plus) x8_plus) (- x4_plus)) (- x18_plus)) x15_plus) (- x12_plus)) x10_plus) x1_plus) (- x0_plus)) 0)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ x7_minus x3_minus) (- x17_minus)) (- x16_minus)) (- x14_minus)) x12_minus) x10_minus) (- x7_plus)) (- x3_plus)) x17_plus) x16_plus) x14_plus) (- x12_plus)) (- x10_plus)) 0)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ x9_minus (* (- 2) x7_minus)) x6_minus) x5_minus) x3_minus) x0_minus) (- x9_plus)) (* 2 x7_plus)) (- x6_plus)) (- x5_plus)) (- x3_plus)) (- x0_plus)) (- 1))))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (* (- 2) x8_minus) x7_minus) x5_minus) x2_minus) (- x19_minus)) (- x18_minus)) x17_minus) x0_minus) (* 2 x8_plus)) (- x7_plus)) (- x5_plus)) (- x2_plus)) x19_plus) x18_plus) (- x17_plus)) (- x0_plus)) 1)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ x7_minus (* 2 x5_minus)) x4_minus) (- x15_minus)) x13_minus) (- x0_minus)) (- x7_plus)) (* (- 2) x5_plus)) (- x4_plus)) x15_plus) (- x13_plus)) x0_plus) 1)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (* (- 2) x6_minus) x3_minus) (- x18_minus)) x17_minus) x15_minus) x12_minus) x10_minus) x1_minus) (* 2 x6_plus)) (- x3_plus)) x18_plus) (- x17_plus)) (- x15_plus)) (- x12_plus)) (- x10_plus)) (- x1_plus)) 0)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (- x8_minus) (* 3 x6_minus)) x18_minus) x15_minus) x1_minus) x8_plus) (* (- 3) x6_plus)) (- x18_plus)) (- x15_plus)) (- x1_plus)) 1)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (- x9_minus) (* (- 2) x6_minus)) (- x4_minus)) (- x3_minus)) x19_minus) x17_minus) x16_minus) x10_minus) x1_minus) x9_plus) (* 2 x6_plus)) x4_plus) x3_plus) (- x19_plus)) (- x17_plus)) (- x16_plus)) (- x10_plus)) (- x1_plus)) 0)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (- x9_minus) x8_minus) x6_minus) x5_minus) x4_minus) (- x2_minus)) (* 2 x19_minus)) x16_minus) (- x12_minus)) x0_minus) x9_plus) (- x8_plus)) (- x6_plus)) (- x5_plus)) (- x4_plus)) x2_plus) (* (- 2) x19_plus)) (- x16_plus)) x12_plus) (- x0_plus)) 1)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ x8_minus (- x2_minus)) (- x17_minus)) x16_minus) x15_minus) (- x13_minus)) (* (- 2) x10_minus)) (* 2 x1_minus)) (- x8_plus)) x2_plus) x17_plus) (- x16_plus)) (- x15_plus)) x13_plus) (* 2 x10_plus)) (* (- 2) x1_plus)) (- 1))))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ x6_minus (- x5_minus)) x18_minus) x17_minus) x16_minus) x12_minus) (* 2 x1_minus)) (- x0_minus)) (- x6_plus)) x5_plus) (- x18_plus)) (- x17_plus)) (- x16_plus)) (- x12_plus)) (* (- 2) x1_plus)) x0_plus) 0)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ x6_minus x4_minus) (- x18_minus)) (* (- 2) x16_minus)) (- x15_minus)) (- x13_minus)) (- x6_plus)) (- x4_plus)) x18_plus) (* 2 x16_plus)) x15_plus) x13_plus) 0)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ x8_minus x3_minus) x2_minus) x18_minus) (- x13_minus)) (- x10_minus)) (- x8_plus)) (- x3_plus)) (- x2_plus)) (- x18_plus)) x13_plus) x10_plus) (- 1))))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (- x9_minus) (- x8_minus)) (- x16_minus)) x14_minus) x9_plus) x8_plus) x16_plus) (- x14_plus)) 0)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ x3_minus x2_minus) (- x19_minus)) (- x17_minus)) (- x16_minus)) (- x14_minus)) (- x10_minus)) (- x3_plus)) (- x2_plus)) x19_plus) x17_plus) x16_plus) x14_plus) x10_plus) 0)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (- x8_minus) (- x4_minus)) (- x3_minus)) (- x2_minus)) x18_minus) x8_plus) x4_plus) x3_plus) x2_plus) (- x18_plus)) (- 1))))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (* 2 x8_minus) (- x5_minus)) x18_minus) x15_minus) x14_minus) x12_minus) (* (- 2) x8_plus)) x5_plus) (- x18_plus)) (- x15_plus)) (- x14_plus)) (- x12_plus)) (- 1))))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (- x9_minus) (- x8_minus)) x7_minus) (- x4_minus)) x3_minus) x17_minus) (* 2 x14_minus)) x11_minus) (- x1_minus)) x9_plus) x8_plus) (- x7_plus)) x4_plus) (- x3_plus)) (- x17_plus)) (* (- 2) x14_plus)) (- x11_plus)) x1_plus) 1)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ x5_minus x4_minus) x19_minus) (* (- 2) x17_minus)) x15_minus) x0_minus) (- x5_plus)) (- x4_plus)) (- x19_plus)) (* 2 x17_plus)) (- x15_plus)) (- x0_plus)) 1)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ x8_minus (- x7_minus)) (- x6_minus)) (- x5_minus)) x16_minus) (- x15_minus)) (- x0_minus)) (- x8_plus)) x7_plus) x6_plus) x5_plus) (- x16_plus)) x15_plus) x0_plus) (- 1))))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (- x9_minus) x5_minus) x3_minus) (- x19_minus)) x13_minus) (- x12_minus)) (* 2 x11_minus)) x9_plus) (- x5_plus)) (- x3_plus)) x19_plus) (- x13_plus)) x12_plus) (* (- 2) x11_plus)) 1)))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ x9_minus (- x4_minus)) x3_minus) x2_minus) x15_minus) x11_minus) x10_minus) (- x1_minus)) (- x9_plus)) x4_plus) (- x3_plus)) (- x2_plus)) (- x15_plus)) (- x11_plus)) (- x10_plus)) x1_plus) (- 1))))
(assert (not (< (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (+ (- x9_minus) x8_minus) x7_minus) (- x6_minus)) x18_minus) x15_minus) x13_minus) (- x12_minus)) (- x10_minus)) x1_minus) x0_minus) x9_plus) (- x8_plus)) (- x7_plus)) x6_plus) (- x18_plus)) (- x15_plus)) (- x13_plus)) x12_plus) x10_plus) (- x1_plus)) (- x0_plus)) 0)))
