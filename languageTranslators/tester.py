#!/usr/bin/env python
from z3 import *
import MySQLdb
import math
import os
import time
db = MySQLdb.connect(host="localhost", user="root", passwd="", db="results")
cur = db.cursor()


def doThings(fileName, cegis):
	os.system("ulimit -s unlimited")
	print "Converting smt to Temp DAG"
	os.system("python smtToTemp.py " + fileName + " > " + fileName + ".temp")
	print "Converting temp DAG to full DAG via post processing"
	os.system("python tempToDAG.py " + fileName + ".temp " + fileName + ".dag" )
	print "Converting that DAG back into SMT"
	os.system("python dagToSMT.py " + fileName + ".dag" + " > " + fileName + ".new.smt2")
	print "Rewritting the DAG"
	os.system(cegis + " --rewrite " + fileName + ".dag " + fileName + ".rewritten.dag lol")
	print "Converting the rewritten DAG"
	os.system("python dagToSMT.py " + fileName + ".rewritten.dag" + " > " + fileName + ".rewritten.smt2")


def main(argv):
	if (not argv[0].endswith(".smt2")) or ("new.smt2" in argv[0]) or ("rewritten.smt2" in argv[0]):
		return 0
	doThings(argv[0], argv[2])
	smt1 = argv[0]
	smt2 = argv[0] + ".new.smt2"
	smt3 = argv[0] + ".rewritten.smt2"
	numRuns = int(argv[1])
	print "Checking that the smts are semantically same"
	if not (compareSMTs(smt1, smt2) and compareSMTs(smt1, smt3)):
		return 0
	print "Starting time check of original smt"
	(origTime, soln1) = timeCheck(smt1, numRuns)
	if origTime == 0:
		return 0
	print "Starting time check of new smt"
	(newTime, soln2) = timeCheck(smt2, numRuns)
	print "Starting time check of rewritten smt"
	(rewrittenTime, soln3) = timeCheck(smt3, numRuns)
	cleanUp(smt1, origTime, newTime, rewrittenTime)


def cleanUp(fileName, origTime, newTime, rewrittenTime):
	numlines1 = sum(1 for line in open(fileName + '.dag'))
	numlines2 = sum(1 for line in open(fileName + '.rewritten.dag'))
	statement = "INSERT into rewritten_results_seis (name, originalTime, newTime, rewrittenTime, origNodeNum, rewrittenNodeNum) VALUES (%s, %f, %f, %f, %d, %d);" % ("'" + fileName + "'", origTime, newTime, rewrittenTime, numlines1, numlines2)
	print statement
	cur.execute(statement)
	cur.close()
	db.commit()
	db.close()
	deleteFiles(fileName)


def deleteFiles(fileName):
	os.remove(fileName + ".temp")
	os.remove(fileName + ".dag")
	os.remove(fileName + ".new.smt2")
	os.remove(fileName + ".rewritten.dag")
	os.remove(fileName + ".rewritten.smt2")

	
def timeCheck(fileName, numRuns):
	times = []
	solver = Solver()
	for x in range(numRuns):
		solver.set("soft_timeout", 300000)
		solver.add(parse_smt2_file(fileName))
		start = time.time()
		ans = solver.check()
		end = time.time()
		solver.reset()
		times.append(end-start)
		if end-start > 300.0:
			print "Checking the time took longer than 5 minutes, terminating program"
			deleteFiles(fileName)
			return (0, 0)
		print "Finishing run " + str(x+1) + "/" + str(numRuns)
		print "Time: " + str(end-start) + "s"
	return (sorted(times)[int(math.floor(numRuns/2))], ans)


def compareSMTs(smt1, smt2):
        f1 = parse_smt2_file(smt1)
        f2 = parse_smt2_file(smt2)
        f3 = (f1 != f2)
        solver = Solver()
        solver.set("soft_timeout", 300000)
        solver.add(f3)
        ans = solver.check()
        solver.reset()
        if ans != z3.unsat:
                print "FAILURE: The SMTs are not semantically equivalent for " + smt1
                deleteFiles(smt1)
                return False
        return True


if (__name__=='__main__'):
        main(sys.argv[1:])
