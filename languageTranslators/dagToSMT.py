#!/usr/bin/env python
import sys
import re
import math
#Mappings of DAG Operation Name -> (smt operation name, # of elements in that operation)
opToName = {"ARRACC": ("ite", 3), "AND": ("and", 2), "EQ": ("=", 2), "OR": ("or", 2), "XOR": ("xor", 2), "PLUS": ("+", 2), "TIMES": ("*", 2), "DIV": ("div", 2), "MOD":("mod", 2), "LTE": ("<=", 2), "LT":("<", 2), "GT":(">", 2), "GTE": (">=", 2), "NEG":("-", 1), "NOT": ("not", 1), "ITE": ("ite", 3), "ARR_R": ("select", 2), "ARR_W": ("store", 3), "S": ("declare-fun", 2)}

dagOTypes = {} #map of id to output Type

def Assert(cond,msg):
    if not cond:
        print "ERROR: " + msg
        raise Exception(msg)
        sys.exit(1)

def vn(idv):
    Assert(idv.isdigit(),"Invalid digit id: " + str(idv))
    return "_n" + str(idv)
    
def printAll(ctrls,lets,asserts):
    for (name,typeStr) in ctrls:
        print "(declare-fun " + name + " () " + typeStr + ")"
    print "(assert "
    for letStr in lets:
        print "(let " + letStr
    
    #print and of all asserts
    Assert(len(asserts) >= 1, "asserts is empty!")
    if len(asserts) == 1:
        print vn(asserts[0])
    else: 
        print "(and ","".join([" "+vn(idv) for idv in asserts]),")"
    print "".join([")" for i in range(len(lets))])
    print ")"

def getTypeStr(typeDAG):
    typeDict = {
        "INT" : "Int",
        "BIT" : "Bool",
        "BOOL" : "Bool",
        "INT_ARR" : "(Array Int Int)",
        "BOOL_ARR" : "(Array Int Bool)",
        "BIT_ARR" : "(Array Int Bool)"
    }
    Assert(typeDAG in typeDict, "Can't find typeDAG in typeDict: " + typeDAG)
    return typeDict[typeDAG]

def joinTypes(a,b):
	if(a==b):
		return a
	validJoins = {}
	validJoins[("BIT","INT")] = "INT"
	validJoins[("BOOL","INT")] = "INT"
	validJoins[("BIT_ARR","INT_ARR")] = "INT_ARR"
	validJoins[("BOOL_ARR","INT_ARR")] = "INT_ARR"
	if (a,b) in validJoins:
		return validJoins[(a,b)]
	elif (b,a) in validJoins:
		return validJoins[(b,a)]
	else:
		Assert(False,"Couldn't find a join of Types: " + a + " & " +b)



def typeCast(exp,finalType):
    Assert(exp in dagOTypes, "This id was not Otyped: " + str(exp))
    currType = dagOTypes[exp]
    if currType == finalType:
        return exp
    elif (currType in ["BIT","BOOL"]) and finalType == "INT":
        return " (ite " + exp + "  1 0) "
    elif currType == "INT" and (finalType in ["BIT","BOOL"]):
        return " (> " + exp + " 0 ) "
    else:
        Assert(False, "Type Casting not possible: " + str(exp))

def makeBool(exp):
    currType = dagOTypes[exp]
    if currType in ["BIT","BOOL"]:
        return exp
    elif currType == "INT":
        return typeCast(exp,"BIT")
    else:
        Assert(False,"can't make bool of: " + str(exp))
        
def makeInt(exp):
    currType = dagOTypes[exp]
    if currType == "INT":
        return exp
    elif currType in ["BIT","BOOL"]:
        return typeCast(exp,"INT")
    else:
        Assert(False,"can't make int of: " + str(exp))
        
def makeEqualTypes(e1,e2):
    cT1 = dagOTypes[e1]
    cT2 = dagOTypes[e2]
    if (cT1 == cT2):
        return (e1,e2)
    elif cT1 in ["BIT","BOOL"] and cT2 == "INT":
        return (makeInt(e1),e2)
    elif cT2 in ["BIT","BOOL"] and cT1 == "INT":
        return (e1,makeInt(e2))
    else:
        Assert(False,"can't make equal: " + str(e1) + " | " + str(e2)) 
def getLetStmt(elt,asserts,ctrls):
    retStr = "((" + vn(elt[0]) + " "
    Assert(elt[1] == "=", "Invalid DAG format")
    #generate computation expression based on elt[2]
    #opToName =  "ARR_R": ("select", 2), "ARR_W": ("store", 3)
    op = elt[2] #elt[3] is retType
    retType = elt[3]
    if op == "ARRACC": #id = ARRACC retType ifval 2 elseval thenval
        Assert(elt[5] == "2", "ARRACC size not specified")
        Assert(len(elt) == 8, "Not enough ARRACC params")
        ifval = makeBool(vn(elt[4]))
        thenval = vn(elt[7])
        elseval = vn(elt[6])
        (thenval,elseval) = makeEqualTypes(thenval,elseval)
        retStr = retStr +  " (ite " + ifval + " " + thenval + " " + elseval + ")"
    elif op == "ARR_W":# id = ARR_W retType index arr val 
        Assert(len(elt) == 7, "Not enough ARRACC params")
        indexval = makeInt(vn(elt[4]))
        arrval = vn(elt[5]) #has to be some array
        if dagOTypes[arrval] == "INT_ARR":
            newval = makeInt(vn(elt[6]))
        elif dagOTypes[arrval] in ["BIT_ARR","BOOL_ARR"]:
            newval = makeBool(vn(elt[6]))
        else:
            Assert(False,"Invalid array otype: " + str(elt))
        retStr = retStr +  " (store " + arrval + " " + indexval + " " + newval + ")"
    elif op in ["AND","OR","XOR"]:
        Assert(len(elt) == 6, "Boolean Binary Op size of elt invalid: " + str(elt))
        leftval = makeBool(vn(elt[4]))
        rightval = makeBool(vn(elt[5]))
        retStr = retStr + "("+opToName[op][0]+" "+ leftval +" " + rightval +")"
    elif op == "EQ":
        Assert(len(elt) == 6, "EQ Binary Op size of elt invalid: " + str(elt))
        leftval = vn(elt[4])
        rightval = vn(elt[5])
        (leftval,rightval) = makeEqualTypes(leftval,rightval)
        retStr = retStr + "("+opToName[op][0]+" "+ leftval +" " + rightval +")"
    elif op in ["PLUS","TIMES","DIV","MOD","LT"]:
        Assert(len(elt) == 6, "Int Binary Op size of elt invalid: " + str(elt))
        leftval = makeInt(vn(elt[4]))
        rightval = makeInt(vn(elt[5]))
        retStr = retStr + "("+opToName[op][0]+" "+ leftval +" " + rightval +")"    
    elif op == "ARR_R": # id = ARR_R retType index arr
        Assert(len(elt) == 6, "ARR_R Op size of elt invalid: " + str(elt))
        retStr = retStr + "(select "+ vn(elt[5]) +" " + makeInt(vn(elt[4])) +")"
    elif op in ["NEG","NOT"]:
        Assert(len(elt) == 5, "Unary Op size of elt invalid: " + str(elt))
        unval = vn(elt[4])
        if op == "NOT":
            unval = makeBool(unval)
        elif op == "NEG":
            unval = makeInt(unval)
        retStr = retStr + "("+opToName[op][0]+" "+ unval +")"
    elif op == "CONST":
        #id = CONST retType val
        Assert(len(elt) == 5,"CONST size of elt invalid: " + str(elt))
        Assert(retType in ["INT","BIT","BOOL"],"Invalid CONST Type: " + retType)
        if retType == "INT":
            retStr = retStr + " " + elt[4]
        elif elt[4] == "1":
            retStr = retStr + " true"
        elif elt[4] == "0":
            retStr = retStr + " false" 
        else:
            Assert(False,"CONST ERROR weird: " + str(elt)) 
            
    elif op == "CTRL":
        #id = CTRL retType name [bits] #TODO: ignoring bits for now, use them for bounds later!
        Assert(len(elt) in [5,6],"CTRL size of elt invalid" + str(elt))
        ctrls.append((elt[4],getTypeStr(retType)))
        retStr = retStr + elt[4] 
    else:
        Assert(False,"Invalid op = " + op)
    retStr = retStr + "))"
    return retStr
def main(argv):
    Assert(len(argv) >= 1,"DAG Filename not specified") 
        
    filename = argv[0]
    f = open(filename, 'r')
    dagnodes = f.readlines()
    
    
    #Decide if need to print logic type
    if len(argv) >= 2: #QF_LIA or something else
        print "(set-logic ",argv[1],")"
    print "(set-info :smt-lib-version 2.0)"
    
    firstIgnored = False
    asserts = [] #ids of all assert nodes
    ctrls = [] #string names, types
    lets = [] #all let statements ((n<i> expr)) in order
    #final smt file would be 1. declare-funs 2. (assert ALL_LETS ALLASSERTS ) 
    #go over each line, ignore the first one if needed
    for node in dagnodes:
        if node.startswith("dag"): 
            continue
        elements = filter(None,re.split(' |\n|\r',node))
        dagOTypes[vn(elements[0])] = elements[3]
        if not elements[0].isdigit():
            Assert(not firstIgnored, "Second invalid line: " + str(elements))
            firstIgnored = True
            continue 
        #print corresponding let statement
        Assert(len(elements) >= 3, "Inavlid size of elements: " + str(elements))
        if elements[2] != "ASSERT":
            lets.append(getLetStmt(elements,asserts,ctrls))
        else:
            asserts.append(elements[3])
    printAll(ctrls,lets,asserts)
    
    print "(check-sat)"
    if len(argv) >= 3: #stuff for end
        for i in range(2,len(argv)):
            print argv[i]
    print "(exit)"
    #smt_node_print = SMT_Node_Print()
    #smt_node_print.print_smt(dagnodes)
    #smt_node_print.print_end()
if (__name__=='__main__'):
    main(sys.argv[1:])    
