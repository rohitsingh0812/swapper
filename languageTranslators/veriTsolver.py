import subprocess
class Solver:
	progName = "veriT"
	fileName = ""
	options = "--max-time=600"
	def __init__(self, fileName):
		self.fileName = fileName
	def check(self):
		proc = subprocess.Popen([self.progName, self.options, self.fileName], stdout=subprocess.PIPE)
		(out, err) = proc.communicate()
		if "unsat" in out:
			return "unsat"
		elif "sat" in out:
			return "sat"
		else:
			return "error"		
