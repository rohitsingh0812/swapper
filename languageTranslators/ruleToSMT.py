#!/usr/bin/env python
import sys
import re
import math
#Mappings of DAG Operation Name -> (smt operation name, # of elements in that operation)
opToName = {"ARRACC": ("ite", 3), "AND": ("and", 2), "EQ": ("=", 2), "OR": ("or", 2), "XOR": ("xor", 2), "PLUS": ("+", 2), "TIMES": ("*", 2), "DIV": ("div", 2), "MOD":("mod", 2), "LTE": ("<=", 2), "LT":("<", 2), "GT":(">", 2), "GTE": (">=", 2), "NEG":("-", 1), "NOT": ("not", 1), "ITE": ("ite", 3), "ARR_R": ("select", 2), "ARR_W": ("store", 3), "S": ("declare-fun", 2)}
boolPar0 = ["AND","OR","XOR","NOT","ARRACC"]
boolPar1 = ["AND","OR","XOR"]
intPar0 = ["LT","DIV","PLUS","TIMES","MOD","LTE","GT","GTE","NEG"]
intPar1 = ["LT","DIV","PLUS","TIMES","MOD","LTE","GT","GTE","ARR_R","ARR_W"]
intPar2 = ["ARR_W"]

class Rule2SMT():
    def __init__(self,declarations):
        self.decl = declarations
        self.smtText = {} # stores map id -> node smt encoding
        self.form = ""
        self.maxEle = 0
        self.lastElement = 0
    def get_rule_smt(self,dagnodes):
        for node in dagnodes:
            self.process_node(node)
        return (self.decl,self.form)
    def get_smt_s(self, node_arr):
        self.decl.add("(declare-fun " + node_arr[4] + " () " + self.print_type(node_arr[3]) + ")")
        self.smtText[node_arr[0]] = node_arr[4]
    def print_type(self, node_type):
        if node_type == "INT":
            return "Int"
        if node_type == "BOOL" or node_type == "BIT":
            return "Bool"
        if node_type == "INT_ARR":
            return "(Array Int Int)"
        if node_type == "BOOL_ARR":
            return "(Array Int Bool)"
        else:
            raise Exception("Unsupported Type " + s)
    def get_smt_ary_op(self, elements, topLevel):
        if elements[0] in self.smtText:
            return self.smtText[elements[0]]
        self.smtText[elements[0]] = "(" + opToName[elements[2]][0]
        for x in range(opToName[elements[2]][1]):
            self.smtText[elements[0]] += " " + self.get_smt_ary_op([elements[4 + x]], False)
        self.smtText[elements[0]] += ")"
        #if topLevel:
            #print self.smtText[elements[0]]
    def process_node(self, node):
        self.maxEle += 1
        elements = re.split(' |\n|\r',node)
        if(len(elements) <= 2):
            #this the DST node
            self.form = self.smtText[elements[0]]
            return
        elif elements[2] == "ARR_R" or elements[2] == "ARR_W":
            elements[4], elements[5] = elements[5], elements[4]
            self.get_smt_ary_op(elements, True)
        elif elements[2] == "S" or elements[2] == "CTRL":
            self.get_smt_s(elements)
        elif elements[2] == "ARRACC":
            if elements[5] != "2":
                print elements[5]
                print "ARRACC IS POORLY FORMED"
            else:
                del elements[5]
                elements[5], elements[6] = elements[6], elements[5] #ite has opposite order than ARRACC for t,e or 0,1 expressions
                self.get_smt_ary_op(elements, True)    
        elif elements[2] in opToName:
            self.get_smt_ary_op(elements, True)
        elif elements[2] == "CONST":
            if "-" in elements[4]:
                num = elements[4].split("-")[1]
                self.smtText[elements[0]] = "(- "+num+")"
            else:                
                self.smtText[elements[0]] = elements[4]
        elif elements[2] == "ASSERT":
            self.smtText[elements[0]] = "(assert " + self.smtText[elements[3]] + ")"
            print "(assert " + self.smtText[str(self.lastElement)] + ")"
        elif elements[2] not in opToName:
            print "WTF MAN " + elements[2] + "isn't handled"
        self.lastElement = elements[0]
    # def print_end(self):
        # print "(check-sat)"
        # print "(exit)"
def main(argv):
    folder = argv[0]
    symbs = ['d','p','f']
    files= dict()
    form = dict()
    for i in symbs:
        files[i] = folder + '/'+i+'.aux'
    f=dict()
    dagnodes =dict()
    decl = set()
    for i in symbs:
        f[i] = open(files[i], 'r')
        dagnodes[i] = f[i].readlines()
    #print "(set-logic QF_LIA)"
    #print "(set-info :smt-lib-version 2.0)"
        smtobj = Rule2SMT(decl)
        (decl_updated,form_i) = smtobj.get_rule_smt(dagnodes[i])
        decl = decl_updated
        form[i] = form_i
    
    for i in decl:
        print i
    if (form['p'].replace(" ","") == '1'):
        print "(assert (not (= "+form['d']+" "+form['f']+")))" 
    else:
        print "(assert (and "+form['p']+" (not (= "+form['d']+" "+form['f']+"))))"    
    print "(check-sat)"
    print "(get-model)"
    print "(exit)"
    #smt_node_print.print_end()
if (__name__=='__main__'):
    main(sys.argv[1:])    
