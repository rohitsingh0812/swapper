#!/usr/bin/env python
import sys
import re
import os
import argparse
import math
import shutil
from subprocess import call,  Popen, PIPE, check_output, CalledProcessError, STDOUT
from pyparsing import OneOrMore, nestedExpr
#Input: Signature of a dag with ANV info


def d():
    import pdb
    pdb.set_trace()

iB = 'INT(N='
bB = 'BIT(b=' 
opcodes = {
    'AND' : [bB,'getBit','&&','getBit'],
    'XOR' : [bB,'getBit','^','getBit'],
    'OR' : [bB,'getBit','||','getBit'],
    'LT' : [bB,'getIntUpCast','<','getIntUpCast'],
    'MOD' : [iB,'getIntUpCast','%','getIntUpCast'],
    'PLUS' : [iB,'getIntUpCast','+','getIntUpCast'],
    'TIMES' : [iB,'getIntUpCast','*','getIntUpCast'],
    'NOT' : [bB,'!','getBit'],
    'NEG' : [iB,'-','getIntUpCast']
}

funcodes = {
    'EQ': [bB,'checkEq',' ',' '],
    'DIV' : [iB,'divfn','getIntUpCast','getIntUpCast'],
    'DIV' : [iB,'modfn','getIntUpCast','getIntUpCast']
}

def latJoin(a,b):
    if a in ['BOOL','BIT']:
        return b
    elif b in ['BOOL','BIT']:
        return a
    elif a == b:
        return a
    elif a == 'INT_ARR' or b=='INT_ARR':
        return 'INT_ARR'
    elif (a in ['BOOL','BIT'] and b in ['BIT_ARR','BOOL_ARR']) or (b in ['BOOL','BIT'] and a in ['BIT_ARR','BOOL_ARR']):
        return 'BOOL_ARR'
    else:
        #shouldn't happen!
        assert(False)
        
    
    
def strDag(i,dagnodes,typemap,final=None):
    if not (i >=0 and i < len(dagnodes)):
        d()
    op = dagnodes[i][0]
    line = ""
    if op == 'S':
        typemap[i] = dagnodes[i][1]
        line = "%d = S %s %s" %(i,typemap[i],dagnodes[i][2])
    else:
        if op in ['AND','OR','XOR','LT','EQ','NOT']:
            typemap[i] = 'BOOL'
        elif op in ['PLUS','TIMES','DIV','MOD','NEG']:
            typemap[i] = 'INT'
        elif op == 'ARRACC':#b v0 v1
            b=dagnodes[i][1]
            v0=dagnodes[i][2]
            v1=dagnodes[i][3]
            typemap[i] = latJoin(typemap[v0],typemap[v1])
            line = "%d = ARRACC %s %d 2 %d %d" %(i,typemap[i],b,v0,v1)
        elif op == 'ARRASS':#b c v0 v1
            assert(False) #not allowed b c or c b ordering confusion!
        elif op == 'ARR_R':#index array
            ind=dagnodes[i][1]
            arr=dagnodes[i][2]
            if typemap[arr] == 'INT_ARR':
                typemap[i] = 'INT'
            elif typemap[arr] in ['BIT_ARR','BOOL_ARR']:
                typemap[i] = 'BOOL'
            else:
                print typemap,arr
                assert(False)
        elif op == 'ARR_W':#index array newval
            ind = dagnodes[i][1]
            arr=dagnodes[i][2]
            newv = dagnodes[i][3]
            typemap[i] = typemap[arr]
            line = "%d = ARR_W %s %d %d %d" %(i,typemap[i],ind,arr,newv)
        elif op == 'CONST':#CONST TYPE VAL
            line = "%d = CONST %s %d" %(i,dagnodes[i][1],dagnodes[i][2])
        else: #invalid op
            assert(False)
        if line == "":
            #print i,op,typemap[i],dagnodes 
            line= "%d = %s %s " %(i,op,typemap[i]) + ' '.join(map(str, dagnodes[i][1:]))
    if i==len(dagnodes)-1 and final is None:
        return line + "\n" + str(i)
    elif i==len(dagnodes)-1 and final is not None:
        return line + "\n" + str(final)
    else:
        return line + "\n" + strDag(i+1,dagnodes,typemap,final)

def sE(f,arg): #simplifyExpr
    if f=='getBit' and arg.startswith('new BIT(b=') and arg.strip()[-1] == ')':
        return arg[10:].strip()[0:-1]
    elif f in ['getInt','getIntUpCast'] and arg.startswith('new INT(N=') and arg.strip()[-1] == ')':
        return arg[10:].strip()[0:-1]
    elif f in ['getInt','getIntUpCast'] and arg.startswith('new BIT(b=') and arg.strip()[-1] == ')':
        return "(int)(arg[10:].strip()[0:-1])"
    else:
        return "%s(%s)" %(f,arg)

def reorder(dagnodes,srcs,types):
    #SRC nodes aren't at the top, put them there!
    newdag = {}
    for i in range(len(srcs)):
        newdag[i] = ["S",types[srcs[i]],srcs[i]]
    nodemap = {}
    for j in dagnodes:
        if dagnodes[j][0] == "S":
            nodemap[j] = srcs.index(dagnodes[j][2])
        else:
            k = len(newdag)
            newdag[k] = [dagnodes[j][0]] +  map(lambda t: nodemap[t],dagnodes[j][1:])
            nodemap[j] = k
    return newdag
    
    
def traverse(l,anvs,types,srcs,sz,dagnodes):
    if type(l) == str:
        return l
    if type(l) == list: 
        if len(l) == 1 and type(l[0]) ==list:
            return traverse(l[0],anvs,types,srcs,sz,dagnodes)
        elif len(l) >= 1:
            lp = filter(lambda x: x != '|',l)
            op = lp[0]
            assert(type(op) == str)
            #TODO: Deal with Const?
            if op.startswith("S:"):#S:NAME:TYPE:ANV
                comps = filter(None,op.split(':'))
                anv = comps[3]
                name = comps[1]
                if name not in srcs:
                    srcs.append(name)
                else:
                    foundid = -1
                    for j in dagnodes:
                        if(len(dagnodes[j]) >= 3 and dagnodes[j][2] == name):
                            foundid = j
                            break
                    assert(foundid != -1)
                    return ("vals[%d]" % srcs.index(name),foundid)
                typ = comps[2]
                types[name] = typ
                idd = len(dagnodes)
                dagnodes[idd] = ["S",typ,name]
                if anv == "T":
                    anvs[name] = "T"
                elif anv == "L":
                    anvs[name] = map(lambda x: int(x),filter(None,lp[1][0].split('|')))
                    if len(anvs[name]) == 2 and anvs[name][0] == 0 and anvs[name][1] == 1:
                        anvs[name] = (0,1)
                elif anv == "R":
                    #if(type(lp[1]) == list):
                    #    d()
                    rstr = lp[1][0] #'-4--1' (potentially 1,2,3 minuses!)
                    r0 = re.match("^-?[0-9]+",rstr).group()
                    assert(rstr[len(r0)] == "-")
                    r1 = rstr[len(r0)+1:]
                    r=[r0,r1]
                    if r[0] == r[1]:
                        anvs[name] = [int(r[0])]
                    else:
                        anvs[name] = (int(r[0]),int(r[1]))
                return ("vals[%d]" % srcs.index(name),idd)
            else:
                #print op, len(lp)-1
                op = op.replace('|','') 
                
                opers = []
                idopers = [op]
                for sub in lp:
                    if type(sub) == list:
                        (t,idoper) = traverse(sub,anvs,types,srcs,sz,dagnodes)
                        idopers.append(idoper)
                        opers.append(t)
                #check if [op] + opers is already there in the dag! resuse the node in that case
                idd = None                
                for xid in dagnodes:
                    if dagnodes[xid] == idopers:
                        #found existing node
                        idd = xid
                        break
                if idd is None:
                    sz[0] = sz[0] + 1
                    idd = len(dagnodes)
                    dagnodes[idd] = idopers
                if len(opers) ==2 and op in opcodes:
                    #return ('new %s(%s(%s) %s %s(%s)))' %(opcodes[op][0],opcodes[op][1],opers[0],opcodes[op][2],opcodes[op][3],opers[1]),idd)
                    return ('new %s(%s %s %s))' %(opcodes[op][0],sE(opcodes[op][1],opers[0]),opcodes[op][2],sE(opcodes[op][3],opers[1])),idd)
                elif len(opers) ==2 and op in funcodes:
                    #print funcodes[op], opers
                    return ('new %s %s(%s,%s))' %(funcodes[op][0],funcodes[op][1],sE(funcodes[op][2],opers[0]),sE(funcodes[op][3],opers[1])),idd)
                elif len(opers) ==1 and op in opcodes: #NOT, NEG
                    return ('new %s(%s %s))' %(opcodes[op][0],opcodes[op][1],sE(opcodes[op][2],opers[0])),idd)
                elif len(opers) ==3 and op == 'ARRACC': #ARRACC b v0 v1
                    return ('(getBit(%s))?%s:%s' %(opers[0],opers[2],opers[1]),idd)
                elif len(opers) ==4 and op == 'ARRASS': #ARRASS b c v0 v1
                    assert(False)
                    return ('(getIntUpCast(%s) == %s)?%s:%s' %(opers[0],opers[1],opers[4],opers[3]),idd)
                elif len(opers) == 2 and op == 'ARR_R':#ARR_R index arr
                    return ('arrRead(%s,%s)' %(opers[0],opers[1]),idd) #NOTE: arrRead(index,arr)
                elif len(opers) == 3 and op == 'ARR_W':#ARR_W index arr new-val
                    return ('arrWrite(%s,%s,%s)' %(opers[0],opers[1],opers[2]),idd) #NOTE: arrWrite(index,arr,newval)
                d()
                assert(False)
        else:
            return ""
       
def getTypedSRCs(types):
    NBITS=0
    NINTS=0
    NBARRS=0
    NIARRS=0
    for src in types:
        if types[src] in ['BIT','BOOL']:
            NBITS=NBITS+1
        elif types[src] == 'INT':
            NINTS=NINTS+1
        elif types[src] in ['BIT_ARR','BOOL_ARR']:
            NBARRS=NBARRS+1
        elif types[src] == 'INT_ARR':
            NIARRS=NIARRS+1
        else:
            d()
            assert(False)
    return  (NBITS,NINTS,NBARRS,NIARRS)           

def getMainHeader(types,srcs):
    hdr = []
    valdefs = []
    for src in types:
        ind = srcs.index(src)
        
        if types[src] in  ['BIT','BOOL']:
            hdr.append("bit " + src)
            valdefs.append("    vals[%d] = new BIT(b=%s); SETBIT(%d) //%s %s" %(ind,src,ind,src,types[src]))
        elif types[src] == 'INT':
            hdr.append("int " + src)
            valdefs.append("    %s=%s-3;    vals[%d] = new INT(N=%s); SETINT(%d) //%s %s" %(src,src,ind,src,ind,src,types[src]))
        elif types[src] in ['BIT_ARR','BOOL_ARR']:
            #hdr.append("int sz" +src)  
            hdr.append("bit[sz%s] %s" %(src,src))
            valdefs.append("    //assert(sz%s < ARRSZ);\n    vals[%d] = new BARR(N=sz%s,ba=%s); SETBARR(%d)//%s %s" %(src,ind,src,src,ind,src,types[src]))
        elif types[src] == 'INT_ARR':
            #hdr.append("int sz" +src)  
            hdr.append("int[sz%s] %s" %(src,src))
            valdefs.append("    SUB3(%s,sz%s);" % (src,src))
            valdefs.append("    //assert(sz%s < ARRSZ);\n    vals[%d] = new IARR(N=sz%s,ia=%s); SETIARR(%d)//%s %s" %(src,ind,src,src,ind,src,types[src]))
        else:
            d()
            assert(False)
        
    return  (",".join(hdr),"\n".join(valdefs))

dagOP = {
    'And' : 'AND',
    'Or' : 'OR',
    'Xor' : 'XOR',
    'Lt' : 'LT',
    'Eq' : 'EQ',
    'Not' : 'NOT',
    'Src' : 'S',
    'ConstB' : 'CONST',
    'ConstI' : 'CONST',
    'Plus' : 'PLUS',
    'Times' : 'TIMES',
    'Div' : 'DIV',
    'Mod' : 'MOD',
    'Neg' : 'NEG',
    'ArrAcc' : 'ARRACC',
    'ArrAss' : 'ARRASS',
    'ArrR' : 'ARR_R',
    'ArrW' : 'ARR_W',
}

def runSketch(curSig,outFolder,fileCtr,pred,srcs,types,s,dagSize,USEINT,rhssize,outBit,outArr,astInclude,allPreds=None):
    filename = writeSketch(curSig,outFolder,fileCtr,pred,srcs,types,s,dagSize,USEINT,rhssize,outBit,outArr,astInclude,allPreds)
    #call(["sk9l",filename])
    X = ""
    with open(outFolder + "/" + str(fileCtr) + ".out","w") as fout:
        try:
            X = check_output(['/usr/bin/skast','--fe-tempdir',outFolder,'--fe-timeout','5', filename],stderr=STDOUT)
            #skast --fe-tempdir ./ 0.sk
            fout.write(X)
        except CalledProcessError, e:
            X = e.output  
            fout.write(X)
            print ' '.join(filter(lambda x: "ERROR" in x,str(e.output).split('\n')))
            return ({},None,None)
    
        
    #output, err = p.communicate()
    #rc = p.returncode
    if "DONE" not in X:
        print outFolder,fileCtr,": Not solved"
        return ({},None,None)
    dagnodes = {}
    final = None
    for src in types:
        dagnodes[srcs.index(src)] = ['S',types[src],src]
    pval = None
    for line in X.split('\n'):
        if allPreds and "PCHOICE" in line and 'uninterp' not in line:
            pvalList = filter(None,re.split("\(|\)| |\t|;|PCHOICE",line))
            assert(len(pvalList) == 1)
            pval = int(pvalList[0])
        if ('id=' in line) and ('Src(' not in line):
            #Node _out_s155 = new Or(id=3, l=2, r=1);
            comps = line.split("new")
            assert(len(comps) == 2)
            comp = comps[1].strip(';').strip()
            op = dagOP[comp.split('(')[0]] #Or : OR
            rest = map(lambda x: x.strip(),comp.split('(')[1].replace(')','').split(','))
            rdict = {}
            for s in rest:
                [a,b] = s.split('=')
                rdict[a]=int(b)
            
            if op in ['AND','OR','XOR','PLUS','LT','EQ','TIMES','DIV','MOD','ARR_R']:
                dagnodes[rdict['id']] = [op,rdict['l'],rdict['r']]
            elif op =='CONST' and 'vb' in rdict:
                dagnodes[rdict['id']] = [op,'BOOL',rdict['vb']]
            elif op =='CONST' and 'vi' in rdict:
                dagnodes[rdict['id']] = [op,'INT',rdict['vi']]
            elif op in ['NOT','NEG']:
                dagnodes[rdict['id']] = [op,rdict['l']]
            elif op=='ARRACC':
                dagnodes[rdict['id']] = [op,rdict['b'],rdict['l'],rdict['r']]
            elif op =='ARRASS':
                dagnodes[rdict['id']] = [op,rdict['b'],rdict['c'],rdict['l'],rdict['r']]
            elif op=='ARR_W':
                dagnodes[rdict['id']] = [op,rdict['l'],rdict['r'],rdict['b']]
            else: #CONST not implemented
                assert(False)
    if len(dagnodes) == len(srcs):
        #see if it's just an SRC
        for line in reversed(X.split('\n')):
            if ('Src(' in line): #last Src node
                comps = line.split("new")
                assert(len(comps) == 2)
                comp = comps[1].strip(';').strip()
                op = comp.split('(')[0] #Or : OR
                [k,v] = comp.split('(')[1].replace(')','').split('=')
                assert(op == 'Src' and k == 'id')
                final = int(v)
                break
    return (dagnodes,final,pval)
    

def hasOnlyBitOps(typemap,srcs):
    for i in typemap:
        if i >= len(srcs):
            if typemap[i] not in ['BIT','BOOL']:
                return "" #Need to use INT ops
    return "//" #use only bit ops!

def writeSketch(curSig,outFolder,fileCtr,pred,srcs,types,s,dagSize,USEINT,rhssize,outBit,outArr,astInclude,allPreds=None):
    NSRC=len(srcs)
    NTOTALF=min(dagSize[0],4)+NSRC-1
        
    if rhssize is not None:
        NTOTALF = min(rhssize,NTOTALF)
    if NTOTALF in [9,10] and NSRC <= 5:
        NTOTALF=8
    (NBITS,NINTS,NBARRS,NIARRS) = getTypedSRCs(types)
    try:
        CBITS = math.ceil(math.log(NTOTALF,2))
    except ValueError, err:
        d()
    USEARR = "//"
    if hasArray(types):
        USEARR=""
    SLVTIMEOUT = 2
    USEARRACC = "//"
    if "ARRACC" in curSig:
        USEARRACC = ""
    if allPreds:
        SLVTIMEOUT = 4#-V 10
    output = "pragma options \"--slv-timeout %d --bnd-inbits 3 --bnd-cbits 3 --bnd-int-range 1000 --beopt:simplifycex NOSIM\";\n#define ARRSZ 5\n%s#define USEARRACC\n%s#define USEINT\n%s#define USEARR\n#define MAXINT 100000\n#define NSRC %d\n#define NTOTALF %d\n#define NBITS %d\n#define NINTS %d\n#define NBARRS %d\n#define NIARRS %d\n#define ARRSZMAX 50\n#define CBITS %d" % (SLVTIMEOUT,USEARRACC,USEINT,USEARR,NSRC,NTOTALF,NBITS,NINTS,NBARRS,NIARRS,CBITS)
    output = output + "\n//" + curSig
    output = output + "\n" + "#include \"%s/ast.skh\"\n\n//Hardcoded evaluation of d-dag\nVal ddag(Val[NTOTALF] vals){\n    return %s;\n}\n" % (astInclude,s);
    
    #output = output + "\n" + "bit pred(Val[NTOTALF] vals){\n    return %s;\n}" % pred
    (header,valdefns) = getMainHeader(types,srcs)
    for src in types:
        if types[src] in ['INT_ARR','BIT_ARR','BOOL_ARR']:
            output = output + "\n#define sz%s ARRSZ-1" % src
    output = output + "\n" + "//harness specifying the synthesis constraints\nharness void main(%s){\n\n" % (header)
    output = output + "\n" + "    Val[NTOTALF] vals;\n     bit[NTOTALF] isBit = 0;\n    bit[NTOTALF] isArr = 0;\n%s\n" % valdefns
    if not allPreds:
        output = output + "\n    bit pr = 0;\n    pr = %s;\n" % pred
    else:
        P = len(allPreds)
        PBITS = max(1,math.ceil(math.log(P,2)))
        output = output + "\n    bit pr = 0;\n    int chpr =??(%d);\n    assert(chpr > 0 && chpr < %d);\n" % (PBITS,P)
        
        for pi in range(P):
            if pi == 0:
                output = output + "\n    if(chpr==%d){\n        assert(false);\n    }\n" % pi
                continue
            else:
                output = output + "\n    else if(chpr==%d){\n        pr=%s; PCHOICE(chpr);\n    }\n" % (pi,allPreds[pi])
        
    output = output + "    /*if(pred(vals)){*/if(pr){\n        int sz=NSRC;\n        int cost=0;\n        Node[NTOTALF] dag=dagGen(sz,cost,isBit,isArr,%s,%s);\n        minimize (cost);\n        int CT =NSRC;\n        assert(checkEq(eval(CT,vals,dag,sz),ddag(vals)));\n    }\n}" % (outBit,outArr)
    filename = outFolder+"/" + str(fileCtr) + ".sk"
    with open(filename, "w") as ofl:
        ofl.write(output)
    return filename

def getBitVals(b,allanvs):
    possible = set()
    for anvs in allanvs:
        if getBitVal(b,anvs[b]) == 1:
            possible.add(1)
        elif getBitVal(b,anvs[b]) == 0:
            possible.add(0)
    return possible
    
def getBitVal(b,anv):
    if len(anv) == 2 and anv[0] == anv[1]:
        return anv[0]
    elif len(anv) >= 2:
        return -1
    elif len(anv) == 1 and anv[0] == 1:
        return 1
    elif len(anv) == 1 and anv[0] == 0:
        return 0
    else:
        return -1

def negValid(i):
    if i==0:
        return 1
    elif i==1:
        return 0
    else:
        return -1

def checkEq(anv1,anv2):
    if anv1 == 'T' or anv2=='T':
        return -1
    elif len(anv1) == 1 and len(anv2) == 1:
        return int(anv1[0] == anv2[0])
    elif type(anv1) == list and type(anv2) == list and not set(anv1).intersection(set(anv2)):
        return 0
    elif predValid(anv1,anv2,'<')==1 or predValid(anv2,anv1,'<')==1:
        return 0
    else:
        return -1
        
def predValid(anv1,anv2,op):
    if op=='<' and anv1 != 'T' and anv2 != 'T':
        lt =max(anv1) < min(anv2)
        notlt = max(anv2) <= min(anv1)
        if (lt):
            return 1
        elif notlt:
            return 0
        else:
            return -1
    elif op=='<=' and anv1 != 'T' and anv2 != 'T':
        return negValid(predValid(anv2,anv1,'<'))
    elif op=='==' and anv1 != 'T' and anv2 != 'T':
        return checkEq(anv1,anv2)
    elif op=='!=' and anv1 != 'T' and anv2 != 'T':
        return negValid(checkEq(anv1,anv2))
    else:
        return -1
def predValidAllANVs(allanvs,s1,s2,op):
    possible = set()
    for anvs in allanvs:
        if 0 in possible and 1 in possible:
            break
        if type(s1) == str:
            anv1 = anvs[s1]
        else:
            anv1 = s1
        if type(s2) == str:
            anv2 = anvs[s2]
        else:
            anv2 = s2
        if predValid(anv1,anv2,op) == 1:
            possible.add(1)
        elif predValid(anv1,anv2,op) == 0:
            possible.add(0)
    return possible
def getPDags(pdefs,types,srcs):
    pdags = []
    for (op,l,r) in pdefs:
        pnodes = {}
        for src in types:
            pnodes[srcs.index(src)] = ['S',types[src],src]
        final = None
        if op is None and l==True and r is None:
            idd = len(pnodes)
            pnodes[idd] = ['CONST','BIT',1]
        elif op is None and type(l) == str and r is None:
            final = srcs.index(l)
        elif op == 'NOT' and type(l) == str and r is None:
            idd = len(pnodes)
            pnodes[idd] = ['NOT',srcs.index(l)]
        elif op == 'LT'and type(l) == str and type(r)==str:
            idd = len(pnodes)
            #print l,r
            pnodes[idd] = ['LT',srcs.index(l),srcs.index(r)]
        elif op == 'LTE'and type(l) == str and type(r)==str:
            idd = len(pnodes)
            pnodes[idd] = ['LT',srcs.index(r),srcs.index(l)]
            idd2 = len(pnodes)
            pnodes[idd2] = ['NOT',idd]
        elif op == 'LT' and type(l) == int and type(r) == str:
            idd = len(pnodes)
            pnodes[idd] = ['CONST','INT',l]
            idd2 = len(pnodes)
            pnodes[idd2] = ['LT',idd,srcs.index(r)]
        elif op == 'LT' and type(l) == str and type(r) == int:
            idd = len(pnodes)
            pnodes[idd] = ['CONST','INT',r]
            idd2 = len(pnodes)
            pnodes[idd2] = ['LT',srcs.index(l),idd]
        elif op == 'EQ' and type(l) == str and type(r) == int:
            idd = len(pnodes)
            pnodes[idd] = ['CONST','INT',r]
            idd2 = len(pnodes)
            pnodes[idd2] = ['EQ',srcs.index(l),idd]
        elif op == 'EQ'  and type(l) == str and type(r) == str:
            idd = len(pnodes)
            pnodes[idd] = ['EQ',srcs.index(l),srcs.index(r)]
        elif op == 'NEQ':
            idd = len(pnodes)
            pnodes[idd] = ['EQ',srcs.index(l),srcs.index(r)]
            idd2 = len(pnodes)
            pnodes[idd2] = ['NOT',idd]
        else:
            assert(False)
        pdags.append(strDag(0,pnodes,{},final))
    return pdags
        
def addImplies(implyList,preddefs,pred):
    if pred in preddefs:
        implyList.append(preddefs.index(pred))


def predImplies(preddefs):
    implies = {}
    TRUE = preddefs.index((None,True,None))
    for i in range(len(preddefs)):
        implies[i] = [TRUE]
        (op,l,r) = preddefs[i]
        if op is None and l==True and r is None:
            implies[i] = []
            continue
        elif op is None and type(l) == str and r is None:
            continue
        elif op == 'NOT' and type(l) == str and r is None:
            continue
        elif op == 'LT'and type(l) == str and type(r)==str:
            addImplies(implies[i],preddefs,('LTE',l,r))
            addImplies(implies[i],preddefs,('NEQ',l,r))
            addImplies(implies[i],preddefs,('NEQ',r,l))
        elif op == 'LTE'and type(l) == str and type(r)==str:
            continue
        elif op == 'LT' and type(l) == int and type(r) == str:
            if l==1:
                addImplies(implies[i],preddefs,('LT',0,r))
        elif op == 'LT' and type(l) == str and type(r) == int:
            if r==0:
                addImplies(implies[i],preddefs,('LT',l,1))
        elif op == 'EQ' and type(r) == int and type(l) == str:
            if r==0:
                addImplies(implies[i],preddefs,('LT',l,1))
        elif op == 'EQ' and type(l) == str and type(r)==str:
            #addImplies(implies[i],preddefs,('LTE',l,r))
            #addImplies(implies[i],preddefs,('LTE',r,l))
            continue
        elif op == 'NEQ' and type(l) == str and type(r)==str:
            continue
        else:
            assert(False)
    impliedby = {}
    for i in range(len(preddefs)):
        impliedby[i] = []
    for i in implies:
        for j in implies[i]:
            impliedby[j].append(i)
    return (implies,impliedby)
def getPreds(srcs,types,allanvs):
    #Old: a {< | <= | > | >= | != | ==} b where a,b are ints
    #New: a {< | != | == } b where a,b are ints/bits
    #a, !a  where a is bit
    preds = ["true"]
    preddefs = [(None,True,None)]
    intsOrBits = []
    for src in types:
        if types[src] in ['BIT','BOOL']:
            bvs = getBitVals(src,allanvs)
            intsOrBits.append(src)
            if 1 in bvs:
                #preds.append("getBit(vals[%d])" % srcs.index(src))
                preds.append(src)
                preddefs.append((None,src,None))
            elif 0 in bvs:
                #preds.append("!getBit(vals[%d])" % srcs.index(src))
                preds.append("!"+src)
                preddefs.append(('NOT',src,None))
        elif types[src] == 'INT' and src not in intsOrBits:
            intsOrBits.append(src)
            i1=src
            for cc in [0]:
                if 1 in predValidAllANVs(allanvs,i1,[cc],"=="):#predValid(anvs[i1],[cc],'==')==1:
                    #preds.append("getIntUpCast(vals[%d]) %s %d" % (srcs.index(i1),'==',cc))
                    preds.append("%s %s %d" %(i1,'==',cc))
                    preddefs.append(('EQ',i1,cc))
                elif 1 in predValidAllANVs(allanvs,[cc],i1,"<"):#predValid([cc],anvs[i1],'<')==1:
                    #preds.append("%d %s getIntUpCast(vals[%d])" % (cc,'<',srcs.index(i1)))
                    preds.append("%d %s %s" % (cc,'<',i1))
                    preddefs.append(('LT',cc,i1))
    for i1 in intsOrBits:
        for i2 in intsOrBits:
            if (i1 != i2):
                for op in ['<','<=']:
                    if 1 in predValidAllANVs(allanvs,i1,i2,op):#predValid(anvs[i1],anvs[i2],op)==1:
                        #preds.append("getIntUpCast(vals[%d]) %s getIntUpCast(vals[%d])" % (srcs.index(i1),'<',srcs.index(i2)))
                        preds.append("%s %s %s" % (i1,op,i2))
                        preddefs.append(('LT' if op == '<' else 'LTE' ,i1,i2))
            if (i1 < i2):
                for op in ['==','!=']:
                    if 1 in predValidAllANVs(allanvs,i1,i2,op):#predValid(anvs[i1],anvs[i2],op)==1:
                        #preds.append("getIntUpCast(vals[%d]) %s getIntUpCast(vals[%d])" % (srcs.index(i1),op,srcs.index(i2)))
                        preds.append("%s %s %s" % (i1,op,i2))
                        preddefs.append(('EQ' if op=='==' else 'NEQ',i1,i2))
    
    return (preds,preddefs)

def dagSig(i,dagnodes,final,ds):
    if not (i >=0 and i < len(dagnodes)):
        d()
    op = dagnodes[i][0]
    line = ""
    if op == 'S':
        line = "S:%s:%s" %(dagnodes[i][1],dagnodes[i][2])
    else:
        if op in ['AND','OR','XOR','LT','EQ','NOT']:
            line=""
        elif op in ['PLUS','TIMES','DIV','MOD','NEG']:
            line=""
        elif op == 'ARRACC':#b v0 v1
            b=dagnodes[i][1]
            v0=dagnodes[i][2]
            v1=dagnodes[i][3]
            line = "(ARRACC|%s|%s|%s)" %(ds[b],ds[v0],ds[v1])
        elif op == 'ARRASS':#b c v0 v1
            assert(False) #not allowed b c or c b ordering confusion!
        elif op == 'ARR_R':#index array
            line=""
        elif op == 'ARR_W':#index array newval
            ind = dagnodes[i][1]
            arr=dagnodes[i][2]
            newv = dagnodes[i][3]
            line = "(ARR_W|%s|%s|%s)" %(ds[ind],ds[arr],ds[newv])
        elif op == 'CONST':#CONST TYPE VAL
            line = "(CONST:%s|%d)" %(dagnodes[i][1],dagnodes[i][2])
        else: #invalid op
            assert(False)
        if line == "":
            line= "(%s|" %(op)
            if op in ['AND','OR','XOR','PLUS','TIMES']:
                sl = ds[dagnodes[i][1]]
                sr = ds[dagnodes[i][2]]
                if sl > sr:
                    st = sl
                    sl = sr
                    sr = st
                line = line + sl + "|" + sr + ")"
            else:
                line = line + '|'.join(map(lambda x: ds[x],dagnodes[i][1:])) + ")"
    ds[i] = line
    if i==len(dagnodes)-1 and final is None:
        return line
    elif i==len(dagnodes)-1 and final is not None:
        return ds[final]
    else:
        return  dagSig(i+1,dagnodes,final,ds)

def hasArray(types):
    for src in types:
        if types[src] in ['BIT_ARR','BOOL_ARR','INT_ARR']:
            return True
    return False
def elimPred(pimpliedby,i):
    pimpliedby.pop(i,None)
    for j in pimpliedby:
        if i in pimpliedby[j]:
            pimpliedby[j].remove(i)

def createDir(diry):
    try:
        os.stat(diry)
        return False
    except:
        os.mkdir(diry)   
        return True
            
def writeRules(rules,outFolder,ddagStr):
    diry = outFolder +"/" + "rules"
    try:
        os.stat(diry)
    except:
        os.mkdir(diry)       
    ctr=1
    for (pdagStr,(fnodes,ffinal)) in rules.values():
        fdagStr = strDag(0,fnodes,{},ffinal)
        dirin = diry +"/" + str(ctr)
        try:
            os.stat(dirin)
        except:
            os.mkdir(dirin)
        with open(dirin + "/d.aux","w") as fd:
            fd.write(ddagStr+"\n")
        with open(dirin + "/f.aux","w") as ff:
            ff.write(fdagStr+"\n")
        with open(dirin + "/p.aux","w") as fp:
            fp.write(pdagStr+"\n")
        ctr = ctr+1  
    
def pruneRules(rules,implies):
    toRemove = set()
    for i in implies:
        for j in implies[i]:
            if i in rules and j in rules:
                (fnodesi,ffinali) = rules[i][1]
                fsigi = dagSig(0,fnodesi,ffinali,{})
                (fnodesj,ffinalj) = rules[j][1]
                fsigj = dagSig(0,fnodesj,ffinalj,{})
                if fsigi == fsigj:
                    toRemove.add(i)
    for i in toRemove:
        rules.pop(i)
    
def getOutType(typemap):
    m=max(typemap.keys())
    if typemap[m] in ['BIT','BOOL']:
        return ("true","false")
    elif typemap[m] == 'INT':
        return ("false","false")
    elif typemap[m] in ['BIT_ARR','BOOL_ARR']:
        return ("true","true")
    elif typemap[m] == 'INT_ARR':
        return ("false","true")
    else:
        assert(False)
def removeANVfromSig(sig_anv):
    newsig = ""
    i=0
    while i < len(sig_anv):
        if sig_anv[i:].startswith("(S:"):
            lparen=0
            curStr = sig_anv[i:]
            for j in range(len(curStr)):
                if curStr[j] == "(":
                    lparen +=1
                elif curStr[j] == ")":
                    lparen -=1
                if lparen == 0:
                    #found matching paren
                    break
            srcl = curStr[1:j].split(":")
            assert(len(srcl) == 4)
            replstr = ":".join(srcl[:3])
            newsig+="(%s)" % replstr
            i=i+j+1
        else:
            newsig += sig_anv[i]
            i=i+1
    return newsig
        
def debugTraverse(file):
    sys.stdout.write(file+"\n")
    with open(file,"r") as ft:
        L = list(ft)
        CTR = 0
        for sig in L:
            try:
                #sys.stdout.write(str(CTR)+"\n")
                dag = OneOrMore(nestedExpr(opener='(',closer=')')).parseString(sig).asList()
                anvs = dict()
                types=dict()
                srcs = []
                dagSize = [0]
                dagnodes ={}
                (s,idd) = traverse(dag,anvs,types,srcs,dagSize,dagnodes)
                dagnodes = reorder(dagnodes,srcs,types)
                CTR=CTR+1
            except:
                d()
                raise
    
def getAllANVs(sigs):
    setcore = set(map(removeANVfromSig,sigs))
    try:
        assert(len(setcore) == 1)
    except:
        d()
        raise()
    allanvs = []
    types=dict()
    srcs = []
    dagSize = [0]
    dagnodes ={}
    s=""
    for sig in sigs:
        dag = OneOrMore(nestedExpr(opener='(',closer=')')).parseString(sig).asList()
        anvs = dict()
        types=dict()
        srcs = []
        dagSize = [0]
        dagnodes ={}
        (s,idd) = traverse(dag,anvs,types,srcs,dagSize,dagnodes)
        allanvs.append(anvs)
    
    return (s,list(setcore)[0],allanvs,types,srcs,dagSize,dagnodes)

def getType(id,dagnodes):
    entry = dagnodes[id]
    if entry[0] == 'S':
        return entry[1]
    elif entry[0] in ['PLUS','TIMES','DIV','MOD','NEG']:
        return 'INT'
    elif entry[0] in ['AND','OR','XOR','NOT']:
        return 'BIT'
    else:
        return None
    
    
def updateScalarToArray(dagnodes,srcs,types,newtypes):
    for i in dagnodes:
        op = dagnodes[i][0]
        if op == 'ARR_W':
            arrid = dagnodes[i][2]
            typ = dagnodes[arrid][1]
            if typ in ['INT','BIT','BOOL']:
                valid = dagnodes[i][3]
                valtyp = getType(valid,dagnodes)
                if valtyp:
                    typ = latJoin(typ,valtyp)
                if arrid not in newtypes:
                    newtypes[arrid] = typ + "_ARR"
                elif newtypes[arrid] != typ + "_ARR":
                    return False
    for i in dagnodes:
        op = dagnodes[i][0]
        if op != 'S':
            opers = dagnodes[i][1:]
            for j in newtypes:
                indices = [k for k,x in enumerate(opers) if x == j]
                if indices:
                    if op == 'ARR_R':
                        #0 = index, 1= arr
                        if len(indices) != 1 or indices[0] != 1:
                            return False
                    elif op == 'ARR_W':
                        #0=index, 1=arr, 2=newval
                        if len(indices) != 1 or indices[0] != 1:
                            return False
                    else:
                        return False
    #Now change the types!
    for j in newtypes:
        dagnodes[j][1] = newtypes[j]
        name = dagnodes[j][2]
        types[name] = newtypes[j]
    
    return True
        
def countDIVMOD(dagnodes):
    ctr=0
    for i in dagnodes:
        op = dagnodes[i][0]
        if op in ['DIV','MOD']:
            ctr +=1
    return ctr
                
def parseSig(sigs,outFolder,astInclude,tryAllPreds=True):
    print "python ~/smt-optim-sos/languageTranslators/dagToSketch.py -sig \"%s\" -of %s -inc %s" %("##".join(sigs),outFolder,astInclude)
    (s,sig,allanvs,types,srcs,dagSize,dagnodes) = getAllANVs(sigs)
    dagnodes = reorder(dagnodes,srcs,types)
    newtypes = {}
    if len(dagnodes) >=4: 
        tryAllPreds = True
    if countDIVMOD(dagnodes) > 2:
        print "Too many DIV/MODs",dagnodes
        return
    if not updateScalarToArray(dagnodes,srcs,types,newtypes):
        print "Can't update bit/int to array because of it's dual use #ERROR_UPDATE_TYPE_INT_ARR", newtypes,dagnodes
        return
    if len(newtypes) > 0:
        print "updates some types to array types: ", newtypes, srcs
        #check if there's any INT/BIT inputs to ARR_W nodes
     
    (outBit,outArr) = getOutType(types)
    (preds,pdefs) = getPreds(srcs,types,allanvs)
    (pimplies,pimpliedby) = predImplies(pdefs)
    
    pdags = getPDags(pdefs,types,srcs)
    #print anvs,types, preds
    fullTypeMap = {}
    ddagStr = strDag(0,dagnodes,fullTypeMap)
    assert(len(fullTypeMap)== len(dagnodes))
    USEINT = hasOnlyBitOps(fullTypeMap,srcs)
    print outFolder.split("/")[-1],":",len(preds),"predicates", pdefs
    #print pdefs
    #d()
    rules = {}
    rhssize = None
    while len(pimpliedby) > 0:
        #print pimpliedby
        #find i for which impliedby[i] is []
        i=-1
        for j in pimpliedby:
            if pdefs[j] == (None,True,None):
                i=j
                break
            if len(pimpliedby[j]) == 0:
                i=j
                break
        assert(i>=0)
        if pdefs[i] != (None,True,None) and tryAllPreds:
            allPreds = preds
        else:
            allPreds = None
        
        if allPreds:
            print "Trying All Preds: %s %s" % (outFolder.split("/")[-1],str(allPreds))
            (fnodes,ffinal,pval) = runSketch(sig,outFolder,"all","all",srcs,types,s,dagSize,USEINT,rhssize,outBit,outArr,astInclude,allPreds)
            i=pval;
            elimPred(pimpliedby,i)
        else:
            print "Trying Pred: %s-%d %s" % (outFolder.split("/")[-1],i,str(pdefs[i]))
            elimPred(pimpliedby,i)
            (fnodes,ffinal,pval) = runSketch(sig,outFolder,i,preds[i],srcs,types,s,dagSize,USEINT,rhssize,outBit,outArr,astInclude,allPreds)
        if fnodes:
            #found a rule!
            #Any predicate j s.t. i => j, 
            #if (j,FDAG(i)) works then improve i to j
            print "Found a rule for pred %d" % i
            #fdagStr = strDag(0,fnodes,{},ffinal)
            rules[i] = (pdags[i],(fnodes,ffinal))
            if pdefs[i] == (None,True,None):
                #any new f's should be smaller than this, strictly
                if ffinal is not None and ffinal < len(srcs):
                    fsz = len(srcs)+1
                else:
                    fsz = len(fnodes)
                if fsz == len(srcs)+1:
                    #can't improve it
                    print "Found a size 1 rule for TRUE. Eliminating all other predicates: " + str(pimpliedby.keys())
                    pimpliedby = {}
                else:
                    rhssize = fsz -1
                    if rhssize == 0:
                        d()
            if allPreds:
                break
                #TODO: try to continue here with other preds?
                    
        else:
            if allPreds:
                print "Tried all preds at once - none worked!"
                break
            #no rule here - 
            #everything this predicate implied won't have any rules too
            print "Eliminated preds: %d -> %s" %  (i,str(pimplies[i])) 
            for imp in pimplies[i]:
                elimPred(pimpliedby,imp)
    #print s, dagSize[0], anvs, types 
    
    #output all rules!
    pruneRules(rules,pimplies)
    writeRules(rules,outFolder,ddagStr)
    print "Generated %d rules in %s" %(len(rules),outFolder)
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-of",help="Where to put the final Sketch files",type=str,required=True)
    parser.add_argument("-sig",help="Signature of DAG with ANV info",type=str,default = "")
    #(ARR_R|(S:N_1:INT:L(|-4|0|8|))|(ARR_W|(S:N_2:INT:L(|0|8|))|(ARR_W|(S:N_3:BOOL:L(|1|))|(S:N_4:INT_ARR:L(|-1|0|))|(S:N_5:INT:L(|-1|1|))|)|(S:N_6:INT:L(|-1|2|))|)|)
    parser.add_argument("-file",type=str,default ="")
    parser.add_argument("-inc",type=str,default="../..")
    args = parser.parse_args()

    if args.file != "":
        with open(args.file,'r') as f:
            lines = [line.replace('\n','') for line in f.readlines()]
            ctr=0
            for line in lines:
                diry = args.of +"/dir" + str(ctr)
                
                try:
                    os.stat(diry)
                except:
                    os.mkdir(diry)
                dirtmp = args.of +"/dir" + str(ctr) + "/tmp"
                try:
                    os.stat(dirtmp)
                except:
                    os.mkdir(dirtmp)    
                ctr=ctr+1     
                parseSig(line.split("##"),diry,args.inc)
    elif args.sig != "":
        parseSig(args.sig.split("##"),args.of,args.inc,False)
    else:
        print "Required -file or -sig arguments. Exiting..."
        sys.exit(1)
        

